#!/bin/bash

NAME="generatorKursow"
DJANGODIR=/home/marek/sites/generatorKursow/source/src/

cd $DJANGODIR

exec ../../virtualenv/bin/gunicorn \
    --bind unix:/tmp/generatorKursow.socket \
    generatorKursow.wsgi:application