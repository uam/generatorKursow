"""
WSGI config for generatorKursow project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os

from django.conf import settings

from plugins.helper import get_all_plugins

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "generatorKursow.settings")

user_plugins = get_all_plugins()

for plugin in user_plugins:
    settings.INSTALLED_APPS += ('userPlugins.%s' % plugin.get('name'), )
    settings.STATICFILES_DIRS += (os.path.join('userPlugins', plugin.get('name'), 'static'), )

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
