from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',

    url(r'^', include('web.urls', namespace='web')),

    url(r'^theory/', include('apps.theory.urls', namespace='theory')),
    url(r'^course/', include('apps.course.urls', namespace='course')),
    url(r'^module/', include('apps.module.urls', namespace='module')),
    url(r'^material/', include('apps.material.urls', namespace='material')),
    url(r'^interactivity/', include('apps.interactivity.urls', namespace='interactivity')),

    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^docs/', include('rest_framework_swagger.urls')),

    url(r'^account/plugins/', include('plugins.urls', namespace="plugins")),
    url(r'^account/templates/', include('userTemplates.urls', namespace='userTemplates')),
    url(r'^login', 'django.contrib.auth.views.login', name="login"),
    url(r'^logout', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="logout"),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/', include('registration.backends.simple.urls', namespace='accounts')),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))