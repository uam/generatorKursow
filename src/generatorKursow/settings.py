# -*- coding: UTF-8 -*-

"""
Django settings for generatorKursow project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


from django.conf import global_settings

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []


SITE_NAME = 'Generator kursów'
META_KEYWORDS = 'generator, kursy elarningowe'
META_DESCRIPTION = """Generator kursów"""

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_extensions',
    'polymorphic',
    'author',
    'rest_framework',
    'rest_framework_swagger',
    'compressor',
    'crispy_forms',
    'django_modalview',

    'jquery',
    'djangoformsetjs',

    'apps.course',
    'apps.module',
    'apps.material',
    'apps.theory',
    'apps.interactivity',
    'apps.common',

    'plugins',
    'web',
    'userTemplates'

)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'author.middlewares.AuthorDefaultBackendMiddleware',

    'generatorKursow.middleware.LoginRequiredMiddleware',

)


TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'web.context_proccessors.site_proccessor',
)

STATICFILES_FINDERS = global_settings.STATICFILES_FINDERS + (
    'compressor.finders.CompressorFinder',
)

ROOT_URLCONF = 'generatorKursow.urls'

WSGI_APPLICATION = 'generatorKursow.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../../', 'database/db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pl'

TIME_ZONE = 'Poland'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '../../', 'static')

STATICFILES_DIRS = global_settings.STATICFILES_DIRS + (os.path.join(BASE_DIR, 'preview'),)

MEDIA_ROOT = os.path.join(BASE_DIR, '../../', 'media')
MEDIA_URL = '/media/'


AUTHOR_CREATED_BY_FIELD_NAME = 'createdBy'
AUTHOR_UPDATED_BY_FIELD_NAME = 'modifiedBy'


COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)


LOGIN_URL = '/login'

LOGIN_EXEMPT_URLS = [u'accounts/register/']

LOGIN_REDIRECT_URL = '/'

CRISPY_TEMPLATE_PACK = 'bootstrap3'
