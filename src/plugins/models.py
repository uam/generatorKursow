from django.db import models


class Plugin(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)
    is_active = models.BooleanField(default=False)

    def __unicode__(self):
        return "[%s] - %s" % (self.name, self.description)
