from django.conf.urls import patterns, url

from plugins import views

urlpatterns = patterns(
    '',

    url(r'^$', views.IndexView.as_view(), name="index"),
    url(r'^refresh/$', views.refresh, name="refresh"),
    url(r'^(?P<pk>[\d]+)/del/$', views.PluginDeleteView.as_view(), name="delete"),
    # url(r'^(?P<pk>[\d]+)/activate/$', views.ActivateView.as_view(), name="activate"),

)
