# -*- coding: utf-8 -*-
from optparse import make_option

from django.core.management.base import NoArgsCommand
from django.utils import six
from django.core.management import call_command

from importlib import import_module

from plugins.helper import get_all_plugins
from plugins.models import Plugin


class Command(NoArgsCommand):
    option_list = NoArgsCommand.option_list + (
        make_option('--delete', action='store_true', dest='delete',
                    help='delete the REMOVED Plugin and PluginPoint '
                    'instances. '),
    )
    help = ("Syncs the registered plugins and plugin points with the model "
            "versions.")

    requires_model_validation = True

    def handle_noargs(self, **options):
        sync = SyncPlugins()


class SyncPlugins():
    """
    In most methods ``src`` and ``dst`` variables are used, they meaning is:

    ``src``
        source, registered plugin point objects

    ``dst``
        destination, database
    """

    def __init__(self):

        user_plugins = get_all_plugins()
        user_plugins_names = [plugin.get('name') for plugin in user_plugins]

        for plugin in user_plugins:
            name = plugin.get('name')
            call_command('migrate', name)

            if not Plugin.objects.filter(name=name).exists():
                Plugin.objects.create(name=plugin.get('name'), description=plugin.get('description'))

        # TODO: usunięcię migracji również!
        Plugin.objects.exclude(name__in=user_plugins_names).delete()
