import os
import json


def get_all_plugins():

    from django.conf import settings

    PLUGINS_PATH = os.path.join(settings.BASE_DIR, "userPlugins")

    plugins = []
    for name in os.listdir(PLUGINS_PATH):
        path = os.path.join(PLUGINS_PATH, name)
        if os.path.isdir(path):

            if os.path.exists(os.path.join(path, 'description.json')):
                file_json = open(os.path.join(path, 'description.json')).read()
                try:
                    json_data = json.loads(file_json)
                except ValueError as error:
                    raise Exception("Niepoprawny format JSON. %s" % error)

                plugins.append(json_data)

            else:
                raise Exception('Brak pliku description.json dla pluginu %s' % (name))

    return plugins


def get_full_plugins_info():
    from django.conf import settings

    PLUGINS_PATH = os.path.join(settings.BASE_DIR, "userPlugins")

    plugins = {}
    for name in os.listdir(PLUGINS_PATH):
        path = os.path.join(PLUGINS_PATH, name)
        if os.path.isdir(path):

            if os.path.exists(os.path.join(path, 'description.json')):
                file_json = open(os.path.join(path, 'description.json')).read()
                try:
                    json_data = json.loads(file_json)
                except ValueError as error:
                    raise Exception("Niepoprawny format JSON. %s" % error)

                plugins[name] = {'json_data': json_data, 'path': path, 'ID': json_data['ID']}
            else:
                raise Exception('Brak pliku description.json dla pluginu %s' % (name))



    return plugins