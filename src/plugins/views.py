# -*- coding: utf-8 -*-
import zipfile

import os
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.views.generic.edit import FormView, DeleteView
from plugins.forms import UploadNewPluginForm
from .models import Plugin


class IndexView(FormView):
    template_name = 'plugins/index.html'
    form_class = UploadNewPluginForm

    success_url = reverse_lazy('plugins:index')

    def get_context_data(self, **kwargs):
        ret = super(IndexView, self).get_context_data(**kwargs)

        ret.update({
            'plugins': Plugin.objects.all()
        })

        return ret

    def form_valid(self, form):

        _file = form.cleaned_data['file']
        output_path = os.path.join(settings.BASE_DIR, 'userPlugins')

        with zipfile.ZipFile(_file, 'r') as archive:
            for name in archive.namelist():
                archive.extract(name, output_path)

        _file.close()

        refresh(self.request)

        return super(IndexView, self).form_valid(form=form)


class PluginDeleteView(DeleteView):
    template_name = 'plugins/delete.html'
    model = Plugin
    success_url = reverse_lazy('plugins:index')

    def post(self, request, *args, **kwargs):

        self.object = self.get_object()
        output_path = os.path.join(settings.BASE_DIR, 'userPlugins')

        import shutil
        shutil.rmtree(os.path.join(output_path, self.object.name))

        refresh(request)

        return redirect(self.success_url)


@login_required
def refresh(request):

    import subprocess
    python_path = os.getenv('VIRTUAL_ENV', os.path.join(settings.BASE_DIR, '../../virtualenv/'))

    process = subprocess.Popen([
        python_path + '/bin/python',
        settings.BASE_DIR + '/manage.py',
        'sync_plugins'])

    process.wait()

    return redirect('plugins:index')



# Wszystkie pluginy są aktywne
# class DeactivateView(UpdateView):
#     template_name = 'plugins/index.html'
#
#     def get_queryset(self):
#         return get_object_or_404(Plugin, pk=self.kwargs.get('pk'))
#
#     def get(self, request, pk):
#
#         plugin = get_object_or_404(Plugin, pk=self.kwargs.get('pk'))
#         plugin.is_active = False
#
#         call(["python", "manage.py", "migrate", "%s" % plugin.name, "0001"])
#         plugin.save()
#
#         return redirect('plugins:index')
#
#
# class ActivateView(UpdateView):
#     template_name = 'plugins/index.html'
#
#     def get_queryset(self):
#         return get_object_or_404(Plugin, pk=self.kwargs.get('pk'))
#
#     def get(self, request, pk):
#
#         plugin = get_object_or_404(Plugin, pk=self.kwargs.get('pk'))
#         plugin.is_active = True
#
#         call(["python", "manage.py", "migrate", "%s" % plugin.name, "0002"])
#
#         plugin.save()
#
#         return redirect('plugins:index')


