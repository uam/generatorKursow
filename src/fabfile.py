import random

from fabric.api import env, local, run
from fabric.contrib.files import append, exists, sed

REPO_URL = 'https://gitlab.com/uam/generatorKursow.git'
PROJECT_NAME = 'generatorKursow'


def install_packages():
    packages = "python-pip python-virtualenv python-dev nginx nodejs-legacy npm git libtiff5-dev libjpeg8-dev zlib1g-dev \
    libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk"
    run('sudo apt-get update')
    run('sudo apt-get install -y %s' % packages)
    run('sudo pip install supervisor')


def nginx():
    nginx_folder = '/home/%s/sites/%s/source/nginx' % (env.user, PROJECT_NAME)

    run('sudo cp %s/generator.conf /etc/nginx/sites-available/generatorKursow' % nginx_folder)
    run('sudo ln -s -f /etc/nginx/sites-available/generatorKursow /etc/nginx/sites-enabled/generatorKursow')
    run('sudo rm /etc/nginx/sites-enabled/default')
    run('sudo service nginx reload')


def deploy():
    site_folder = '/home/%s/sites/%s' % (env.user, PROJECT_NAME)
    source_folder = site_folder + '/source'
    app_folder = source_folder + '/src'
    static_folder = site_folder + '/static'
    _create_directory_structure_if_necessary(site_folder)
    _get_latest_source(source_folder)
    _update_settings(app_folder, env.host)
    _update_virtualenv(source_folder)
    _update_static_files(app_folder)
    _update_database(app_folder)
    _install_bower_dependencies(static_folder)


def bower():
    site_folder = '/home/%s/sites/%s' % (env.user, PROJECT_NAME)
    source_folder = site_folder + '/static'
    _install_bower_dependencies(source_folder)


def start():
    site_folder = '/home/%s/sites/%s' % (env.user, PROJECT_NAME)
    source_folder = site_folder + '/source/src'
    _kill_previous_process(source_folder)
    _start_gunicorn(source_folder)


def _kill_previous_process(folder):
    if exists(folder + '/pidfile.txt'):
        run('cd %s && kill -9 `cat pidfile.txt`' % folder)
        run('cd %s && rm -f pidfile.txt' % folder)


def _start_gunicorn(folder):
    run('cd %s && ../../virtualenv/bin/gunicorn -p pidfile.txt -w 2 --bind unix:/tmp/generatorKursow.socket %s.wsgi:application' % (folder, PROJECT_NAME))
    run('sleep 3')



def _create_directory_structure_if_necessary(site_folder):
    for subfolder in ('database', 'static', 'virtualenv', 'source', 'logs', 'media'):
        run('mkdir -p %s/%s' % (site_folder, subfolder))


def _get_latest_source(source_folder):
    if exists(source_folder + '/.git'):
        run('cd %s && git pull' % (source_folder,))
    else:
        run('git clone %s %s' % (REPO_URL, source_folder))
        current_commit = local("git log -n 1 --format=%H", capture=True)
        run('cd %s && git reset --hard %s' % (source_folder, current_commit))


def _update_settings(source_folder, site_name):
    settings_path = source_folder + '/%s/settings.py' % PROJECT_NAME
    # sed(settings_path, "DEBUG = True", "DEBUG = False")
    sed(
        settings_path,
        'ALLOWED_HOSTS =.+$',
        'ALLOWED_HOSTS = ["%s"]' % (site_name,)
    )
    secret_key_file = source_folder + '/%s/secret_key.py' % PROJECT_NAME
    if not exists(secret_key_file):
        chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
        key = ''.join(random.SystemRandom().choice(chars) for _ in range(50))
        append(secret_key_file, "SECRET_KEY = '%s'" % (key,))
    append(settings_path, '\nfrom .secret_key import SECRET_KEY')


def _update_virtualenv(source_folder):
    virtualenv_folder = source_folder + '/../virtualenv'
    if not exists(virtualenv_folder + '/bin/pip'):
        run('virtualenv %s' % (virtualenv_folder,))
    run('%s/bin/pip install -r %s/src/requirements.txt' % (
        virtualenv_folder, source_folder
    ))


def _update_static_files(source_folder):
    run('cd %s && ../../virtualenv/bin/python manage.py collectstatic --noinput' % (
        source_folder,
    ))


def _update_database(source_folder):
    run('cd %s && ../../virtualenv/bin/python manage.py migrate --noinput' % (
        source_folder,
    ))


def _install_bower_dependencies(source_folder):
    run('cd %s && mkdir -p npm' % source_folder)
    if not exists(source_folder + '/npm/node_modules/bower'):
        run('cd %s && npm install --prefix ./npm bower' % source_folder)
    run('cd %s && %s/npm/node_modules/bower/bin/bower install' % (source_folder, source_folder))
