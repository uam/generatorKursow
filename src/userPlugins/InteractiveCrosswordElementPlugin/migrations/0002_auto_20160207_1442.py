# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('InteractiveCrosswordElementPlugin', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('interactivity', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='InteractiveCrosswordElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('answerPosition', models.IntegerField(verbose_name='Pozycja odpowiedzi')),
                ('mainAnswer', models.CharField(max_length=512, verbose_name='Has\u0142o krzy\u017c\xf3wki')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.AddField(
            model_name='crosswordanswer',
            name='createdBy',
            field=models.ForeignKey(related_name='crosswordanswer_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='crosswordanswer',
            name='interactive_element',
            field=models.ForeignKey(related_name='answers', to='InteractiveCrosswordElementPlugin.InteractiveCrosswordElement'),
        ),
        migrations.AddField(
            model_name='crosswordanswer',
            name='modifiedBy',
            field=models.ForeignKey(related_name='crosswordanswer_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
