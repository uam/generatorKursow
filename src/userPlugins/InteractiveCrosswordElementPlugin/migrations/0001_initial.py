# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CrosswordAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('answer', models.CharField(max_length=512, verbose_name='Odpowied\u017a')),
                ('begin', models.IntegerField(default=1, verbose_name='Pozycja pocz\u0105tku')),
                ('number', models.IntegerField(default=1, verbose_name='Numer odpowiedzi')),
                ('question', models.TextField(verbose_name='Pytanie pomocnicze')),
            ],
            options={
                'ordering': ['number'],
            },
        ),
    ]
