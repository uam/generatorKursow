from apps.common.forms import BaseInteractiveElementForm, BaseInteractiveRelatedElementForm
from .models import InteractiveCrosswordElement, CrosswordAnswer


class InteractiveCrosswordElementForm(BaseInteractiveElementForm):
    @staticmethod
    def get_related_forms():
        return [CrosswordAnswerForm]

    class Meta:
        model = InteractiveCrosswordElement
        exclude = ('visibilityType', 'createdBy', 'modifiedBy',)


class CrosswordAnswerForm(BaseInteractiveRelatedElementForm):
    name = 'crossword_answer'

    class Meta:
        model = CrosswordAnswer
        exclude = ('visibilityType', 'createdBy', 'modifiedBy',)
