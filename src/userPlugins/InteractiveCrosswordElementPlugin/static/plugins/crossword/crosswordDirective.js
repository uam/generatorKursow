Directives.directive("crossword", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/crossword/template.html',
        controller: 'crosswordController',

    };

});