Controllers.controller('crosswordController', ['$scope', '$element',
    function ($scope, $element) {



        var fisrstHeight = 0;

        var createRoundDiv = (function (left, top, color, circleWidth, circleHeight) {

            var element = document.createElement("input");
            $(element).css("left", left);
            $(element).css("top", top);
            $(element).css("width", circleWidth);
            $(element).css("height", circleHeight);
            $(element).attr("type", "text");
            $(element).attr("maxlength", 1);
            if (color) {
                $(element).addClass("crossword-answer");
            }
            $(element).addClass("crossword-circle-input2");
       
            $(element).focus(function (e) {
                    $(e.target).select();
                    for (var i = 1; i <= $scope.data.answers.length; ++i) {
                        if ($(e.target).hasClass("crossword-row" + i)) {
                            $($element).find(".crossword-question" + i).slideDown(300);

                        } else {
                            $($element).find(".crossword-question" + i).hide();
                        }

                    }
                }                 


            )



            $(element).on("keyup", function (e) {
                if ($(e.target).val().length == 1 && !$($(e.target).next()).hasClass("crossword-number")) {

                    $(e.target).next().focus().select();
                } else {

                }

                check();

            })

            return element;
        })

        var createNumber = (function (left, top, number, circleWidth, circleHeight) {
            var element = document.createElement("div");
            $(element).css("left", left);
            $(element).css("top", top);
            $(element).css("width", circleWidth);
            $(element).css("height", circleHeight);
            $(element).addClass("crossword-number");
            $(element).text(number + ".");
            return element;
        })

        var createQuestions = (function (number, text) {
            var element = document.createElement("div");
            $(element).addClass("crossword-question" + number);
            $(element).text(number + ". " + text);
            return element;
        });

        var init = (function () {

            var helper = $scope.data.answers;
            var maxWidth = 0;
            var circleSpace = 5;
            var rowTop = circleSpace;
            var counter = 0;

            var maxLengthOfAnswer = 0;
            for (var i in helper) {
                if (helper[i].answer.length + helper[i].begin > maxLengthOfAnswer) {
                    maxLengthOfAnswer = helper[i].answer.length + helper[i].begin;
                }
            }

            var circleWidth = ($($($element).find(".crossword-crossword-container")[0]).width() - maxLengthOfAnswer * circleSpace) / (maxLengthOfAnswer);
            var circleHeight = circleWidth;


            for (var i in helper) {

                var rowLeft = 0;
                for (var q = 1; q < helper[i].begin; ++q) {
                    rowLeft += circleWidth + circleSpace;
                }
                $($element).find(".crossword-crossword-container").append(createNumber(rowLeft, rowTop, ++counter, circleWidth, circleHeight));
                rowLeft += circleWidth + circleSpace;
                for (var q = 1; q <= helper[i].answer.length; ++q) {
                    if (rowLeft / (circleWidth + circleSpace) == $scope.data.answerPosition) {
                        $($element).find(".crossword-crossword-container").append($(createRoundDiv(rowLeft, rowTop, true, circleWidth, circleHeight)).addClass("crossword-row" + counter));
                    } else {

                        $(".crossword-crossword-container").append($(createRoundDiv(rowLeft, rowTop, false, circleWidth, circleHeight)).addClass("crossword-row" + counter));
                    }
                    rowLeft += circleWidth + circleSpace;
                    if (rowLeft > maxWidth) {
                        maxWidth = rowLeft;
                    }
                }

                $(".crossword-crossword-bottom").append(createQuestions(counter, helper[i].question));


                rowTop += circleSpace + circleHeight;
                $($element).find(".crossword-crossword-container").css("height", rowTop + circleSpace);
                fisrstHeight = rowTop + circleSpace;
                $($element).find(".crossword-crossword-container").css("width", maxWidth + (circleWidth + circleSpace));
            }

            rwd();
        });


        var clearAll = (function () {
            $($element).find(".crossword-circle-input").val("");
        })


        var check = (function () {


            var helper = $scope.data.answers;
            var counter = 0;
            var score = 0;

            for (var i in helper) {
                var answer = helper[i].answer;
                guestAnswer = "";
                var row = $($element).find(".crossword-row" + (++counter));
                for (var q = 0; q < row.length; ++q) {
                    guestAnswer += $(row[q]).val(); //concatenation
                }
                if (answer.toUpperCase() == guestAnswer.toUpperCase()) {
                    score++;

                    $($element).find(".crossword-row" + (counter)).addClass("crossword-good");
                } else {
                    $(".crossword-row" + (counter)).removeClass("crossword-good");
                }
            }

            var guestMainAnswer = "";
            var column = $($element).find(".crossword-answer");
            for (var q = 0; q < column.length; ++q) {
                guestMainAnswer += $(column[q]).val(); //concatenation
            }
            if (guestMainAnswer.toUpperCase() == $scope.data.mainAnswer) {
                $($element).find(".crossword-answer").addClass("crossword-good");
                $($element).find(".crossword-winner").fadeIn(300);
            } else {
                $($element).find(".crossword-answer").removeClass("crossword-good");
                $($element).find(".crossword-winner").hide();
            }


        })

        $(window).on('resize', function () {
            rwd();
        });

        var rwd = (function () {

            var widthPage = $($($element).find(".crossword-content")[0]).width();
            var widthCrossword = $($($element).find(".crossword-crossword-container")[0]).width();

            var ratio = widthCrossword / widthPage;

            $($($element).find(".crossword-crossword-container")[0]).css("transform", "scale(" + 1 / ratio + ")");
            $($($element).find(".crossword-crossword-container")[0]).css("transform-origin", "0 0 0");
            $($($element).find(".crossword-crossword-container")[0]).height(fisrstHeight * (1 / ratio));



        });


        init();



                }]);