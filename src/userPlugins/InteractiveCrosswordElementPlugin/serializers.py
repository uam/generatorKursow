from api.common.serializers import DefaultInteractiveElementSerializer, DEFAULT_INTERACTIVE_ELEMENTS_FIELDS, \
    DefaultSerializer
from api.interactivity.serializers import InteractiveElementSerializer
from .models import CrosswordAnswer, InteractiveCrosswordElement


class CrosswordAnswerSerializer(DefaultSerializer):

    class Meta:
        model = CrosswordAnswer
        fields = ('answer', 'begin', 'question')


class InteractiveCrosswordElementSerializer(InteractiveElementSerializer):

    answers = CrosswordAnswerSerializer(many=True)

    def create(self, validated_data):
        _answers = validated_data.pop('answers')

        interactive_element = InteractiveCrosswordElement.objects.create(**validated_data)

        for answer in _answers:
            CrosswordAnswer.objects.create(interactive_element=interactive_element, **answer)

        return interactive_element

    def update(self, instance, validated_data):

        _answers = validated_data.pop('answers')

        # Delete any answers not included in the request
        answers_ids = [answer.get('id') for answer in _answers]
        for answer in instance.answers.all():
            if answer.id not in answers_ids:
                answer.delete()

        updated_answers = []

        # Create or update answer instances that are in the request
        for item in _answers:
            (answer_object, _) = CrosswordAnswer.objects.update_or_create(id=item.get('id', None), interactive_element=instance, defaults=item)
            updated_answers.append(answer_object)

        instance.answers = updated_answers

        super(InteractiveCrosswordElementSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        model = InteractiveCrosswordElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('answers', 'answerPosition', 'mainAnswer')
