# coding=utf-8
from apps.interactivity.models import InteractiveElement
from author.decorators import with_author
from django.db import models
from django_extensions.db.models import TimeStampedModel


class InteractiveCrosswordElement(InteractiveElement):

    answerPosition = models.IntegerField(verbose_name=u"Pozycja odpowiedzi")
    mainAnswer = models.CharField(max_length=512, verbose_name=u"Hasło krzyżówki")

    @staticmethod
    def get_serializer():
        from .serializers import InteractiveCrosswordElementSerializer
        return InteractiveCrosswordElementSerializer

    @staticmethod
    def get_form():
        from .forms import InteractiveCrosswordElementForm
        return InteractiveCrosswordElementForm

    @staticmethod
    def get_related_objects_form_dict():
        from .forms import CrosswordAnswerForm
        return {'crossword_answer': CrosswordAnswerForm}

    class InteractiveType:
        ID = 'crossword'
        text = u'Element interaktywny - krzyzowka'


@with_author
class CrosswordAnswer(TimeStampedModel):
    interactive_element = models.ForeignKey('InteractiveCrosswordElement', related_name='answers')
    answer = models.CharField(verbose_name=u"Odpowiedź", max_length=512)
    begin = models.IntegerField(default=1, verbose_name=u"Pozycja początku")
    number = models.IntegerField(default=1, verbose_name=u"Numer odpowiedzi")
    question = models.TextField(verbose_name=u"Pytanie pomocnicze")

    def __unicode__(self):
        return u"[%s ]: %s" % (self.question, self.answer)

    @staticmethod
    def get_form():
        from .forms import CrosswordAnswerForm
        return CrosswordAnswerForm

    class Meta:
        ordering = ['number']

