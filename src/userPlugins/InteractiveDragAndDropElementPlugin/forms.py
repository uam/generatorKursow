from apps.common.forms import BaseInteractiveRelatedElementForm, BaseInteractiveElementForm
from .models import InteractiveDragAndDropElement


class DragAndDropElementItemForm(BaseInteractiveRelatedElementForm):
    name = 'draganddrop_item'

    class Meta:
        model = InteractiveDragAndDropElement
        exclude = ('visibilityType', 'createdBy', 'modifiedBy',)


class InteractiveDragAndDropElementForm(BaseInteractiveElementForm):
    @staticmethod
    def get_related_forms():
        return [DragAndDropElementItemForm]

    class Meta:
        model = InteractiveDragAndDropElement
        exclude = ('visibilityType', 'createdBy', 'modifiedBy',)
