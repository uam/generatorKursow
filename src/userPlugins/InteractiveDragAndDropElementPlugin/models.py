# coding=utf-8
from apps.interactivity.models import InteractiveElement
from author.decorators import with_author
from django.db import models
from django_extensions.db.models import TimeStampedModel


class InteractiveDragAndDropElement(InteractiveElement):

    title = models.CharField(max_length=512, verbose_name='Tytuł')

    @staticmethod
    def get_serializer():
        from .serializers import InteractiveDragAndDropElementSerializer
        return InteractiveDragAndDropElementSerializer

    @staticmethod
    def get_form():
        from .forms import InteractiveDragAndDropElementForm
        return InteractiveDragAndDropElementForm

    @staticmethod
    def get_related_objects_form_dict():
        from .forms import DragAndDropElementItemForm
        return {'draganddrop_item': DragAndDropElementItemForm}


    class InteractiveType:
        ID = 'draganddrop'
        text = u'Element interaktywny - Drag & Drop'


@with_author
class DragAndDropElementItem(TimeStampedModel):

    interactive_element = models.ForeignKey('InteractiveDragAndDropElement', related_name='items')
    correct_position = models.IntegerField(default=1, verbose_name="Poprawna pozycja")
    position = models.IntegerField(default=1, verbose_name="Pozycja w ciągu")
    label = models.CharField(max_length=512, verbose_name="Tekst")

    def __unicode__(self):
        return u"[%s ]: %s" % (self.question, self.answer)

    @staticmethod
    def get_form():
        from .forms import DragAndDropElementItemForm
        return DragAndDropElementItemForm

    class Meta:
        ordering = ['position']
