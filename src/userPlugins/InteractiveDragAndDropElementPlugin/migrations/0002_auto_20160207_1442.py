# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('InteractiveDragAndDropElementPlugin', '0001_initial'),
        ('interactivity', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='InteractiveDragAndDropElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('title', models.CharField(max_length=512, verbose_name=b'Tytu\xc5\x82')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.AddField(
            model_name='draganddropelementitem',
            name='createdBy',
            field=models.ForeignKey(related_name='draganddropelementitem_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='draganddropelementitem',
            name='interactive_element',
            field=models.ForeignKey(related_name='items', to='InteractiveDragAndDropElementPlugin.InteractiveDragAndDropElement'),
        ),
        migrations.AddField(
            model_name='draganddropelementitem',
            name='modifiedBy',
            field=models.ForeignKey(related_name='draganddropelementitem_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
