Directives.directive("draganddrop", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/draganddrop/template.html',
        controller: 'draganddropController',

    };

});