Controllers.controller('draganddropController', ['$scope', '$element',
    function ($scope, $element) {

        $scope.model = $scope.data.items;
        $scope.title = $scope.data.title;


        $scope.dragoverCallback = function (event, index, external, type) {
            $scope.logListEvent('dragged over', event, index, external, type);
            // Disallow dropping in the third row. Could also be done with dnd-disable-if.
            return index < 10;
        };

        $scope.dropCallback = function (event, index, item, external, type, allowedType) {
            $scope.logListEvent('dropped at', event, index, external, type);
            if (external) {
                if (allowedType === 'itemType' && !item.label) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }
            return item;
        };

        $scope.logEvent = function (message, event) {

        };

        $scope.logListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;
            $scope.logEvent(message, event);
            checkAllItem(message, event, action, index, type);
        };


        $scope.$watch('model', function (model) {
            $scope.modelAsJson = angular.toJson(model, true);
        }, true);


        checkAllItem = function (message, event, action, index, type) {

            var len = $scope.model.length;
            var tab = $scope.model;

            var suma = 0;
            for (var i = 0; i < len; i++) {
                if (i + 1 == tab[i].correct_position) {
                    suma++;
                }
            }

            if (suma == len) {
                $scope.win = true;
            }

        };

        $scope.$watch('model', $scope.logListEvent, true);


    }]);