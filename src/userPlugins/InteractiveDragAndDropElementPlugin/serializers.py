from api.common.serializers import DefaultSerializer, DefaultInteractiveElementSerializer, \
    DEFAULT_INTERACTIVE_ELEMENTS_FIELDS
from rest_framework import serializers

from api.interactivity.serializers import InteractiveElementSerializer
from .models import DragAndDropElementItem, InteractiveDragAndDropElement


class DragAndDropElementItemSerializer(DefaultSerializer):
    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = DragAndDropElementItem
        fields = ('correct_position', 'position', 'label', 'id')


class InteractiveDragAndDropElementSerializer(InteractiveElementSerializer):
    items = DragAndDropElementItemSerializer(many=True)

    def create(self, validated_data):
        _items = validated_data.pop('items')

        interactive_element = InteractiveDragAndDropElement.objects.create(**validated_data)

        for item in _items:
            DragAndDropElementItem.objects.create(interactive_element=interactive_element, **item)

        return interactive_element

    def update(self, instance, validated_data):

        _items = validated_data.pop('answers')

        # Delete any answers not included in the request
        items_ids = [item.get('id') for item in _items]
        for item in instance.items.all():
            if item.id not in items_ids:
                item.delete()

        updated_items = []

        # Create or update answer instances that are in the request
        for item in _items:
            (item_object, _) = DragAndDropElementItem.objects.update_or_create(id=item.get('id', None), interactive_element=instance, defaults=item)
            updated_items.append(item_object)

        instance.answers = updated_items

        super(InteractiveDragAndDropElementSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        model = InteractiveDragAndDropElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('items', 'title', 'interactive_type')

