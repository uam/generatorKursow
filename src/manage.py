#!/usr/bin/env python
import os
import sys

from plugins.helper import get_all_plugins

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "generatorKursow.settings")

    from django.core.management import execute_from_command_line
    from django.conf import settings

    user_plugins = get_all_plugins()

    for plugin in user_plugins:
        settings.INSTALLED_APPS += ('userPlugins.%s' % plugin.get('name'), )
        settings.STATICFILES_DIRS += (os.path.join('userPlugins', plugin.get('name'), 'static'), )

    execute_from_command_line(sys.argv)
