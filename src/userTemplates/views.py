# -*- coding:utf8 -*-

import shutil
import os
import zipfile

from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect, render
from django.views.generic.edit import FormView

from userTemplates.forms import UploadNewTemplateForm


class IndexView(FormView):
    template_name = 'userTemplates/index.html'
    form_class = UploadNewTemplateForm
    success_url = reverse_lazy('userTemplates:index')

    def get_context_data(self, **kwargs):
        ret = super(IndexView, self).get_context_data(**kwargs)

        if not os.path.isdir(settings.BASE_DIR + '/userTemplates/uploaded'):
            os.mkdir(settings.BASE_DIR + '/userTemplates/uploaded')

        ret.update({
            'templates': os.listdir(
                settings.BASE_DIR + '/userTemplates/uploaded')
        })

        return ret


class UploadTemplateView(FormView):
    template_name = 'userTemplates/upload.html'
    form_class = UploadNewTemplateForm
    success_url = reverse_lazy('userTemplates:index')

    def form_valid(self, form):

        _file = form.cleaned_data['file']
        output_path = os.path.join(settings.BASE_DIR, 'userTemplates/uploaded')

        with zipfile.ZipFile(_file, 'r') as archive:
            for name in archive.namelist():
                archive.extract(name, output_path)

        _file.close()

        return super(UploadTemplateView, self).form_valid(form=form)


def request_delete_template(request, pk):
    return render(request, 'userTemplates/delete.html', {'name' : pk})


def delete_template(request, pk):
    success_url = reverse_lazy('userTemplates:index')
    output_path = os.path.join(settings.BASE_DIR, 'userTemplates/uploaded')
    shutil.rmtree(os.path.join(output_path, pk))
    return redirect(success_url)


# class DeleteTemplateView(ModalDeleteView, DeleteView):
#     def __init__(self, *args, **kwargs):
#         self.model = UserTemplate
#         self.title = "Usuwanie obiektu"
#         self.description = "Jesteś pewny?"
#
#     def dispatch(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         return super(DeleteTemplateView, self).dispatch(
#             request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         reversed = reverse('userTemplates:delete-confirm')
#         self.response = ModalJsonResponseRedirect(reversed)
#         super(DeleteTemplateView, self).delete(request, *args, **kwargs)


# class DeleteTemplateView(DeleteView):
#     template_name = 'userTemplates/delete.html'
#     model = UserTemplate
#     success_url = reverse_lazy('userTemplates:index')
#
#     def post(self, request, *args, **kwargs):
#
#         self.object = self.get_object()
#         output_path = os.path.join(
#             settings.BASE_DIR, 'userTemplates/uploaded')
#
#         import shutil
#         shutil.rmtree(os.path.join(output_path, self.object.name))
#
#         return redirect(self.success_url)
