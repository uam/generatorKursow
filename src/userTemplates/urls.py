from django.conf.urls import patterns, url

from userTemplates import views


urlpatterns = patterns(
    '',

    url(r'^$',
        views.IndexView.as_view(),
        name='index'),
    url(r'^upload/$',
        views.UploadTemplateView.as_view(),
        name='upload-template'),
    url(r'^(?P<pk>[A-Za-z]+)/delete/$',
        views.request_delete_template,
        name="delete"),
    url(r'^(?P<pk>[A-Za-z]+)/delete/confirm$',
        views.delete_template,
        name="delete-confirm"),

)
