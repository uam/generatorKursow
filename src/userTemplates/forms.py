# -*- coding: utf-8 -*-
import zipfile

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms


class UploadNewTemplateForm(forms.Form):

    file = forms.FileField()

    def __init__(self, *args, **kwargs):
        super(UploadNewTemplateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit('submit', 'Zapisz'))

    def clean_file(self, *args, **kwargs):

        _file = self.cleaned_data['file']
        ext = _file.name.split('.')[-1]

        if not zipfile.is_zipfile(_file):
            raise forms.ValidationError(u'Przesłałeś plik z rozszerzeniem %s. Prześlij .zip!' % ext)

        return _file
