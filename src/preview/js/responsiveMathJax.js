var COMPILED = !0, goog = goog || {};
goog.global = this;
goog.DEBUG = !0;
goog.LOCALE = "en";
goog.TRUSTED_SITE = !0;
goog.provide = function (a) {
    if (!COMPILED) {
        if (goog.isProvided_(a))throw Error('Namespace "' + a + '" already declared.');
        delete goog.implicitNamespaces_[a];
        for (var b = a; (b = b.substring(0, b.lastIndexOf("."))) && !goog.getObjectByName(b);)goog.implicitNamespaces_[b] = !0
    }
    goog.exportPath_(a)
};
goog.setTestOnly = function (a) {
    if (COMPILED && !goog.DEBUG)throw a = a || "", Error("Importing test-only code into non-debug environment" + a ? ": " + a : ".");
};
COMPILED || (goog.isProvided_ = function (a) {
    return !goog.implicitNamespaces_[a] && !!goog.getObjectByName(a)
}, goog.implicitNamespaces_ = {});
goog.exportPath_ = function (a, b, c) {
    a = a.split(".");
    c = c || goog.global;
    a[0] in c || !c.execScript || c.execScript("var " + a[0]);
    for (var d; a.length && (d = a.shift());)!a.length && goog.isDef(b) ? c[d] = b : c = c[d] ? c[d] : c[d] = {}
};
goog.getObjectByName = function (a, b) {
    for (var c = a.split("."), d = b || goog.global, e; e = c.shift();)if (goog.isDefAndNotNull(d[e]))d = d[e]; else return null;
    return d
};
goog.globalize = function (a, b) {
    var c = b || goog.global, d;
    for (d in a)c[d] = a[d]
};
goog.addDependency = function (a, b, c) {
    if (!COMPILED) {
        var d;
        a = a.replace(/\\/g, "/");
        for (var e = goog.dependencies_, f = 0; d = b[f]; f++)e.nameToPath[d] = a, a in e.pathToNames || (e.pathToNames[a] = {}), e.pathToNames[a][d] = !0;
        for (d = 0; b = c[d]; d++)a in e.requires || (e.requires[a] = {}), e.requires[a][b] = !0
    }
};
goog.ENABLE_DEBUG_LOADER = !0;
goog.require = function (a) {
    if (!COMPILED && !goog.isProvided_(a)) {
        if (goog.ENABLE_DEBUG_LOADER) {
            var b = goog.getPathFromDeps_(a);
            if (b) {
                goog.included_[b] = !0;
                goog.writeScripts_();
                return
            }
        }
        a = "goog.require could not find: " + a;
        goog.global.console && goog.global.console.error(a);
        throw Error(a);
    }
};
goog.basePath = "";
goog.nullFunction = function () {
};
goog.identityFunction = function (a, b) {
    return a
};
goog.abstractMethod = function () {
    throw Error("unimplemented abstract method");
};
goog.addSingletonGetter = function (a) {
    a.getInstance = function () {
        if (a.instance_)return a.instance_;
        goog.DEBUG && (goog.instantiatedSingletons_[goog.instantiatedSingletons_.length] = a);
        return a.instance_ = new a
    }
};
goog.instantiatedSingletons_ = [];
!COMPILED && goog.ENABLE_DEBUG_LOADER && (goog.included_ = {}, goog.dependencies_ = {
    pathToNames: {},
    nameToPath: {},
    requires: {},
    visited: {},
    written: {}
}, goog.inHtmlDocument_ = function () {
    var a = goog.global.document;
    return "undefined" != typeof a && "write" in a
}, goog.findBasePath_ = function () {
    if (goog.global.CLOSURE_BASE_PATH)goog.basePath = goog.global.CLOSURE_BASE_PATH; else if (goog.inHtmlDocument_())for (var a = goog.global.document.getElementsByTagName("script"), b = a.length - 1; 0 <= b; --b) {
        var c = a[b].src, d = c.lastIndexOf("?"), d =
            -1 == d ? c.length : d;
        if ("base.js" == c.substr(d - 7, 7)) {
            goog.basePath = c.substr(0, d - 7);
            break
        }
    }
}, goog.importScript_ = function (a) {
    var b = goog.global.CLOSURE_IMPORT_SCRIPT || goog.writeScriptTag_;
    !goog.dependencies_.written[a] && b(a) && (goog.dependencies_.written[a] = !0)
}, goog.writeScriptTag_ = function (a) {
    if (goog.inHtmlDocument_()) {
        var b = goog.global.document;
        if ("complete" == b.readyState) {
            if (/\bdeps.js$/.test(a))return !1;
            throw Error('Cannot write "' + a + '" after document load');
        }
        b.write('<script type="text/javascript" src="' +
            a + '">\x3c/script>');
        return !0
    }
    return !1
}, goog.writeScripts_ = function () {
    function a(e) {
        if (!(e in d.written)) {
            if (!(e in d.visited) && (d.visited[e] = !0, e in d.requires))for (var g in d.requires[e])if (!goog.isProvided_(g))if (g in d.nameToPath)a(d.nameToPath[g]); else throw Error("Undefined nameToPath for " + g);
            e in c || (c[e] = !0, b.push(e))
        }
    }

    var b = [], c = {}, d = goog.dependencies_, e;
    for (e in goog.included_)d.written[e] || a(e);
    for (e = 0; e < b.length; e++)if (b[e])goog.importScript_(goog.basePath + b[e]); else throw Error("Undefined script input");
}, goog.getPathFromDeps_ = function (a) {
    return a in goog.dependencies_.nameToPath ? goog.dependencies_.nameToPath[a] : null
}, goog.findBasePath_(), goog.global.CLOSURE_NO_DEPS || goog.importScript_(goog.basePath + "deps.js"));
goog.typeOf = function (a) {
    var b = typeof a;
    if ("object" == b)if (a) {
        if (a instanceof Array)return "array";
        if (a instanceof Object)return b;
        var c = Object.prototype.toString.call(a);
        if ("[object Window]" == c)return "object";
        if ("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice"))return "array";
        if ("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call"))return "function"
    } else return "null";
    else if ("function" == b && "undefined" == typeof a.call)return "object";
    return b
};
goog.isDef = function (a) {
    return void 0 !== a
};
goog.isNull = function (a) {
    return null === a
};
goog.isDefAndNotNull = function (a) {
    return null != a
};
goog.isArray = function (a) {
    return "array" == goog.typeOf(a)
};
goog.isArrayLike = function (a) {
    var b = goog.typeOf(a);
    return "array" == b || "object" == b && "number" == typeof a.length
};
goog.isDateLike = function (a) {
    return goog.isObject(a) && "function" == typeof a.getFullYear
};
goog.isString = function (a) {
    return "string" == typeof a
};
goog.isBoolean = function (a) {
    return "boolean" == typeof a
};
goog.isNumber = function (a) {
    return "number" == typeof a
};
goog.isFunction = function (a) {
    return "function" == goog.typeOf(a)
};
goog.isObject = function (a) {
    var b = typeof a;
    return "object" == b && null != a || "function" == b
};
goog.getUid = function (a) {
    return a[goog.UID_PROPERTY_] || (a[goog.UID_PROPERTY_] = ++goog.uidCounter_)
};
goog.removeUid = function (a) {
    "removeAttribute" in a && a.removeAttribute(goog.UID_PROPERTY_);
    try {
        delete a[goog.UID_PROPERTY_]
    } catch (b) {
    }
};
goog.UID_PROPERTY_ = "closure_uid_" + (1E9 * Math.random() >>> 0);
goog.uidCounter_ = 0;
goog.getHashCode = goog.getUid;
goog.removeHashCode = goog.removeUid;
goog.cloneObject = function (a) {
    var b = goog.typeOf(a);
    if ("object" == b || "array" == b) {
        if (a.clone)return a.clone();
        var b = "array" == b ? [] : {}, c;
        for (c in a)b[c] = goog.cloneObject(a[c]);
        return b
    }
    return a
};
goog.bindNative_ = function (a, b, c) {
    return a.call.apply(a.bind, arguments)
};
goog.bindJs_ = function (a, b, c) {
    if (!a)throw Error();
    if (2 < arguments.length) {
        var d = Array.prototype.slice.call(arguments, 2);
        return function () {
            var c = Array.prototype.slice.call(arguments);
            Array.prototype.unshift.apply(c, d);
            return a.apply(b, c)
        }
    }
    return function () {
        return a.apply(b, arguments)
    }
};
goog.bind = function (a, b, c) {
    Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? goog.bind = goog.bindNative_ : goog.bind = goog.bindJs_;
    return goog.bind.apply(null, arguments)
};
goog.partial = function (a, b) {
    var c = Array.prototype.slice.call(arguments, 1);
    return function () {
        var b = Array.prototype.slice.call(arguments);
        b.unshift.apply(b, c);
        return a.apply(this, b)
    }
};
goog.mixin = function (a, b) {
    for (var c in b)a[c] = b[c]
};
goog.now = goog.TRUSTED_SITE && Date.now || function () {
        return +new Date
    };
goog.globalEval = function (a) {
    if (goog.global.execScript)goog.global.execScript(a, "JavaScript"); else if (goog.global.eval)if (null == goog.evalWorksForGlobals_ && (goog.global.eval("var _et_ = 1;"), "undefined" != typeof goog.global._et_ ? (delete goog.global._et_, goog.evalWorksForGlobals_ = !0) : goog.evalWorksForGlobals_ = !1), goog.evalWorksForGlobals_)goog.global.eval(a); else {
        var b = goog.global.document, c = b.createElement("script");
        c.type = "text/javascript";
        c.defer = !1;
        c.appendChild(b.createTextNode(a));
        b.body.appendChild(c);
        b.body.removeChild(c)
    } else throw Error("goog.globalEval not available");
};
goog.evalWorksForGlobals_ = null;
goog.getCssName = function (a, b) {
    var c = function (a) {
        return goog.cssNameMapping_[a] || a
    }, d = function (a) {
        a = a.split("-");
        for (var b = [], d = 0; d < a.length; d++)b.push(c(a[d]));
        return b.join("-")
    }, d = goog.cssNameMapping_ ? "BY_WHOLE" == goog.cssNameMappingStyle_ ? c : d : function (a) {
        return a
    };
    return b ? a + "-" + d(b) : d(a)
};
goog.setCssNameMapping = function (a, b) {
    goog.cssNameMapping_ = a;
    goog.cssNameMappingStyle_ = b
};
!COMPILED && goog.global.CLOSURE_CSS_NAME_MAPPING && (goog.cssNameMapping_ = goog.global.CLOSURE_CSS_NAME_MAPPING);
goog.getMsg = function (a, b) {
    var c = b || {}, d;
    for (d in c) {
        var e = ("" + c[d]).replace(/\$/g, "$$$$");
        a = a.replace(new RegExp("\\{\\$" + d + "\\}", "gi"), e)
    }
    return a
};
goog.getMsgWithFallback = function (a, b) {
    return a
};
goog.exportSymbol = function (a, b, c) {
    goog.exportPath_(a, b, c)
};
goog.exportProperty = function (a, b, c) {
    a[b] = c
};
goog.inherits = function (a, b) {
    function c() {
    }

    c.prototype = b.prototype;
    a.superClass_ = b.prototype;
    a.prototype = new c;
    a.prototype.constructor = a
};
goog.base = function (a, b, c) {
    var d = arguments.callee.caller;
    if (d.superClass_)return d.superClass_.constructor.apply(a, Array.prototype.slice.call(arguments, 1));
    for (var e = Array.prototype.slice.call(arguments, 2), f = !1, g = a.constructor; g; g = g.superClass_ && g.superClass_.constructor)if (g.prototype[b] === d)f = !0; else if (f)return g.prototype[b].apply(a, e);
    if (a[b] === d)return a.constructor.prototype[b].apply(a, e);
    throw Error("goog.base called from a method of one name to a method of a different name");
};
goog.scope = function (a) {
    a.call(goog.global)
};
var sre = {
    EnrichCase: function () {
    }
};
sre.EnrichCase.prototype.getMathml = function () {
};
sre.AbstractEnrichCase = function (a) {
    this.semantic = a
};
sre.EnrichCaseFactory = function () {
};
sre.EnrichCaseFactory.getEmbellishedCase = function (a) {
    return new sre.EnrichCaseFactory.embellishedCase(a)
};
sre.EnrichCaseFactory.cases = [];
sre.EnrichCaseFactory.getCase = function (a) {
    for (var b = 0, c; c = sre.EnrichCaseFactory.cases[b]; b++)if (c.test(a))return new c.constr(a);
    return null
};
sre.SystemExternal = function () {
};
sre.SystemExternal.jsonPath = function () {
    if ("undefined" !== typeof process && "undefined" !== typeof global)return (process.env.SRE_JSON_PATH || global.SRE_JSON_PATH || process.cwd() + "/mathmaps") + "/"
}();
sre.SystemExternal.require = function (a) {
    return "undefined" !== typeof require ? require(a) : null
};
sre.SystemExternal.documentSupported = function () {
    return "undefined" != typeof document
};
sre.SystemExternal.xmldom = sre.SystemExternal.documentSupported() ? window : sre.SystemExternal.require("xmldom");
sre.SystemExternal.document = sre.SystemExternal.documentSupported() ? document : (new sre.SystemExternal.xmldom.DOMImplementation).createDocument("", "", 0);
sre.SystemExternal.xpath = sre.SystemExternal.documentSupported() ? document : sre.SystemExternal.require("xpath");
sre.SystemExternal.commander = sre.SystemExternal.documentSupported() ? null : sre.SystemExternal.require("commander");
sre.SystemExternal.fs = sre.SystemExternal.documentSupported() ? null : sre.SystemExternal.require("fs");
sre.Debugger = function () {
    this.isActive_ = !1;
    this.outputFunction_ = console.log;
    this.stream_ = null
};
goog.addSingletonGetter(sre.Debugger);
sre.Debugger.prototype.init = function (a) {
    a && this.startDebugFile_(a);
    this.isActive_ = !0
};
sre.Debugger.prototype.startDebugFile_ = function (a) {
    this.stream_ = sre.SystemExternal.fs.createWriteStream(a);
    this.outputFunction_ = goog.bind(function (a) {
        var c = Array.prototype.slice.call(arguments, 0);
        this.stream_.write(c.join(" "));
        this.stream_.write("\n")
    }, this);
    this.stream_.on("error", goog.bind(function (a) {
        console.log("Invalid log file. Debug information sent to console.");
        this.outputFunction_ = console.log
    }, this));
    this.stream_.on("finish", function (a) {
        console.log("Finalizing debug file.")
    })
};
sre.Debugger.prototype.output_ = function (a) {
    this.outputFunction_.apply(this.outputFunction_, ["Speech Rule Engine Debugger:"].concat(a))
};
sre.Debugger.prototype.output = function (a) {
    this.isActive_ && this.output_(Array.prototype.slice.call(arguments, 0))
};
sre.Debugger.prototype.generateOutput = function (a) {
    this.isActive_ && this.output_(a.apply(a, []))
};
sre.Debugger.prototype.exit = function () {
    this.isActive_ && this.stream_ && this.stream_.end();
    this.isActive_ = !1
};
sre.XpathUtil = function () {
};
sre.XpathUtil.xpathSupported = function () {
    return "undefined" == typeof XPathResult ? !1 : !0
};
sre.XpathUtil.xpathEvaluate = sre.XpathUtil.xpathSupported() ? document.evaluate : sre.SystemExternal.xpath.evaluate;
sre.XpathUtil.xpathResult = sre.XpathUtil.xpathSupported() ? XPathResult : sre.SystemExternal.xpath.XPathResult;
sre.XpathUtil.createNSResolver = sre.XpathUtil.xpathSupported() ? document.createNSResolver : sre.SystemExternal.xpath.createNSResolver;
sre.XpathUtil.nameSpaces_ = {
    xhtml: "http://www.w3.org/1999/xhtml",
    mathml: "http://www.w3.org/1998/Math/MathML",
    svg: "http://www.w3.org/2000/svg"
};
sre.XpathUtil.evalXPath = function (a, b) {
    try {
        var c = sre.XpathUtil.xpathEvaluate(a, b, sre.XpathUtil.createNSResolver(b), sre.XpathUtil.xpathResult.ORDERED_NODE_ITERATOR_TYPE, null)
    } catch (d) {
        return []
    }
    for (var e = [], f = c.iterateNext(); f; f = c.iterateNext())e.push(f);
    return e
};
sre.XpathUtil.getLeafNodes = function (a) {
    try {
        var b = sre.XpathUtil.xpathEvaluate(".//*[count(*)=0]", a, null, sre.XpathUtil.xpathResult.ORDERED_NODE_ITERATOR_TYPE, null)
    } catch (c) {
        return []
    }
    a = [];
    for (var d = b.iterateNext(); d; d = b.iterateNext())a.push(d);
    return a
};
sre.XpathUtil.evaluateBoolean = function (a, b) {
    try {
        var c = sre.XpathUtil.xpathEvaluate(a, b, sre.XpathUtil.createNSResolver(b), sre.XpathUtil.xpathResult.BOOLEAN_TYPE, null)
    } catch (d) {
        return !1
    }
    return c.booleanValue
};
sre.XpathUtil.evaluateString = function (a, b) {
    try {
        var c = sre.XpathUtil.xpathEvaluate(a, b, sre.XpathUtil.createNSResolver(b), sre.XpathUtil.xpathResult.STRING_TYPE, null)
    } catch (d) {
        return ""
    }
    return c.stringValue
};
sre.XpathUtil.prefixes_ = function () {
    var a = {};
    Object.keys(sre.XpathUtil.nameSpaces_).map(function (b) {
        a[sre.XpathUtil.nameSpaces_[b]] = b
    });
    return a
}();
sre.XpathUtil.defaultNamespace_ = function (a) {
    if (a && 1 === a.nodeType) {
        !a.prefix && a.namespaceURI && (a.prefix = sre.XpathUtil.prefixes_[a.namespaceURI], a.nodeName = a.prefix + ":" + a.nodeName);
        a = a.childNodes;
        for (var b = 0, c; c = a[b]; b++)sre.XpathUtil.defaultNamespace_(c)
    }
};
sre.XpathUtil.prefixNamespace = function (a) {
    if (a && a._nsMap) {
        for (var b = a.attributes, c = 0, d; d = b[c]; c++)if ("xmlns" == d.name && !d.prefix) {
            var e = sre.XpathUtil.prefixes_[a.namespaceURI];
            e && (d.prefix = d.name, d.localName = e, d.nodeName = d.nodeName + ":" + e)
        }
        a.namespaceURI && (a._nsMap[e] = a.namespaceURI, delete a._nsMap[""], sre.XpathUtil.defaultNamespace_(a))
    }
};
sre.DomUtil = function () {
};
sre.DomUtil.toArray = function (a) {
    for (var b = [], c = 0; c < a.length; c++)b.push(a[c]);
    return b
};
sre.DomUtil.removeEmpty = function (a) {
    return a.filter(function (a) {
        return a
    })
};
sre.DomUtil.trimInput_ = function (a) {
    return a.replace(/>\s+</g, "><").trim()
};
sre.DomUtil.parseInput = function (a, b) {
    var c = b || Error, d = new sre.SystemExternal.xmldom.DOMParser, e = sre.DomUtil.trimInput_(a);
    if (!e)throw new c("Empty input!");
    try {
        var f = d.parseFromString(e, "text/xml").documentElement;
        sre.XpathUtil.prefixNamespace(f);
        return f
    } catch (g) {
        throw new c("Illegal input: " + g.message);
    }
};
sre.DomUtil.NodeType = {
    ELEMENT_NODE: 1,
    ATTRIBUTE_NODE: 2,
    TEXT_NODE: 3,
    CDATA_SECTION_NODE: 4,
    ENTITY_REFERENCE_NODE: 5,
    ENTITY_NODE: 6,
    PROCESSING_INSTRUCTION_NODE: 7,
    COMMENT_NODE: 8,
    DOCUMENT_NODE: 9,
    DOCUMENT_TYPE_NODE: 10,
    DOCUMENT_FRAGMENT_NODE: 11,
    NOTATION_NODE: 12
};
sre.DomUtil.replaceNode = function (a, b) {
    a.parentNode && (a.parentNode.insertBefore(b, a), a.parentNode.removeChild(a))
};
sre.SemanticUtil = function () {
};
sre.SemanticUtil.objectsToKeys = function (a) {
    a = Array.prototype.slice.call(arguments, 0);
    var b = [];
    return b.concat.apply(b, a.map(Object.keys))
};
sre.SemanticUtil.objectsToValues = function (a) {
    a = Array.prototype.slice.call(arguments, 0);
    var b = [];
    a.forEach(function (a) {
        for (var d in a)b.push(a[d])
    });
    return b
};
sre.SemanticUtil.unicodeToNumber = function (a) {
    if (!a || 2 < a.length)return null;
    if (2 == a.length) {
        var b = a.charCodeAt(0);
        a = a.charCodeAt(1);
        return 55296 <= b && 56319 >= b && !isNaN(a) ? 1024 * (b - 55296) + (a - 56320) + 65536 : null
    }
    return a.charCodeAt(0)
};
sre.SemanticUtil.numberToUnicode = function (a) {
    return 65536 <= a ? String.fromCharCode((a - 65536) / 1024 + 55296, (a - 65536) % 1024 + 56320) : String.fromCharCode(a)
};
sre.SemanticUtil.splitUnicode = function (a) {
    a = a.split("");
    for (var b = [], c = 0, d; d = a[c]; c++)"\ud800" <= d && "\udbff" >= d && a[c + 1] ? b.push(d + a[++c]) : b.push(d);
    return b
};
sre.SemanticUtil.tagName = function (a) {
    return a.tagName.toUpperCase()
};
sre.SemanticUtil.LEAFTAGS = ["MO", "MI", "MN", "MTEXT", "MS"];
sre.SemanticUtil.IGNORETAGS = "MERROR MPHANTOM MSPACE MACTION MALIGNGROUP MALIGNMARK NONE MPRESCRIPTS".split(" ");
sre.SemanticUtil.EMPTYTAGS = ["MATH", "MROW", "MPADDED", "MSTYLE"];
sre.SemanticUtil.hasMathTag = function (a) {
    return !!a && "MATH" === sre.SemanticUtil.tagName(a)
};
sre.SemanticUtil.hasIgnoreTag = function (a) {
    return !!a && -1 !== sre.SemanticUtil.IGNORETAGS.indexOf(sre.SemanticUtil.tagName(a))
};
sre.SemanticUtil.hasEmptyTag = function (a) {
    return !!a && -1 !== sre.SemanticUtil.EMPTYTAGS.indexOf(sre.SemanticUtil.tagName(a))
};
sre.SemanticUtil.purgeNodes = function (a) {
    for (var b = [], c = 0, d; d = a[c]; c++) {
        var e = sre.SemanticUtil.tagName(d);
        -1 == sre.SemanticUtil.IGNORETAGS.indexOf(e) && (-1 != sre.SemanticUtil.EMPTYTAGS.indexOf(e) && 0 == d.childNodes.length || b.push(d))
    }
    return b
};
sre.SemanticAttr = function () {
    this.generalPunctuations = '!"#%&:;?@\\\u00a1\u00a7\u00b6\u00bf\u2017\u2020\u2021\u2022\u2023\u2024\u2025\u2027\u2030\u2031\u2038\u203b\u203c\u203d\u203e\u2041\u2042\u2043\u2047\u2048\u2049\u204b\u204c\u204d\u204e\u204f\u2050\u2051\u2053\u2055\u2056\u2058\u2059\u205a\u205b\u205c\u205d\u205e\ufe10\ufe13\ufe14\ufe15\ufe16\ufe30\ufe45\ufe46\ufe49\ufe4a\ufe4b\ufe4c\ufe50\ufe54\ufe55\ufe56\ufe57\ufe5f\ufe60\ufe61\ufe68\ufe6a\ufe6b\uff01\uff02\uff03\uff05\uff06\uff07\uff0a\uff0c\uff0f\uff1a\uff1b\uff1f\uff20\uff3c'.split("");
    this.invisibleComma_ = sre.SemanticUtil.numberToUnicode(8291);
    this.commas = [",", this.invisibleComma_];
    this.ellipses = "\u2026\u22ee\u22ef\u22f0\u22f1\ufe19".split("");
    this.fullStops = [".", "\ufe52", "\uff0e"];
    this.dashes = "\u2012\u2013\u2014\u2015\u301c\ufe31\ufe32\ufe58".split("");
    this.primes = "'\u2032\u2033\u2034\u2035\u2036\u2037\u2057".split("");
    this.openClosePairs = {
        "(": ")",
        "[": "]",
        "{": "}",
        "\u2045": "\u2046",
        "\u2329": "\u232a",
        "\u2768": "\u2769",
        "\u276a": "\u276b",
        "\u276c": "\u276d",
        "\u276e": "\u276f",
        "\u2770": "\u2771",
        "\u2772": "\u2773",
        "\u2774": "\u2775",
        "\u27c5": "\u27c6",
        "\u27e6": "\u27e7",
        "\u27e8": "\u27e9",
        "\u27ea": "\u27eb",
        "\u27ec": "\u27ed",
        "\u27ee": "\u27ef",
        "\u2983": "\u2984",
        "\u2985": "\u2986",
        "\u2987": "\u2988",
        "\u2989": "\u298a",
        "\u298b": "\u298c",
        "\u298d": "\u298e",
        "\u298f": "\u2990",
        "\u2991": "\u2992",
        "\u2993": "\u2994",
        "\u2995": "\u2996",
        "\u2997": "\u2998",
        "\u29d8": "\u29d9",
        "\u29da": "\u29db",
        "\u29fc": "\u29fd",
        "\u2e22": "\u2e23",
        "\u2e24": "\u2e25",
        "\u2e26": "\u2e27",
        "\u2e28": "\u2e29",
        "\u3008": "\u3009",
        "\u300a": "\u300b",
        "\u300c": "\u300d",
        "\u300e": "\u300f",
        "\u3010": "\u3011",
        "\u3014": "\u3015",
        "\u3016": "\u3017",
        "\u3018": "\u3019",
        "\u301a": "\u301b",
        "\u301d": "\u301e",
        "\ufd3e": "\ufd3f",
        "\ufe17": "\ufe18",
        "\ufe59": "\ufe5a",
        "\ufe5b": "\ufe5c",
        "\ufe5d": "\ufe5e",
        "\uff08": "\uff09",
        "\uff3b": "\uff3d",
        "\uff5b": "\uff5d",
        "\uff5f": "\uff60",
        "\uff62": "\uff63",
        "\u2308": "\u2309",
        "\u230a": "\u230b",
        "\u230c": "\u230d",
        "\u230e": "\u230f",
        "\u231c": "\u231d",
        "\u231e": "\u231f",
        "\u239b": "\u239e",
        "\u239c": "\u239f",
        "\u239d": "\u23a0",
        "\u23a1": "\u23a4",
        "\u23a2": "\u23a5",
        "\u23a3": "\u23a6",
        "\u23a7": "\u23ab",
        "\u23a8": "\u23ac",
        "\u23a9": "\u23ad",
        "\u23b0": "\u23b1",
        "\u23b8": "\u23b9"
    };
    this.topBottomPairs = {
        "\u23b4": "\u23b5",
        "\u23dc": "\u23dd",
        "\u23de": "\u23df",
        "\u23e0": "\u23e1",
        "\ufe35": "\ufe36",
        "\ufe37": "\ufe38",
        "\ufe39": "\ufe3a",
        "\ufe3b": "\ufe3c",
        "\ufe3d": "\ufe3e",
        "\ufe3f": "\ufe40",
        "\ufe41": "\ufe42",
        "\ufe43": "\ufe44",
        "\ufe47": "\ufe48"
    };
    this.leftFences = sre.SemanticUtil.objectsToKeys(this.openClosePairs);
    this.rightFences = sre.SemanticUtil.objectsToValues(this.openClosePairs);
    this.rightFences.push("\u301f");
    this.topFences = sre.SemanticUtil.objectsToKeys(this.topBottomPairs);
    this.bottomFences = sre.SemanticUtil.objectsToValues(this.topBottomPairs);
    this.neutralFences = "|\u00a6\u2016\u2758\u2980\u2af4\uffe4\uff5c".split("");
    this.fences = this.neutralFences.concat(this.leftFences, this.rightFences, this.topFences, this.bottomFences);
    this.capitalLatin = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
    this.smallLatin = "abcdefghijklmnopqrstuvwxyz\u0131\u0237".split("");
    this.capitalLatinFullWidth =
        "\uff21\uff22\uff23\uff24\uff25\uff26\uff27\uff28\uff29\uff2a\uff2b\uff2c\uff2d\uff2e\uff2f\uff30\uff31\uff32\uff33\uff34\uff35\uff36\uff37\uff38\uff39\uff3a".split("");
    this.smallLatinFullWidth = "\uff41\uff42\uff43\uff44\uff45\uff46\uff47\uff48\uff49\uff4a\uff4b\uff4c\uff4d\uff4e\uff4f\uff50\uff51\uff52\uff53\uff54\uff55\uff56\uff57\uff58\uff59\uff5a".split("");
    this.capitalLatinBold = "\ud835\udc00 \ud835\udc01 \ud835\udc02 \ud835\udc03 \ud835\udc04 \ud835\udc05 \ud835\udc06 \ud835\udc07 \ud835\udc08 \ud835\udc09 \ud835\udc0a \ud835\udc0b \ud835\udc0c \ud835\udc0d \ud835\udc0e \ud835\udc0f \ud835\udc10 \ud835\udc11 \ud835\udc12 \ud835\udc13 \ud835\udc14 \ud835\udc15 \ud835\udc16 \ud835\udc17 \ud835\udc18 \ud835\udc19".split(" ");
    this.smallLatinBold = "\ud835\udc1a \ud835\udc1b \ud835\udc1c \ud835\udc1d \ud835\udc1e \ud835\udc1f \ud835\udc20 \ud835\udc21 \ud835\udc22 \ud835\udc23 \ud835\udc24 \ud835\udc25 \ud835\udc26 \ud835\udc27 \ud835\udc28 \ud835\udc29 \ud835\udc2a \ud835\udc2b \ud835\udc2c \ud835\udc2d \ud835\udc2e \ud835\udc2f \ud835\udc30 \ud835\udc31 \ud835\udc32 \ud835\udc33".split(" ");
    this.capitalLatinItalic = "\ud835\udc34 \ud835\udc35 \ud835\udc36 \ud835\udc37 \ud835\udc38 \ud835\udc39 \ud835\udc3a \ud835\udc3b \ud835\udc3c \ud835\udc3d \ud835\udc3e \ud835\udc3f \ud835\udc40 \ud835\udc41 \ud835\udc42 \ud835\udc43 \ud835\udc44 \ud835\udc45 \ud835\udc46 \ud835\udc47 \ud835\udc48 \ud835\udc49 \ud835\udc4a \ud835\udc4b \ud835\udc4c \ud835\udc4d".split(" ");
    this.smallLatinItalic = "\ud835\udc4e \ud835\udc4f \ud835\udc50 \ud835\udc51 \ud835\udc52 \ud835\udc53 \ud835\udc54 \u210e \ud835\udc56 \ud835\udc57 \ud835\udc58 \ud835\udc59 \ud835\udc5a \ud835\udc5b \ud835\udc5c \ud835\udc5d \ud835\udc5e \ud835\udc5f \ud835\udc60 \ud835\udc61 \ud835\udc62 \ud835\udc63 \ud835\udc64 \ud835\udc65 \ud835\udc66 \ud835\udc67 \ud835\udea4 \ud835\udea5".split(" ");
    this.capitalLatinScript = "\ud835\udc9c \u212c \ud835\udc9e \ud835\udc9f \u2130 \u2131 \ud835\udca2 \u210b \u2110 \ud835\udca5 \ud835\udca6 \u2112 \u2133 \ud835\udca9 \ud835\udcaa \ud835\udcab \ud835\udcac \u211b \ud835\udcae \ud835\udcaf \ud835\udcb0 \ud835\udcb1 \ud835\udcb2 \ud835\udcb3 \ud835\udcb4 \ud835\udcb5 \u2118".split(" ");
    this.smallLatinScript = "\ud835\udcb6 \ud835\udcb7 \ud835\udcb8 \ud835\udcb9 \u212f \ud835\udcbb \u210a \ud835\udcbd \ud835\udcbe \ud835\udcbf \ud835\udcc0 \ud835\udcc1 \ud835\udcc2 \ud835\udcc3 \u2134 \ud835\udcc5 \ud835\udcc6 \ud835\udcc7 \ud835\udcc8 \ud835\udcc9 \ud835\udcca \ud835\udccb \ud835\udccc \ud835\udccd \ud835\udcce \ud835\udccf \u2113".split(" ");
    this.capitalLatinBoldScript = "\ud835\udcd0 \ud835\udcd1 \ud835\udcd2 \ud835\udcd3 \ud835\udcd4 \ud835\udcd5 \ud835\udcd6 \ud835\udcd7 \ud835\udcd8 \ud835\udcd9 \ud835\udcda \ud835\udcdb \ud835\udcdc \ud835\udcdd \ud835\udcde \ud835\udcdf \ud835\udce0 \ud835\udce1 \ud835\udce2 \ud835\udce3 \ud835\udce4 \ud835\udce5 \ud835\udce6 \ud835\udce7 \ud835\udce8 \ud835\udce9".split(" ");
    this.smallLatinBoldScript = "\ud835\udcea \ud835\udceb \ud835\udcec \ud835\udced \ud835\udcee \ud835\udcef \ud835\udcf0 \ud835\udcf1 \ud835\udcf2 \ud835\udcf3 \ud835\udcf4 \ud835\udcf5 \ud835\udcf6 \ud835\udcf7 \ud835\udcf8 \ud835\udcf9 \ud835\udcfa \ud835\udcfb \ud835\udcfc \ud835\udcfd \ud835\udcfe \ud835\udcff \ud835\udd00 \ud835\udd01 \ud835\udd02 \ud835\udd03".split(" ");
    this.capitalLatinFraktur = "\ud835\udd04 \ud835\udd05 \u212d \ud835\udd07 \ud835\udd08 \ud835\udd09 \ud835\udd0a \u210c \u2111 \ud835\udd0d \ud835\udd0e \ud835\udd0f \ud835\udd10 \ud835\udd11 \ud835\udd12 \ud835\udd13 \ud835\udd14 \u211c \ud835\udd16 \ud835\udd17 \ud835\udd18 \ud835\udd19 \ud835\udd1a \ud835\udd1b \ud835\udd1c \u2128".split(" ");
    this.smallLatinFraktur = "\ud835\udd1e \ud835\udd1f \ud835\udd20 \ud835\udd21 \ud835\udd22 \ud835\udd23 \ud835\udd24 \ud835\udd25 \ud835\udd26 \ud835\udd27 \ud835\udd28 \ud835\udd29 \ud835\udd2a \ud835\udd2b \ud835\udd2c \ud835\udd2d \ud835\udd2e \ud835\udd2f \ud835\udd30 \ud835\udd31 \ud835\udd32 \ud835\udd33 \ud835\udd34 \ud835\udd35 \ud835\udd36 \ud835\udd37".split(" ");
    this.capitalLatinDoubleStruck = "\ud835\udd38 \ud835\udd39 \u2102 \ud835\udd3b \ud835\udd3c \ud835\udd3d \ud835\udd3e \u210d \ud835\udd40 \ud835\udd41 \ud835\udd42 \ud835\udd43 \ud835\udd44 \u2115 \ud835\udd46 \u2119 \u211a \u211d \ud835\udd4a \ud835\udd4b \ud835\udd4c \ud835\udd4d \ud835\udd4e \ud835\udd4f \ud835\udd50 \u2124".split(" ");
    this.smallLatinDoubleStruck = "\ud835\udd52 \ud835\udd53 \ud835\udd54 \ud835\udd55 \ud835\udd56 \ud835\udd57 \ud835\udd58 \ud835\udd59 \ud835\udd5a \ud835\udd5b \ud835\udd5c \ud835\udd5d \ud835\udd5e \ud835\udd5f \ud835\udd60 \ud835\udd61 \ud835\udd62 \ud835\udd63 \ud835\udd64 \ud835\udd65 \ud835\udd66 \ud835\udd67 \ud835\udd68 \ud835\udd69 \ud835\udd6a \ud835\udd6b".split(" ");
    this.capitalLatinBoldFraktur = "\ud835\udd6c \ud835\udd6d \ud835\udd6e \ud835\udd6f \ud835\udd70 \ud835\udd71 \ud835\udd72 \ud835\udd73 \ud835\udd74 \ud835\udd75 \ud835\udd76 \ud835\udd77 \ud835\udd78 \ud835\udd79 \ud835\udd7a \ud835\udd7b \ud835\udd7c \ud835\udd7d \ud835\udd7e \ud835\udd7f \ud835\udd80 \ud835\udd81 \ud835\udd82 \ud835\udd83 \ud835\udd84 \ud835\udd85".split(" ");
    this.smallLatinBoldFraktur = "\ud835\udd86 \ud835\udd87 \ud835\udd88 \ud835\udd89 \ud835\udd8a \ud835\udd8b \ud835\udd8c \ud835\udd8d \ud835\udd8e \ud835\udd8f \ud835\udd90 \ud835\udd91 \ud835\udd92 \ud835\udd93 \ud835\udd94 \ud835\udd95 \ud835\udd96 \ud835\udd97 \ud835\udd98 \ud835\udd99 \ud835\udd9a \ud835\udd9b \ud835\udd9c \ud835\udd9d \ud835\udd9e \ud835\udd9f".split(" ");
    this.capitalLatinSansSerif = "\ud835\udda0 \ud835\udda1 \ud835\udda2 \ud835\udda3 \ud835\udda4 \ud835\udda5 \ud835\udda6 \ud835\udda7 \ud835\udda8 \ud835\udda9 \ud835\uddaa \ud835\uddab \ud835\uddac \ud835\uddad \ud835\uddae \ud835\uddaf \ud835\uddb0 \ud835\uddb1 \ud835\uddb2 \ud835\uddb3 \ud835\uddb4 \ud835\uddb5 \ud835\uddb6 \ud835\uddb7 \ud835\uddb8 \ud835\uddb9".split(" ");
    this.smallLatinSansSerif = "\ud835\uddba \ud835\uddbb \ud835\uddbc \ud835\uddbd \ud835\uddbe \ud835\uddbf \ud835\uddc0 \ud835\uddc1 \ud835\uddc2 \ud835\uddc3 \ud835\uddc4 \ud835\uddc5 \ud835\uddc6 \ud835\uddc7 \ud835\uddc8 \ud835\uddc9 \ud835\uddca \ud835\uddcb \ud835\uddcc \ud835\uddcd \ud835\uddce \ud835\uddcf \ud835\uddd0 \ud835\uddd1 \ud835\uddd2 \ud835\uddd3".split(" ");
    this.capitalLatinSansSerifBold = "\ud835\uddd4 \ud835\uddd5 \ud835\uddd6 \ud835\uddd7 \ud835\uddd8 \ud835\uddd9 \ud835\uddda \ud835\udddb \ud835\udddc \ud835\udddd \ud835\uddde \ud835\udddf \ud835\udde0 \ud835\udde1 \ud835\udde2 \ud835\udde3 \ud835\udde4 \ud835\udde5 \ud835\udde6 \ud835\udde7 \ud835\udde8 \ud835\udde9 \ud835\uddea \ud835\uddeb \ud835\uddec \ud835\udded".split(" ");
    this.smallLatinSansSerifBold = "\ud835\uddee \ud835\uddef \ud835\uddf0 \ud835\uddf1 \ud835\uddf2 \ud835\uddf3 \ud835\uddf4 \ud835\uddf5 \ud835\uddf6 \ud835\uddf7 \ud835\uddf8 \ud835\uddf9 \ud835\uddfa \ud835\uddfb \ud835\uddfc \ud835\uddfd \ud835\uddfe \ud835\uddff \ud835\ude00 \ud835\ude01 \ud835\ude02 \ud835\ude03 \ud835\ude04 \ud835\ude05 \ud835\ude06 \ud835\ude07".split(" ");
    this.capitalLatinSansSerifItalic = "\ud835\ude08 \ud835\ude09 \ud835\ude0a \ud835\ude0b \ud835\ude0c \ud835\ude0d \ud835\ude0e \ud835\ude0f \ud835\ude10 \ud835\ude11 \ud835\ude12 \ud835\ude13 \ud835\ude14 \ud835\ude15 \ud835\ude16 \ud835\ude17 \ud835\ude18 \ud835\ude19 \ud835\ude1a \ud835\ude1b \ud835\ude1c \ud835\ude1d \ud835\ude1e \ud835\ude1f \ud835\ude20 \ud835\ude21".split(" ");
    this.smallLatinSansSerifItalic = "\ud835\ude22 \ud835\ude23 \ud835\ude24 \ud835\ude25 \ud835\ude26 \ud835\ude27 \ud835\ude28 \ud835\ude29 \ud835\ude2a \ud835\ude2b \ud835\ude2c \ud835\ude2d \ud835\ude2e \ud835\ude2f \ud835\ude30 \ud835\ude31 \ud835\ude32 \ud835\ude33 \ud835\ude34 \ud835\ude35 \ud835\ude36 \ud835\ude37 \ud835\ude38 \ud835\ude39 \ud835\ude3a \ud835\ude3b".split(" ");
    this.capitalLatinMonospace = "\ud835\ude70 \ud835\ude71 \ud835\ude72 \ud835\ude73 \ud835\ude74 \ud835\ude75 \ud835\ude76 \ud835\ude77 \ud835\ude78 \ud835\ude79 \ud835\ude7a \ud835\ude7b \ud835\ude7c \ud835\ude7d \ud835\ude7e \ud835\ude7f \ud835\ude80 \ud835\ude81 \ud835\ude82 \ud835\ude83 \ud835\ude84 \ud835\ude85 \ud835\ude86 \ud835\ude87 \ud835\ude88 \ud835\ude89".split(" ");
    this.smallLatinMonospace = "\ud835\ude8a \ud835\ude8b \ud835\ude8c \ud835\ude8d \ud835\ude8e \ud835\ude8f \ud835\ude90 \ud835\ude91 \ud835\ude92 \ud835\ude93 \ud835\ude94 \ud835\ude95 \ud835\ude96 \ud835\ude97 \ud835\ude98 \ud835\ude99 \ud835\ude9a \ud835\ude9b \ud835\ude9c \ud835\ude9d \ud835\ude9e \ud835\ude9f \ud835\udea0 \ud835\udea1 \ud835\udea2 \ud835\udea3".split(" ");
    this.latinDoubleStruckItalic = ["\u2145", "\u2146", "\u2147", "\u2148", "\u2149"];
    this.capitalGreek = "\u0391\u0392\u0393\u0394\u0395\u0396\u0397\u0398\u0399\u039a\u039b\u039c\u039d\u039e\u039f\u03a0\u03a1\u03a3\u03a4\u03a5\u03a6\u03a7\u03a8\u03a9".split("");
    this.smallGreek = "\u03b1\u03b2\u03b3\u03b4\u03b5\u03b6\u03b7\u03b8\u03b9\u03ba\u03bb\u03bc\u03bd\u03be\u03bf\u03c0\u03c1\u03c2\u03c3\u03c4\u03c5\u03c6\u03c7\u03c8\u03c9".split("");
    this.capitalGreekBold = "\ud835\udea8 \ud835\udea9 \ud835\udeaa \ud835\udeab \ud835\udeac \ud835\udead \ud835\udeae \ud835\udeaf \ud835\udeb0 \ud835\udeb1 \ud835\udeb2 \ud835\udeb3 \ud835\udeb4 \ud835\udeb5 \ud835\udeb6 \ud835\udeb7 \ud835\udeb8 \ud835\udeba \ud835\udebb \ud835\udebc \ud835\udebd \ud835\udebe \ud835\udebf \ud835\udec0".split(" ");
    this.smallGreekBold = "\ud835\udec2 \ud835\udec3 \ud835\udec4 \ud835\udec5 \ud835\udec6 \ud835\udec7 \ud835\udec8 \ud835\udec9 \ud835\udeca \ud835\udecb \ud835\udecc \ud835\udecd \ud835\udece \ud835\udecf \ud835\uded0 \ud835\uded1 \ud835\uded2 \ud835\uded3 \ud835\uded4 \ud835\uded5 \ud835\uded6 \ud835\uded7 \ud835\uded8 \ud835\uded9 \ud835\udeda".split(" ");
    this.capitalGreekItalic = "\ud835\udee2 \ud835\udee3 \ud835\udee4 \ud835\udee5 \ud835\udee6 \ud835\udee7 \ud835\udee8 \ud835\udee9 \ud835\udeea \ud835\udeeb \ud835\udeec \ud835\udeed \ud835\udeee \ud835\udeef \ud835\udef0 \ud835\udef1 \ud835\udef2 \ud835\udef4 \ud835\udef5 \ud835\udef6 \ud835\udef7 \ud835\udef8 \ud835\udef9 \ud835\udefa".split(" ");
    this.smallGreekItalic = "\ud835\udefc \ud835\udefd \ud835\udefe \ud835\udeff \ud835\udf00 \ud835\udf01 \ud835\udf02 \ud835\udf03 \ud835\udf04 \ud835\udf05 \ud835\udf06 \ud835\udf07 \ud835\udf08 \ud835\udf09 \ud835\udf0a \ud835\udf0b \ud835\udf0c \ud835\udf0d \ud835\udf0e \ud835\udf0f \ud835\udf10 \ud835\udf11 \ud835\udf12 \ud835\udf13 \ud835\udf14".split(" ");
    this.capitalGreekSansSerifBold = "\ud835\udf56 \ud835\udf57 \ud835\udf58 \ud835\udf59 \ud835\udf5a \ud835\udf5b \ud835\udf5c \ud835\udf5d \ud835\udf5e \ud835\udf5f \ud835\udf60 \ud835\udf61 \ud835\udf62 \ud835\udf63 \ud835\udf64 \ud835\udf65 \ud835\udf66 \ud835\udf68 \ud835\udf69 \ud835\udf6a \ud835\udf6b \ud835\udf6c \ud835\udf6d \ud835\udf6e".split(" ");
    this.smallGreekSansSerifBold = "\ud835\udf70 \ud835\udf71 \ud835\udf72 \ud835\udf73 \ud835\udf74 \ud835\udf75 \ud835\udf76 \ud835\udf77 \ud835\udf78 \ud835\udf79 \ud835\udf7a \ud835\udf7b \ud835\udf7c \ud835\udf7d \ud835\udf7e \ud835\udf7f \ud835\udf80 \ud835\udf81 \ud835\udf82 \ud835\udf83 \ud835\udf84 \ud835\udf85 \ud835\udf86 \ud835\udf87 \ud835\udf88".split(" ");
    this.greekDoubleStruck = ["\u213c", "\u213d", "\u213e", "\u213f"];
    this.hebrewLetters = ["\u2135", "\u2136", "\u2137", "\u2138"];
    this.allLetters =
        this.capitalLatin.concat(this.smallLatin, this.capitalLatinFullWidth, this.smallLatinFullWidth, this.capitalLatinBold, this.smallLatinBold, this.capitalLatinItalic, this.smallLatinItalic, this.capitalLatinScript, this.smallLatinScript, this.capitalLatinBoldScript, this.smallLatinBoldScript, this.capitalLatinFraktur, this.smallLatinFraktur, this.capitalLatinDoubleStruck, this.smallLatinDoubleStruck, this.capitalLatinBoldFraktur, this.smallLatinBoldFraktur, this.capitalLatinSansSerif, this.smallLatinSansSerif, this.capitalLatinSansSerifBold,
            this.smallLatinSansSerifBold, this.capitalLatinSansSerifItalic, this.smallLatinSansSerifItalic, this.capitalLatinMonospace, this.smallLatinMonospace, this.latinDoubleStruckItalic, this.capitalGreek, this.smallGreek, this.capitalGreekBold, this.smallGreekBold, this.capitalGreekItalic, this.smallGreekItalic, this.capitalGreekSansSerifBold, this.smallGreekSansSerifBold, this.greekDoubleStruck, this.hebrewLetters);
    this.additions = "+\u00b1\u2213\u2214\u2227\u2228\u2229\u222a\u228c\u2293\u2294\u229d\u229e\u22a4\u22a5\u22ba\u22bb\u22bc\u22c4\u22ce\u22cf\u22d2\u22d3\u25b3\u25b7\u25bd\u25c1\u2a5e\u2295".split("");
    this.invisiblePlus_ = sre.SemanticUtil.numberToUnicode(8292);
    this.additions.push(this.invisiblePlus_);
    this.multiplications = "\u2020\u2021\u2210\u2217\u2218\u2219\u2240\u229a\u229b\u22a0\u22a1\u22c5\u22c6\u22c7\u22c9\u22ca\u22cb\u22cc\u25cb\u00b7*".split("");
    this.invisibleTimes_ = sre.SemanticUtil.numberToUnicode(8290);
    this.multiplications.push(this.invisibleTimes_);
    this.subtractions = "-\u2052\u207b\u208b\u2212\u2216\u2238\u2242\u2296\u229f\u2796\u2a29\u2a2a\u2a2b\u2a2c\u2a3a\u2a41\u2a6c\ufe63\uff0d\u2010\u2011".split("");
    this.divisions = "/\u00f7\u2044\u2215\u2298\u27cc\u29bc\u2a38".split("");
    this.functionApplication_ = sre.SemanticUtil.numberToUnicode(8289);
    this.equalities = "=~\u207c\u208c\u223c\u223d\u2243\u2245\u2248\u224a\u224b\u224c\u224d\u224e\u2251\u2252\u2253\u2254\u2255\u2256\u2257\u2258\u2259\u225a\u225b\u225c\u225d\u225e\u225f\u2261\u2263\u29e4\u2a66\u2a6e\u2a6f\u2a70\u2a71\u2a72\u2a73\u2a74\u2a75\u2a76\u2a77\u2a78\u22d5\u2a6d\u2a6a\u2a6b\u2a6c\ufe66\uff1d".split("");
    this.inequalities = "<>\u2241\u2242\u2244\u2246\u2247\u2249\u224f\u2250\u2260\u2262\u2264\u2265\u2266\u2267\u2268\u2269\u226a\u226b\u226c\u226d\u226e\u226f\u2270\u2271\u2272\u2273\u2274\u2275\u2276\u2277\u2278\u2279\u227a\u227b\u227c\u227d\u227e\u227f\u2280\u2281\u22d6\u22d7\u22d8\u22d9\u22da\u22db\u22dc\u22dd\u22de\u22df\u22e0\u22e1\u22e2\u22e3\u22e4\u22e5\u22e6\u22e7\u22e8\u22e9\u2a79\u2a7a\u2a7b\u2a7c\u2a7d\u2a7e\u2a7f\u2a80\u2a81\u2a82\u2a83\u2a84\u2a85\u2a86\u2a87\u2a88\u2a89\u2a8a\u2a8b\u2a8c\u2a8d\u2a8e\u2a8f\u2a90\u2a91\u2a92\u2a93\u2a94\u2a95\u2a96\u2a97\u2a98\u2a99\u2a9a\u2a9b\u2a9c\u2a9d\u2a9e\u2a9f\u2aa0\u2aa1\u2aa2\u2aa3\u2aa4\u2aa5\u2aa6\u2aa7\u2aa8\u2aa9\u2aaa\u2aab\u2aac\u2aad\u2aae\u2aaf\u2ab0\u2ab1\u2ab2\u2ab3\u2ab4\u2ab5\u2ab6\u2ab7\u2ab8\u2ab9\u2aba\u2abb\u2abc\u2af7\u2af8\u2af9\u2afa\u29c0\u29c1\ufe64\ufe65\uff1c\uff1e".split("");
    this.relations = [];
    this.arrows = "\u2190\u2191\u2192\u2193\u2194\u2195\u2196\u2197\u2198\u2199\u219a\u219b\u219c\u219d\u219e\u219f\u21a0\u21a1\u21a2\u21a3\u21a4\u21a5\u21a6\u21a7\u21a8\u21a9\u21aa\u21ab\u21ac\u21ad\u21ae\u21af\u21b0\u21b1\u21b2\u21b3\u21b4\u21b5\u21b6\u21b7\u21b8\u21b9\u21ba\u21bb\u21c4\u21c5\u21c6\u21c7\u21c8\u21c9\u21ca\u21cd\u21ce\u21cf\u21d0\u21d1\u21d2\u21d3\u21d4\u21d5\u21d6\u21d7\u21d8\u21d9\u21da\u21db\u21dc\u21dd\u21de\u21df\u21e0\u21e1\u21e2\u21e3\u21e4\u21e5\u21e6\u21e7\u21e8\u21e9\u21ea\u21eb\u21ec\u21ed\u21ee\u21ef\u21f0\u21f1\u21f2\u21f3\u21f4\u21f5\u21f6\u21f7\u21f8\u21f9\u21fa\u21fb\u21fc\u21fd\u21fe\u21ff\u2301\u2303\u2304\u2324\u238b\u2794\u2798\u2799\u279a\u279b\u279c\u279d\u279e\u279f\u27a0\u27a1\u27a2\u27a3\u27a4\u27a5\u27a6\u27a7\u27a8\u27a9\u27aa\u27ab\u27ac\u27ad\u27ae\u27af\u27b1\u27b2\u27b3\u27b4\u27b5\u27b6\u27b7\u27b8\u27b9\u27ba\u27bb\u27bc\u27bd\u27be\u27f0\u27f1\u27f2\u27f3\u27f4\u27f5\u27f6\u27f7\u27f8\u27f9\u27fa\u27fb\u27fc\u27fd\u27fe\u27ff\u2900\u2901\u2902\u2903\u2904\u2905\u2906\u2907\u2908\u2909\u290a\u290b\u290c\u290d\u290e\u290f\u2910\u2911\u2912\u2913\u2914\u2915\u2916\u2917\u2918\u2919\u291a\u291b\u291c\u291d\u291e\u291f\u2920\u2921\u2922\u2923\u2924\u2925\u2926\u2927\u2928\u2929\u292a\u292d\u292e\u292f\u2930\u2931\u2932\u2933\u2934\u2935\u2936\u2937\u2938\u2939\u293a\u293b\u293c\u293d\u293e\u293f\u2940\u2941\u2942\u2943\u2944\u2945\u2946\u2947\u2948\u2949\u2970\u2971\u2972\u2973\u2974\u2975\u2976\u2977\u2978\u2979\u297a\u297b\u29b3\u29b4\u29bd\u29ea\u29ec\u29ed\u2a17\u2b00\u2b01\u2b02\u2b03\u2b04\u2b05\u2b06\u2b07\u2b08\u2b09\u2b0a\u2b0b\u2b0c\u2b0d\u2b0e\u2b0f\u2b10\u2b11\u2b30\u2b31\u2b32\u2b33\u2b34\u2b35\u2b36\u2b37\u2b38\u2b39\u2b3a\u2b3b\u2b3c\u2b3d\u2b3e\u2b3f\u2b40\u2b41\u2b42\u2b43\u2b44\u2b45\u2b46\u2b47\u2b48\u2b49\u2b4a\u2b4b\u2b4c\uffe9\uffea\uffeb\uffec\u21bc\u21bd\u21be\u21bf\u21c0\u21c1\u21c2\u21c3\u21cb\u21cc\u294a\u294b\u294c\u294d\u294e\u294f\u2950\u2951\u2952\u2953\u2954\u2955\u2956\u2957\u2958\u2959\u295a\u295b\u295c\u295d\u295e\u295f\u2960\u2961\u2962\u2963\u2964\u2965\u2966\u2967\u2968\u2969\u296a\u296b\u296c\u296d\u296e\u296f\u297c\u297d\u297e\u297f".split("");
    this.sumOps = "\u2140\u220f\u2210\u2211\u22c0\u22c1\u22c2\u22c3\u2a00\u2a01\u2a02\u2a03\u2a04\u2a05\u2a06\u2a07\u2a08\u2a09\u2a0a\u2a0b\u2afc\u2aff".split("");
    this.intOps = "\u222b\u222c\u222d\u222e\u222f\u2230\u2231\u2232\u2233\u2a0c\u2a0d\u2a0e\u2a0f\u2a10\u2a11\u2a12\u2a13\u2a14\u2a15\u2a16\u2a17\u2a18\u2a19\u2a1a\u2a1b\u2a1c".split("");
    this.prefixOps = ["\u2200", "\u2203"];
    this.operatorBits = "\u2320\u2321\u23b6\u23aa\u23ae\u23af\u23b2\u23b3\u23b7".split("");
    this.digitsNormal = "0123456789".split("");
    this.digitsFullWidth =
        "\uff10\uff11\uff12\uff13\uff14\uff15\uff16\uff17\uff18\uff19".split("");
    this.digitsBold = "\ud835\udfce \ud835\udfcf \ud835\udfd0 \ud835\udfd1 \ud835\udfd2 \ud835\udfd3 \ud835\udfd4 \ud835\udfd5 \ud835\udfd6 \ud835\udfd7".split(" ");
    this.digitsDoubleStruck = "\ud835\udfd8 \ud835\udfd9 \ud835\udfda \ud835\udfdb \ud835\udfdc \ud835\udfdd \ud835\udfde \ud835\udfdf \ud835\udfe0 \ud835\udfe1".split(" ");
    this.digitsSansSerif = "\ud835\udfe2 \ud835\udfe3 \ud835\udfe4 \ud835\udfe5 \ud835\udfe6 \ud835\udfe7 \ud835\udfe8 \ud835\udfe9 \ud835\udfea \ud835\udfeb".split(" ");
    this.digitsSansSerifBold = "\ud835\udfec \ud835\udfed \ud835\udfee \ud835\udfef \ud835\udff0 \ud835\udff1 \ud835\udff2 \ud835\udff3 \ud835\udff4 \ud835\udff5".split(" ");
    this.digitsMonospace = "\ud835\udff6 \ud835\udff7 \ud835\udff8 \ud835\udff9 \ud835\udffa \ud835\udffb \ud835\udffc \ud835\udffd \ud835\udffe \ud835\udfff".split(" ");
    this.digitsSuperscript = "\u00b2\u00b3\u00b9\u2070\u2074\u2075\u2076\u2077\u2078\u2079".split("");
    this.digitsSubscript = "\u2080\u2081\u2082\u2083\u2084\u2085\u2086\u2087\u2088\u2089".split("");
    this.fractions = "\u00bc\u00bd\u00be\u2150\u2151\u2152\u2153\u2154\u2155\u2156\u2157\u2158\u2159\u215a\u215b\u215c\u215d\u215e\u215f\u2189".split("");
    this.enclosedNumbers = "\u2460\u2461\u2462\u2463\u2464\u2465\u2466\u2467\u2468\u2469\u246a\u246b\u246c\u246d\u246e\u246f\u2470\u2471\u2472\u2473\u24ea\u24eb\u24ec\u24ed\u24ee\u24ef\u24f0\u24f1\u24f2\u24f3\u24f4\u24f5\u24f6\u24f7\u24f8\u24f9\u24fa\u24fb\u24fc\u24fd\u24fe\u24ff\u2776\u2777\u2778\u2779\u277a\u277b\u277c\u277d\u277e\u277f\u2780\u2781\u2782\u2783\u2784\u2785\u2786\u2787\u2788\u2789\u278a\u278b\u278c\u278d\u278e\u278f\u2790\u2791\u2792\u2793\u3248\u3249\u324a\u324b\u324c\u324d\u324e\u324f\u3251\u3252\u3253\u3254\u3255\u3256\u3257\u3258\u3259\u325a\u325b\u325c\u325d\u325e\u325f\u32b1\u32b2\u32b3\u32b4\u32b5\u32b6\u32b7\u32b8\u32b9\u32ba\u32bb\u32bc\u32bd\u32be\u32bf".split("");
    this.fencedNumbers = "\u2474\u2475\u2476\u2477\u2478\u2479\u247a\u247b\u247c\u247d\u247e\u247f\u2480\u2481\u2482\u2483\u2484\u2485\u2486\u2487".split("");
    this.punctuatedNumbers = "\u2488 \u2489 \u248a \u248b \u248c \u248d \u248e \u248f \u2490 \u2491 \u2492 \u2493 \u2494 \u2495 \u2496 \u2497 \u2498 \u2499 \u249a \u249b \ud83c\udd00 \ud83c\udd01 \ud83c\udd02 \ud83c\udd03 \ud83c\udd04 \ud83c\udd05 \ud83c\udd06 \ud83c\udd07 \ud83c\udd08 \ud83c\udd09 \ud83c\udd0a".split(" ");
    this.digits = this.digitsNormal.concat(this.digitsFullWidth,
        this.digitsBold, this.digitsDoubleStruck, this.digitsSansSerif, this.digitsSansSerifBold, this.digitsMonospace);
    this.numbers = this.fractions.concat(this.digitsSuperscript, this.digitsSubscript, this.enclosedNumbers, this.fencedNumbers, this.punctuatedNumbers);
    this.allNumbers = this.digits.concat(this.numbers);
    this.trigonometricFunctions = "cos cot csc sec sin tan arccos arccot arccsc arcsec arcsin arctan".split(" ");
    this.hyperbolicFunctions = "cosh coth csch sech sinh tanh arcosh arcoth arcsch arsech arsinh artanh arccosh arccoth arccsch arcsech arcsinh arctanh".split(" ");
    this.algebraicFunctions = "deg det dim hom ker Tr tr".split(" ");
    this.elementaryFunctions = "log ln lg exp expt gcd gcd arg im re Pr".split(" ");
    this.prefixFunctions = this.trigonometricFunctions.concat(this.hyperbolicFunctions, this.algebraicFunctions, this.elementaryFunctions);
    this.limitFunctions = "inf lim liminf limsup max min sup injlim projlim".split(" ");
    this.infixFunctions = ["mod", "rem"];
    this.symbolSetToSemantic_ = [{
        set: this.generalPunctuations,
        type: sre.SemanticAttr.Type.PUNCTUATION,
        role: sre.SemanticAttr.Role.UNKNOWN
    },
        {
            set: this.commas,
            type: sre.SemanticAttr.Type.PUNCTUATION,
            role: sre.SemanticAttr.Role.COMMA
        }, {
            set: this.ellipses,
            type: sre.SemanticAttr.Type.PUNCTUATION,
            role: sre.SemanticAttr.Role.ELLIPSIS
        }, {
            set: this.fullStops,
            type: sre.SemanticAttr.Type.PUNCTUATION,
            role: sre.SemanticAttr.Role.FULLSTOP
        }, {
            set: this.dashes,
            type: sre.SemanticAttr.Type.PUNCTUATION,
            role: sre.SemanticAttr.Role.DASH
        }, {set: this.primes, type: sre.SemanticAttr.Type.PUNCTUATION, role: sre.SemanticAttr.Role.PRIME}, {
            set: this.leftFences, type: sre.SemanticAttr.Type.FENCE,
            role: sre.SemanticAttr.Role.OPEN
        }, {
            set: this.rightFences,
            type: sre.SemanticAttr.Type.FENCE,
            role: sre.SemanticAttr.Role.CLOSE
        }, {
            set: this.topFences,
            type: sre.SemanticAttr.Type.FENCE,
            role: sre.SemanticAttr.Role.TOP
        }, {
            set: this.bottomFences,
            type: sre.SemanticAttr.Type.FENCE,
            role: sre.SemanticAttr.Role.BOTTOM
        }, {
            set: this.neutralFences,
            type: sre.SemanticAttr.Type.FENCE,
            role: sre.SemanticAttr.Role.NEUTRAL
        }, {
            set: this.smallLatin,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        },
        {
            set: this.capitalLatin,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.smallLatinFullWidth,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.capitalLatinFullWidth,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.smallLatinBold, type: sre.SemanticAttr.Type.IDENTIFIER, role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.BOLD
        }, {
            set: this.capitalLatinBold,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.BOLD
        }, {
            set: this.smallLatinItalic,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.ITALIC
        }, {
            set: this.capitalLatinItalic,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.ITALIC
        }, {
            set: this.smallLatinScript, type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER, font: sre.SemanticAttr.Font.SCRIPT
        }, {
            set: this.capitalLatinScript,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.SCRIPT
        }, {
            set: this.smallLatinBoldScript,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.BOLDSCRIPT
        }, {
            set: this.capitalLatinBoldScript,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.BOLDSCRIPT
        },
        {
            set: this.smallLatinFraktur,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.FRAKTUR
        }, {
            set: this.capitalLatinFraktur,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.FRAKTUR
        }, {
            set: this.smallLatinDoubleStruck,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.DOUBLESTRUCK
        }, {
            set: this.capitalLatinDoubleStruck, type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER, font: sre.SemanticAttr.Font.DOUBLESTRUCK
        }, {
            set: this.smallLatinBoldFraktur,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.BOLDFRAKTUR
        }, {
            set: this.capitalLatinBoldFraktur,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.BOLDFRAKTUR
        }, {
            set: this.smallLatinSansSerif,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.SANSSERIF
        },
        {
            set: this.capitalLatinSansSerif,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.SANSSERIF
        }, {
            set: this.smallLatinSansSerifBold,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.SANSSERIFBOLD
        }, {
            set: this.capitalLatinSansSerifBold,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.SANSSERIFBOLD
        }, {
            set: this.smallLatinSansSerifItalic, type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER, font: sre.SemanticAttr.Font.SANSSERIFITALIC
        }, {
            set: this.capitalLatinSansSerifItalic,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.SANSSERIFITALIC
        }, {
            set: this.smallLatinMonospace,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.MONOSPACE
        }, {
            set: this.capitalLatinMonospace,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.MONOSPACE
        },
        {
            set: this.latinDoubleStruckItalic,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.LATINLETTER,
            font: sre.SemanticAttr.Font.DOUBLESTRUCKITALIC
        }, {
            set: this.smallGreek,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.capitalGreek,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.smallGreekBold, type: sre.SemanticAttr.Type.IDENTIFIER, role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.BOLD
        }, {
            set: this.capitalGreekBold,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.BOLD
        }, {
            set: this.smallGreekItalic,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.ITALIC
        }, {
            set: this.capitalGreekItalic,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.ITALIC
        }, {
            set: this.smallGreekSansSerifBold, type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER, font: sre.SemanticAttr.Font.SANSSERIFBOLD
        }, {
            set: this.capitalGreekSansSerifBold,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.SANSSERIFBOLD
        }, {
            set: this.greekDoubleStruck,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.GREEKLETTER,
            font: sre.SemanticAttr.Font.DOUBLESTRUCK
        }, {
            set: this.hebrewLetters,
            type: sre.SemanticAttr.Type.IDENTIFIER,
            role: sre.SemanticAttr.Role.OTHERLETTER,
            font: sre.SemanticAttr.Font.NORMAL
        },
        {
            set: this.digitsNormal,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.digitsFullWidth,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.NORMAL
        }, {
            set: this.digitsBold,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.BOLD
        }, {
            set: this.digitsDoubleStruck,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.DOUBLESTRUCK
        },
        {
            set: this.digitsSansSerif,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.SANSSERIF
        }, {
            set: this.digitsSansSerifBold,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.SANSSERIFBOLD
        }, {
            set: this.digitsMonospace,
            type: sre.SemanticAttr.Type.NUMBER,
            role: sre.SemanticAttr.Role.INTEGER,
            font: sre.SemanticAttr.Font.MONOSPACE
        }, {set: this.numbers, type: sre.SemanticAttr.Type.NUMBER, role: sre.SemanticAttr.Role.FLOAT}, {
            set: this.additions,
            type: sre.SemanticAttr.Type.OPERATOR, role: sre.SemanticAttr.Role.ADDITION
        }, {
            set: this.multiplications,
            type: sre.SemanticAttr.Type.OPERATOR,
            role: sre.SemanticAttr.Role.MULTIPLICATION
        }, {
            set: this.subtractions,
            type: sre.SemanticAttr.Type.OPERATOR,
            role: sre.SemanticAttr.Role.SUBTRACTION
        }, {
            set: this.divisions,
            type: sre.SemanticAttr.Type.OPERATOR,
            role: sre.SemanticAttr.Role.DIVISION
        }, {set: this.prefixOps, type: sre.SemanticAttr.Type.PREFIXOP, role: sre.SemanticAttr.Role.PREFIXFUNC}, {
            set: this.equalities, type: sre.SemanticAttr.Type.RELATION,
            role: sre.SemanticAttr.Role.EQUALITY
        }, {
            set: this.inequalities,
            type: sre.SemanticAttr.Type.RELATION,
            role: sre.SemanticAttr.Role.INEQUALITY
        }, {
            set: this.relations,
            type: sre.SemanticAttr.Type.RELATION,
            role: sre.SemanticAttr.Role.UNKNOWN
        }, {
            set: this.arrows,
            type: sre.SemanticAttr.Type.RELATION,
            role: sre.SemanticAttr.Role.ARROW
        }, {set: this.sumOps, type: sre.SemanticAttr.Type.LARGEOP, role: sre.SemanticAttr.Role.SUM}, {
            set: this.intOps,
            type: sre.SemanticAttr.Type.LARGEOP,
            role: sre.SemanticAttr.Role.INTEGRAL
        }, {
            set: this.limitFunctions,
            type: sre.SemanticAttr.Type.FUNCTION, role: sre.SemanticAttr.Role.LIMFUNC
        }, {
            set: this.prefixFunctions,
            type: sre.SemanticAttr.Type.FUNCTION,
            role: sre.SemanticAttr.Role.PREFIXFUNC
        }, {
            set: this.infixFunctions,
            type: sre.SemanticAttr.Type.OPERATOR,
            role: sre.SemanticAttr.Role.MULTIPLICATION
        }];
    this.meaning_ = this.initMeaning_()
};
goog.addSingletonGetter(sre.SemanticAttr);
sre.SemanticAttr.Type = {
    PUNCTUATION: "punctuation",
    FENCE: "fence",
    NUMBER: "number",
    IDENTIFIER: "identifier",
    TEXT: "text",
    OPERATOR: "operator",
    RELATION: "relation",
    LARGEOP: "largeop",
    FUNCTION: "function",
    ACCENT: "accent",
    FENCED: "fenced",
    FRACTION: "fraction",
    PUNCTUATED: "punctuated",
    RELSEQ: "relseq",
    MULTIREL: "multirel",
    INFIXOP: "infixop",
    PREFIXOP: "prefixop",
    POSTFIXOP: "postfixop",
    APPL: "appl",
    INTEGRAL: "integral",
    BIGOP: "bigop",
    SQRT: "sqrt",
    ROOT: "root",
    LIMUPPER: "limupper",
    LIMLOWER: "limlower",
    LIMBOTH: "limboth",
    SUBSCRIPT: "subscript",
    SUPERSCRIPT: "superscript",
    UNDERSCORE: "underscore",
    OVERSCORE: "overscore",
    TENSOR: "tensor",
    TABLE: "table",
    MULTILINE: "multiline",
    MATRIX: "matrix",
    VECTOR: "vector",
    CASES: "cases",
    ROW: "row",
    LINE: "line",
    CELL: "cell",
    ENCLOSE: "enclose",
    UNKNOWN: "unknown",
    EMPTY: "empty"
};
sre.SemanticAttr.Role = {
    COMMA: "comma",
    ELLIPSIS: "ellipsis",
    FULLSTOP: "fullstop",
    DASH: "dash",
    PRIME: "prime",
    VBAR: "vbar",
    OPENFENCE: "openfence",
    CLOSEFENCE: "closefence",
    APPLICATION: "application",
    DUMMY: "dummy",
    UNIT: "unit",
    OPEN: "open",
    CLOSE: "close",
    TOP: "top",
    BOTTOM: "bottom",
    NEUTRAL: "neutral",
    LATINLETTER: "latinletter",
    GREEKLETTER: "greekletter",
    OTHERLETTER: "otherletter",
    INTEGER: "integer",
    FLOAT: "float",
    OTHERNUMBER: "othernumber",
    MIXED: "mixed",
    MULTIACCENT: "multiaccent",
    OVERACCENT: "overaccent",
    UNDERACCENT: "underaccent",
    SUBSUP: "subsup",
    LEFTSUB: "leftsub",
    LEFTSUPER: "leftsuper",
    RIGHTSUB: "rightsub",
    RIGHTSUPER: "rightsuper",
    LEFTRIGHT: "leftright",
    ABOVEBELOW: "abovebelow",
    STRING: "string",
    SEQUENCE: "sequence",
    ENDPUNCT: "endpunct",
    STARTPUNCT: "startpunct",
    TEXT: "text",
    NEGATIVE: "negative",
    NEGATION: "negation",
    MULTIOP: "multiop",
    LIMFUNC: "limit function",
    INFIXFUNC: "infix function",
    PREFIXFUNC: "prefix function",
    POSTFIXFUNC: "postfix function",
    SIMPLEFUNC: "simple function",
    SUM: "sum",
    INTEGRAL: "integral",
    ADDITION: "addition",
    MULTIPLICATION: "multiplication",
    SUBTRACTION: "subtraction",
    IMPLICIT: "implicit",
    DIVISION: "division",
    VULGAR: "vulgar",
    EQUALITY: "equality",
    INEQUALITY: "inequality",
    ELEMENT: "element",
    BINREL: "binrel",
    ARROW: "arrow",
    DETERMINANT: "determinant",
    ROWVECTOR: "rowvector",
    BINOMIAL: "binomial",
    SQUAREMATRIX: "squarematrix",
    MULTILINE: "multiline",
    MATRIX: "matrix",
    VECTOR: "vector",
    CASES: "cases",
    TABLE: "table",
    UNKNOWN: "unknown",
    PROTECTED: "protected"
};
sre.SemanticAttr.Font = {
    BOLD: "bold",
    BOLDFRAKTUR: "bold-fraktur",
    BOLDITALIC: "bold-italic",
    BOLDSCRIPT: "bold-script",
    DOUBLESTRUCK: "double-struck",
    DOUBLESTRUCKITALIC: "double-struck-italic",
    FRAKTUR: "fraktur",
    ITALIC: "italic",
    MONOSPACE: "monospace",
    NORMAL: "normal",
    SCRIPT: "script",
    SANSSERIF: "sans-serif",
    SANSSERIFITALIC: "sans-serif-italic",
    SANSSERIFBOLD: "sans-serif-bold",
    SANSSERIFBOLDITALIC: "sans-serif-bold-italic",
    UNKNOWN: "unknown"
};
sre.SemanticAttr.prototype.lookupType = function (a) {
    return sre.SemanticAttr.Type.UNKNOWN
};
sre.SemanticAttr.prototype.lookupRole = function (a) {
    return sre.SemanticAttr.Role.UNKNOWN
};
sre.SemanticAttr.lookupMeaning = function (a) {
    return sre.SemanticAttr.getInstance().lookupMeaning_(a)
};
sre.SemanticAttr.invisibleTimes = function () {
    return sre.SemanticAttr.getInstance().invisibleTimes_
};
sre.SemanticAttr.invisibleComma = function () {
    return sre.SemanticAttr.getInstance().invisibleComma_
};
sre.SemanticAttr.functionApplication = function () {
    return sre.SemanticAttr.getInstance().functionApplication_
};
sre.SemanticAttr.isMatchingFenceRole = function (a, b) {
    return a == sre.SemanticAttr.Role.OPEN && b == sre.SemanticAttr.Role.CLOSE || a == sre.SemanticAttr.Role.NEUTRAL && b == sre.SemanticAttr.Role.NEUTRAL || a == sre.SemanticAttr.Role.TOP && b == sre.SemanticAttr.Role.BOTTOM
};
sre.SemanticAttr.isMatchingFence = function (a, b) {
    return sre.SemanticAttr.getInstance().isMatchingFence_(a, b)
};
sre.SemanticAttr.isOpeningFence = function (a) {
    return a == sre.SemanticAttr.Role.OPEN || a == sre.SemanticAttr.Role.NEUTRAL
};
sre.SemanticAttr.isClosingFence = function (a) {
    return a == sre.SemanticAttr.Role.CLOSE || a == sre.SemanticAttr.Role.NEUTRAL
};
sre.SemanticAttr.isEmbellishedType = function (a) {
    return a === sre.SemanticAttr.Type.OPERATOR || a === sre.SemanticAttr.Type.RELATION || a === sre.SemanticAttr.Type.FENCE || a === sre.SemanticAttr.Type.PUNCTUATION
};
sre.SemanticAttr.isCharacterD = function (a) {
    return -1 != "d \u2146 \uff44 \ud835\udc1d \ud835\udc51 \ud835\udcb9 \ud835\udced \ud835\udd21 \ud835\udd55 \ud835\udd89 \ud835\uddbd \ud835\uddf1 \ud835\ude25 \ud835\ude8d".split(" ").indexOf(a)
};
sre.SemanticAttr.prototype.isMatchingFence_ = function (a, b) {
    return -1 != this.neutralFences.indexOf(a) ? a == b : this.openClosePairs[a] == b || this.topBottomPairs[a] == b
};
sre.SemanticAttr.prototype.initMeaning_ = function () {
    for (var a = {}, b = 0, c; c = this.symbolSetToSemantic_[b]; b++)c.set.forEach(function (b) {
        a[b] = {
            role: c.role || sre.SemanticAttr.Role.UNKNOWN,
            type: c.type || sre.SemanticAttr.Type.UNKNOWN,
            font: c.font || sre.SemanticAttr.Font.UNKNOWN
        }
    });
    return a
};
sre.SemanticAttr.prototype.lookupMeaning_ = function (a) {
    return this.meaning_[a] || {
            role: sre.SemanticAttr.Role.UNKNOWN,
            type: sre.SemanticAttr.Type.UNKNOWN,
            font: sre.SemanticAttr.Font.UNKNOWN
        }
};
sre.MathUtil = {};
sre.MathUtil.isMathmlNodeOfClass_ = function (a, b) {
    return -1 != b.indexOf(a.tagName.toUpperCase())
};
sre.MathUtil.isMathjaxNodeOfClass_ = function (a, b) {
    return "SPAN" == a.tagName ? a.className.split(" ").some(function (a) {
        return -1 != b.indexOf(a.toUpperCase())
    }) : !1
};
sre.MathUtil.isMathNodeOfClass_ = function (a, b) {
    return a.nodeType == sre.DomUtil.NodeType.ELEMENT_NODE && (sre.MathUtil.isMathmlNodeOfClass_(a, b) || sre.MathUtil.isMathjaxNodeOfClass_(a, b))
};
sre.MathUtil.TOKEN_LIST = "MI MN MO MTEXT MSPACE MS".split(" ");
sre.MathUtil.isToken = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.TOKEN_LIST)
};
sre.MathUtil.LAYOUT_LIST = "MROW MFRAC MSQRT MROOT MSTYLE MERROR MPADDED MPHANTOM MFENCED MENCLOSE".split(" ");
sre.MathUtil.isLayout = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.LAYOUT_LIST)
};
sre.MathUtil.SCRIPT_LIST = "MSUB MSUP MSUBSUP MUNDER MOVER MUNDEROVER MMULTISCRIPTS MPRESCRIPTS".split(" ");
sre.MathUtil.isScript = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.SCRIPT_LIST)
};
sre.MathUtil.TABLES_LIST = "MTABLE MLABELEDTR MTR MTD MALIGNGROUP MALIGNMARK".split(" ");
sre.MathUtil.isTables = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.TABLES_LIST)
};
sre.MathUtil.ELEMENTARY_LIST = "MSTACK MLONGDIV MSGROUP MSROW MSCARRIES MSCARRY MSLINE".split(" ");
sre.MathUtil.isElementary = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.ELEMENTARY_LIST)
};
sre.MathUtil.MATHML_TAG_LIST = [sre.MathUtil.TOKEN_LIST, sre.MathUtil.LAYOUT_LIST, sre.MathUtil.SCRIPT_LIST, sre.MathUtil.TABLES_LIST, sre.MathUtil.ELEMENTARY_LIST].reduce(function (a, b) {
    return a.concat(b)
});
sre.MathUtil.isMathmlTag = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.MATHML_TAG_LIST)
};
sre.MathUtil.WHITESPACE_LIST = ["MSROW", "MROW", "MSPACE", "MPHANTOM", "MPADDED"];
sre.MathUtil.isWhitespace = function (a) {
    return sre.MathUtil.isMathNodeOfClass_(a, sre.MathUtil.WHITESPACE_LIST)
};
sre.MathUtil.isNotWhitespace = function (a) {
    return sre.MathUtil.isMathmlTag(a) && !sre.MathUtil.isWhitespace(a)
};
sre.MathUtil.setdifference = function (a, b) {
    return a.filter(function (a) {
        return 0 > b.indexOf(a)
    })
};
sre.MathUtil.union = function (a, b) {
    return a.concat(sre.MathUtil.setdifference(b, a))
};
sre.MathUtil.nextSeparatorFunction = function (a) {
    if (a) {
        if (a.match(/^\s+$/))return null;
        var b = a.replace(/\s/g, "").split("").filter(function (a) {
            return a
        })
    } else b = [","];
    return function () {
        return 1 < b.length ? b.shift() : b[0]
    }
};
goog.debug = {};
goog.debug.Error = function (a) {
    Error.captureStackTrace ? Error.captureStackTrace(this, goog.debug.Error) : this.stack = Error().stack || "";
    a && (this.message = String(a))
};
goog.inherits(goog.debug.Error, Error);
goog.debug.Error.prototype.name = "CustomError";
goog.string = {};
goog.string.Unicode = {NBSP: "\u00a0"};
goog.string.startsWith = function (a, b) {
    return 0 == a.lastIndexOf(b, 0)
};
goog.string.endsWith = function (a, b) {
    var c = a.length - b.length;
    return 0 <= c && a.indexOf(b, c) == c
};
goog.string.caseInsensitiveStartsWith = function (a, b) {
    return 0 == goog.string.caseInsensitiveCompare(b, a.substr(0, b.length))
};
goog.string.caseInsensitiveEndsWith = function (a, b) {
    return 0 == goog.string.caseInsensitiveCompare(b, a.substr(a.length - b.length, b.length))
};
goog.string.caseInsensitiveEquals = function (a, b) {
    return a.toLowerCase() == b.toLowerCase()
};
goog.string.subs = function (a, b) {
    for (var c = 1; c < arguments.length; c++) {
        var d = String(arguments[c]).replace(/\$/g, "$$$$");
        a = a.replace(/\%s/, d)
    }
    return a
};
goog.string.collapseWhitespace = function (a) {
    return a.replace(/[\s\xa0]+/g, " ").replace(/^\s+|\s+$/g, "")
};
goog.string.isEmpty = function (a) {
    return /^[\s\xa0]*$/.test(a)
};
goog.string.isEmptySafe = function (a) {
    return goog.string.isEmpty(goog.string.makeSafe(a))
};
goog.string.isBreakingWhitespace = function (a) {
    return !/[^\t\n\r ]/.test(a)
};
goog.string.isAlpha = function (a) {
    return !/[^a-zA-Z]/.test(a)
};
goog.string.isNumeric = function (a) {
    return !/[^0-9]/.test(a)
};
goog.string.isAlphaNumeric = function (a) {
    return !/[^a-zA-Z0-9]/.test(a)
};
goog.string.isSpace = function (a) {
    return " " == a
};
goog.string.isUnicodeChar = function (a) {
    return 1 == a.length && " " <= a && "~" >= a || "\u0080" <= a && "\ufffd" >= a
};
goog.string.stripNewlines = function (a) {
    return a.replace(/(\r\n|\r|\n)+/g, " ")
};
goog.string.canonicalizeNewlines = function (a) {
    return a.replace(/(\r\n|\r|\n)/g, "\n")
};
goog.string.normalizeWhitespace = function (a) {
    return a.replace(/\xa0|\s/g, " ")
};
goog.string.normalizeSpaces = function (a) {
    return a.replace(/\xa0|[ \t]+/g, " ")
};
goog.string.collapseBreakingSpaces = function (a) {
    return a.replace(/[\t\r\n ]+/g, " ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g, "")
};
goog.string.trim = function (a) {
    return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
};
goog.string.trimLeft = function (a) {
    return a.replace(/^[\s\xa0]+/, "")
};
goog.string.trimRight = function (a) {
    return a.replace(/[\s\xa0]+$/, "")
};
goog.string.caseInsensitiveCompare = function (a, b) {
    var c = String(a).toLowerCase(), d = String(b).toLowerCase();
    return c < d ? -1 : c == d ? 0 : 1
};
goog.string.numerateCompareRegExp_ = /(\.\d+)|(\d+)|(\D+)/g;
goog.string.numerateCompare = function (a, b) {
    if (a == b)return 0;
    if (!a)return -1;
    if (!b)return 1;
    for (var c = a.toLowerCase().match(goog.string.numerateCompareRegExp_), d = b.toLowerCase().match(goog.string.numerateCompareRegExp_), e = Math.min(c.length, d.length), f = 0; f < e; f++) {
        var g = c[f], h = d[f];
        if (g != h)return c = parseInt(g, 10), !isNaN(c) && (d = parseInt(h, 10), !isNaN(d) && c - d) ? c - d : g < h ? -1 : 1
    }
    return c.length != d.length ? c.length - d.length : a < b ? -1 : 1
};
goog.string.urlEncode = function (a) {
    return encodeURIComponent(String(a))
};
goog.string.urlDecode = function (a) {
    return decodeURIComponent(a.replace(/\+/g, " "))
};
goog.string.newLineToBr = function (a, b) {
    return a.replace(/(\r\n|\r|\n)/g, b ? "<br />" : "<br>")
};
goog.string.htmlEscape = function (a, b) {
    if (b)return a.replace(goog.string.amperRe_, "&amp;").replace(goog.string.ltRe_, "&lt;").replace(goog.string.gtRe_, "&gt;").replace(goog.string.quotRe_, "&quot;");
    if (!goog.string.allRe_.test(a))return a;
    -1 != a.indexOf("&") && (a = a.replace(goog.string.amperRe_, "&amp;"));
    -1 != a.indexOf("<") && (a = a.replace(goog.string.ltRe_, "&lt;"));
    -1 != a.indexOf(">") && (a = a.replace(goog.string.gtRe_, "&gt;"));
    -1 != a.indexOf('"') && (a = a.replace(goog.string.quotRe_, "&quot;"));
    return a
};
goog.string.amperRe_ = /&/g;
goog.string.ltRe_ = /</g;
goog.string.gtRe_ = />/g;
goog.string.quotRe_ = /\"/g;
goog.string.allRe_ = /[&<>\"]/;
goog.string.unescapeEntities = function (a) {
    return goog.string.contains(a, "&") ? "document" in goog.global ? goog.string.unescapeEntitiesUsingDom_(a) : goog.string.unescapePureXmlEntities_(a) : a
};
goog.string.unescapeEntitiesUsingDom_ = function (a) {
    var b = {"&amp;": "&", "&lt;": "<", "&gt;": ">", "&quot;": '"'}, c = document.createElement("div");
    return a.replace(goog.string.HTML_ENTITY_PATTERN_, function (a, e) {
        var f = b[a];
        if (f)return f;
        if ("#" == e.charAt(0)) {
            var g = Number("0" + e.substr(1));
            isNaN(g) || (f = String.fromCharCode(g))
        }
        f || (c.innerHTML = a + " ", f = c.firstChild.nodeValue.slice(0, -1));
        return b[a] = f
    })
};
goog.string.unescapePureXmlEntities_ = function (a) {
    return a.replace(/&([^;]+);/g, function (a, c) {
        switch (c) {
            case "amp":
                return "&";
            case "lt":
                return "<";
            case "gt":
                return ">";
            case "quot":
                return '"';
            default:
                if ("#" == c.charAt(0)) {
                    var d = Number("0" + c.substr(1));
                    if (!isNaN(d))return String.fromCharCode(d)
                }
                return a
        }
    })
};
goog.string.HTML_ENTITY_PATTERN_ = /&([^;\s<&]+);?/g;
goog.string.whitespaceEscape = function (a, b) {
    return goog.string.newLineToBr(a.replace(/  /g, " &#160;"), b)
};
goog.string.stripQuotes = function (a, b) {
    for (var c = b.length, d = 0; d < c; d++) {
        var e = 1 == c ? b : b.charAt(d);
        if (a.charAt(0) == e && a.charAt(a.length - 1) == e)return a.substring(1, a.length - 1)
    }
    return a
};
goog.string.truncate = function (a, b, c) {
    c && (a = goog.string.unescapeEntities(a));
    a.length > b && (a = a.substring(0, b - 3) + "...");
    c && (a = goog.string.htmlEscape(a));
    return a
};
goog.string.truncateMiddle = function (a, b, c, d) {
    c && (a = goog.string.unescapeEntities(a));
    if (d && a.length > b) {
        d > b && (d = b);
        var e = a.length - d;
        a = a.substring(0, b - d) + "..." + a.substring(e)
    } else a.length > b && (d = Math.floor(b / 2), e = a.length - d, a = a.substring(0, d + b % 2) + "..." + a.substring(e));
    c && (a = goog.string.htmlEscape(a));
    return a
};
goog.string.specialEscapeChars_ = {
    "\x00": "\\0",
    "\b": "\\b",
    "\f": "\\f",
    "\n": "\\n",
    "\r": "\\r",
    "\t": "\\t",
    "\x0B": "\\x0B",
    '"': '\\"',
    "\\": "\\\\"
};
goog.string.jsEscapeCache_ = {"'": "\\'"};
goog.string.quote = function (a) {
    a = String(a);
    if (a.quote)return a.quote();
    for (var b = ['"'], c = 0; c < a.length; c++) {
        var d = a.charAt(c), e = d.charCodeAt(0);
        b[c + 1] = goog.string.specialEscapeChars_[d] || (31 < e && 127 > e ? d : goog.string.escapeChar(d))
    }
    b.push('"');
    return b.join("")
};
goog.string.escapeString = function (a) {
    for (var b = [], c = 0; c < a.length; c++)b[c] = goog.string.escapeChar(a.charAt(c));
    return b.join("")
};
goog.string.escapeChar = function (a) {
    if (a in goog.string.jsEscapeCache_)return goog.string.jsEscapeCache_[a];
    if (a in goog.string.specialEscapeChars_)return goog.string.jsEscapeCache_[a] = goog.string.specialEscapeChars_[a];
    var b = a, c = a.charCodeAt(0);
    if (31 < c && 127 > c)b = a; else {
        if (256 > c) {
            if (b = "\\x", 16 > c || 256 < c)b += "0"
        } else b = "\\u", 4096 > c && (b += "0");
        b += c.toString(16).toUpperCase()
    }
    return goog.string.jsEscapeCache_[a] = b
};
goog.string.toMap = function (a) {
    for (var b = {}, c = 0; c < a.length; c++)b[a.charAt(c)] = !0;
    return b
};
goog.string.contains = function (a, b) {
    return -1 != a.indexOf(b)
};
goog.string.countOf = function (a, b) {
    return a && b ? a.split(b).length - 1 : 0
};
goog.string.removeAt = function (a, b, c) {
    var d = a;
    0 <= b && b < a.length && 0 < c && (d = a.substr(0, b) + a.substr(b + c, a.length - b - c));
    return d
};
goog.string.remove = function (a, b) {
    var c = new RegExp(goog.string.regExpEscape(b), "");
    return a.replace(c, "")
};
goog.string.removeAll = function (a, b) {
    var c = new RegExp(goog.string.regExpEscape(b), "g");
    return a.replace(c, "")
};
goog.string.regExpEscape = function (a) {
    return String(a).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08")
};
goog.string.repeat = function (a, b) {
    return Array(b + 1).join(a)
};
goog.string.padNumber = function (a, b, c) {
    a = goog.isDef(c) ? a.toFixed(c) : String(a);
    c = a.indexOf(".");
    -1 == c && (c = a.length);
    return goog.string.repeat("0", Math.max(0, b - c)) + a
};
goog.string.makeSafe = function (a) {
    return null == a ? "" : String(a)
};
goog.string.buildString = function (a) {
    return Array.prototype.join.call(arguments, "")
};
goog.string.getRandomString = function () {
    return Math.floor(2147483648 * Math.random()).toString(36) + Math.abs(Math.floor(2147483648 * Math.random()) ^ goog.now()).toString(36)
};
goog.string.compareVersions = function (a, b) {
    for (var c = 0, d = goog.string.trim(String(a)).split("."), e = goog.string.trim(String(b)).split("."), f = Math.max(d.length, e.length), g = 0; 0 == c && g < f; g++) {
        var h = d[g] || "", k = e[g] || "", l = RegExp("(\\d*)(\\D*)", "g"), p = RegExp("(\\d*)(\\D*)", "g");
        do {
            var m = l.exec(h) || ["", "", ""], n = p.exec(k) || ["", "", ""];
            if (0 == m[0].length && 0 == n[0].length)break;
            var c = 0 == m[1].length ? 0 : parseInt(m[1], 10), q = 0 == n[1].length ? 0 : parseInt(n[1], 10), c = goog.string.compareElements_(c, q) || goog.string.compareElements_(0 ==
                    m[2].length, 0 == n[2].length) || goog.string.compareElements_(m[2], n[2])
        } while (0 == c)
    }
    return c
};
goog.string.compareElements_ = function (a, b) {
    return a < b ? -1 : a > b ? 1 : 0
};
goog.string.HASHCODE_MAX_ = 4294967296;
goog.string.hashCode = function (a) {
    for (var b = 0, c = 0; c < a.length; ++c)b = 31 * b + a.charCodeAt(c), b %= goog.string.HASHCODE_MAX_;
    return b
};
goog.string.uniqueStringCounter_ = 2147483648 * Math.random() | 0;
goog.string.createUniqueString = function () {
    return "goog_" + goog.string.uniqueStringCounter_++
};
goog.string.toNumber = function (a) {
    var b = Number(a);
    return 0 == b && goog.string.isEmpty(a) ? NaN : b
};
goog.string.toCamelCase = function (a) {
    return String(a).replace(/\-([a-z])/g, function (a, c) {
        return c.toUpperCase()
    })
};
goog.string.toSelectorCase = function (a) {
    return String(a).replace(/([A-Z])/g, "-$1").toLowerCase()
};
goog.string.toTitleCase = function (a, b) {
    var c = goog.isString(b) ? goog.string.regExpEscape(b) : "\\s";
    return a.replace(new RegExp("(^" + (c ? "|[" + c + "]+" : "") + ")([a-z])", "g"), function (a, b, c) {
        return b + c.toUpperCase()
    })
};
goog.string.parseInt = function (a) {
    isFinite(a) && (a = String(a));
    return goog.isString(a) ? /^\s*-?0x/i.test(a) ? parseInt(a, 16) : parseInt(a, 10) : NaN
};
goog.asserts = {};
goog.asserts.ENABLE_ASSERTS = goog.DEBUG;
goog.asserts.AssertionError = function (a, b) {
    b.unshift(a);
    goog.debug.Error.call(this, goog.string.subs.apply(null, b));
    b.shift();
    this.messagePattern = a
};
goog.inherits(goog.asserts.AssertionError, goog.debug.Error);
goog.asserts.AssertionError.prototype.name = "AssertionError";
goog.asserts.doAssertFailure_ = function (a, b, c, d) {
    var e = "Assertion failed";
    if (c)var e = e + (": " + c), f = d; else a && (e += ": " + a, f = b);
    throw new goog.asserts.AssertionError("" + e, f || []);
};
goog.asserts.assert = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !a && goog.asserts.doAssertFailure_("", null, b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.fail = function (a, b) {
    if (goog.asserts.ENABLE_ASSERTS)throw new goog.asserts.AssertionError("Failure" + (a ? ": " + a : ""), Array.prototype.slice.call(arguments, 1));
};
goog.asserts.assertNumber = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !goog.isNumber(a) && goog.asserts.doAssertFailure_("Expected number but got %s: %s.", [goog.typeOf(a), a], b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.assertString = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !goog.isString(a) && goog.asserts.doAssertFailure_("Expected string but got %s: %s.", [goog.typeOf(a), a], b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.assertFunction = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !goog.isFunction(a) && goog.asserts.doAssertFailure_("Expected function but got %s: %s.", [goog.typeOf(a), a], b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.assertObject = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !goog.isObject(a) && goog.asserts.doAssertFailure_("Expected object but got %s: %s.", [goog.typeOf(a), a], b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.assertArray = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !goog.isArray(a) && goog.asserts.doAssertFailure_("Expected array but got %s: %s.", [goog.typeOf(a), a], b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.assertBoolean = function (a, b, c) {
    goog.asserts.ENABLE_ASSERTS && !goog.isBoolean(a) && goog.asserts.doAssertFailure_("Expected boolean but got %s: %s.", [goog.typeOf(a), a], b, Array.prototype.slice.call(arguments, 2));
    return a
};
goog.asserts.assertInstanceof = function (a, b, c, d) {
    !goog.asserts.ENABLE_ASSERTS || a instanceof b || goog.asserts.doAssertFailure_("instanceof check failed.", null, c, Array.prototype.slice.call(arguments, 3));
    return a
};
goog.array = {};
goog.NATIVE_ARRAY_PROTOTYPES = goog.TRUSTED_SITE;
goog.array.peek = function (a) {
    return a[a.length - 1]
};
goog.array.ARRAY_PROTOTYPE_ = Array.prototype;
goog.array.indexOf = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.indexOf ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.indexOf.call(a, b, c)
} : function (a, b, c) {
    c = null == c ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
    if (goog.isString(a))return goog.isString(b) && 1 == b.length ? a.indexOf(b, c) : -1;
    for (; c < a.length; c++)if (c in a && a[c] === b)return c;
    return -1
};
goog.array.lastIndexOf = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.lastIndexOf ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.lastIndexOf.call(a, b, null == c ? a.length - 1 : c)
} : function (a, b, c) {
    c = null == c ? a.length - 1 : c;
    0 > c && (c = Math.max(0, a.length + c));
    if (goog.isString(a))return goog.isString(b) && 1 == b.length ? a.lastIndexOf(b, c) : -1;
    for (; 0 <= c; c--)if (c in a && a[c] === b)return c;
    return -1
};
goog.array.forEach = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.forEach ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    goog.array.ARRAY_PROTOTYPE_.forEach.call(a, b, c)
} : function (a, b, c) {
    for (var d = a.length, e = goog.isString(a) ? a.split("") : a, f = 0; f < d; f++)f in e && b.call(c, e[f], f, a)
};
goog.array.forEachRight = function (a, b, c) {
    for (var d = a.length, e = goog.isString(a) ? a.split("") : a, d = d - 1; 0 <= d; --d)d in e && b.call(c, e[d], d, a)
};
goog.array.filter = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.filter ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.filter.call(a, b, c)
} : function (a, b, c) {
    for (var d = a.length, e = [], f = 0, g = goog.isString(a) ? a.split("") : a, h = 0; h < d; h++)if (h in g) {
        var k = g[h];
        b.call(c, k, h, a) && (e[f++] = k)
    }
    return e
};
goog.array.map = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.map ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.map.call(a, b, c)
} : function (a, b, c) {
    for (var d = a.length, e = Array(d), f = goog.isString(a) ? a.split("") : a, g = 0; g < d; g++)g in f && (e[g] = b.call(c, f[g], g, a));
    return e
};
goog.array.reduce = function (a, b, c, d) {
    if (a.reduce)return d ? a.reduce(goog.bind(b, d), c) : a.reduce(b, c);
    var e = c;
    goog.array.forEach(a, function (c, g) {
        e = b.call(d, e, c, g, a)
    });
    return e
};
goog.array.reduceRight = function (a, b, c, d) {
    if (a.reduceRight)return d ? a.reduceRight(goog.bind(b, d), c) : a.reduceRight(b, c);
    var e = c;
    goog.array.forEachRight(a, function (c, g) {
        e = b.call(d, e, c, g, a)
    });
    return e
};
goog.array.some = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.some ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.some.call(a, b, c)
} : function (a, b, c) {
    for (var d = a.length, e = goog.isString(a) ? a.split("") : a, f = 0; f < d; f++)if (f in e && b.call(c, e[f], f, a))return !0;
    return !1
};
goog.array.every = goog.NATIVE_ARRAY_PROTOTYPES && goog.array.ARRAY_PROTOTYPE_.every ? function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.every.call(a, b, c)
} : function (a, b, c) {
    for (var d = a.length, e = goog.isString(a) ? a.split("") : a, f = 0; f < d; f++)if (f in e && !b.call(c, e[f], f, a))return !1;
    return !0
};
goog.array.count = function (a, b, c) {
    var d = 0;
    goog.array.forEach(a, function (a, f, g) {
        b.call(c, a, f, g) && ++d
    }, c);
    return d
};
goog.array.find = function (a, b, c) {
    b = goog.array.findIndex(a, b, c);
    return 0 > b ? null : goog.isString(a) ? a.charAt(b) : a[b]
};
goog.array.findIndex = function (a, b, c) {
    for (var d = a.length, e = goog.isString(a) ? a.split("") : a, f = 0; f < d; f++)if (f in e && b.call(c, e[f], f, a))return f;
    return -1
};
goog.array.findRight = function (a, b, c) {
    b = goog.array.findIndexRight(a, b, c);
    return 0 > b ? null : goog.isString(a) ? a.charAt(b) : a[b]
};
goog.array.findIndexRight = function (a, b, c) {
    for (var d = a.length, e = goog.isString(a) ? a.split("") : a, d = d - 1; 0 <= d; d--)if (d in e && b.call(c, e[d], d, a))return d;
    return -1
};
goog.array.contains = function (a, b) {
    return 0 <= goog.array.indexOf(a, b)
};
goog.array.isEmpty = function (a) {
    return 0 == a.length
};
goog.array.clear = function (a) {
    if (!goog.isArray(a))for (var b = a.length - 1; 0 <= b; b--)delete a[b];
    a.length = 0
};
goog.array.insert = function (a, b) {
    goog.array.contains(a, b) || a.push(b)
};
goog.array.insertAt = function (a, b, c) {
    goog.array.splice(a, c, 0, b)
};
goog.array.insertArrayAt = function (a, b, c) {
    goog.partial(goog.array.splice, a, c, 0).apply(null, b)
};
goog.array.insertBefore = function (a, b, c) {
    var d;
    2 == arguments.length || 0 > (d = goog.array.indexOf(a, c)) ? a.push(b) : goog.array.insertAt(a, b, d)
};
goog.array.remove = function (a, b) {
    var c = goog.array.indexOf(a, b), d;
    (d = 0 <= c) && goog.array.removeAt(a, c);
    return d
};
goog.array.removeAt = function (a, b) {
    goog.asserts.assert(null != a.length);
    return 1 == goog.array.ARRAY_PROTOTYPE_.splice.call(a, b, 1).length
};
goog.array.removeIf = function (a, b, c) {
    b = goog.array.findIndex(a, b, c);
    return 0 <= b ? (goog.array.removeAt(a, b), !0) : !1
};
goog.array.concat = function (a) {
    return goog.array.ARRAY_PROTOTYPE_.concat.apply(goog.array.ARRAY_PROTOTYPE_, arguments)
};
goog.array.toArray = function (a) {
    var b = a.length;
    if (0 < b) {
        for (var c = Array(b), d = 0; d < b; d++)c[d] = a[d];
        return c
    }
    return []
};
goog.array.clone = goog.array.toArray;
goog.array.extend = function (a, b) {
    for (var c = 1; c < arguments.length; c++) {
        var d = arguments[c], e;
        if (goog.isArray(d) || (e = goog.isArrayLike(d)) && Object.prototype.hasOwnProperty.call(d, "callee"))a.push.apply(a, d); else if (e)for (var f = a.length, g = d.length, h = 0; h < g; h++)a[f + h] = d[h]; else a.push(d)
    }
};
goog.array.splice = function (a, b, c, d) {
    goog.asserts.assert(null != a.length);
    return goog.array.ARRAY_PROTOTYPE_.splice.apply(a, goog.array.slice(arguments, 1))
};
goog.array.slice = function (a, b, c) {
    goog.asserts.assert(null != a.length);
    return 2 >= arguments.length ? goog.array.ARRAY_PROTOTYPE_.slice.call(a, b) : goog.array.ARRAY_PROTOTYPE_.slice.call(a, b, c)
};
goog.array.removeDuplicates = function (a, b) {
    for (var c = b || a, d = {}, e = 0, f = 0; f < a.length;) {
        var g = a[f++], h = goog.isObject(g) ? "o" + goog.getUid(g) : (typeof g).charAt(0) + g;
        Object.prototype.hasOwnProperty.call(d, h) || (d[h] = !0, c[e++] = g)
    }
    c.length = e
};
goog.array.binarySearch = function (a, b, c) {
    return goog.array.binarySearch_(a, c || goog.array.defaultCompare, !1, b)
};
goog.array.binarySelect = function (a, b, c) {
    return goog.array.binarySearch_(a, b, !0, void 0, c)
};
goog.array.binarySearch_ = function (a, b, c, d, e) {
    for (var f = 0, g = a.length, h; f < g;) {
        var k = f + g >> 1, l;
        l = c ? b.call(e, a[k], k, a) : b(d, a[k]);
        0 < l ? f = k + 1 : (g = k, h = !l)
    }
    return h ? f : ~f
};
goog.array.sort = function (a, b) {
    goog.asserts.assert(null != a.length);
    goog.array.ARRAY_PROTOTYPE_.sort.call(a, b || goog.array.defaultCompare)
};
goog.array.stableSort = function (a, b) {
    for (var c = 0; c < a.length; c++)a[c] = {index: c, value: a[c]};
    var d = b || goog.array.defaultCompare;
    goog.array.sort(a, function (a, b) {
        return d(a.value, b.value) || a.index - b.index
    });
    for (c = 0; c < a.length; c++)a[c] = a[c].value
};
goog.array.sortObjectsByKey = function (a, b, c) {
    var d = c || goog.array.defaultCompare;
    goog.array.sort(a, function (a, c) {
        return d(a[b], c[b])
    })
};
goog.array.isSorted = function (a, b, c) {
    b = b || goog.array.defaultCompare;
    for (var d = 1; d < a.length; d++) {
        var e = b(a[d - 1], a[d]);
        if (0 < e || 0 == e && c)return !1
    }
    return !0
};
goog.array.equals = function (a, b, c) {
    if (!goog.isArrayLike(a) || !goog.isArrayLike(b) || a.length != b.length)return !1;
    var d = a.length;
    c = c || goog.array.defaultCompareEquality;
    for (var e = 0; e < d; e++)if (!c(a[e], b[e]))return !1;
    return !0
};
goog.array.compare = function (a, b, c) {
    return goog.array.equals(a, b, c)
};
goog.array.compare3 = function (a, b, c) {
    c = c || goog.array.defaultCompare;
    for (var d = Math.min(a.length, b.length), e = 0; e < d; e++) {
        var f = c(a[e], b[e]);
        if (0 != f)return f
    }
    return goog.array.defaultCompare(a.length, b.length)
};
goog.array.defaultCompare = function (a, b) {
    return a > b ? 1 : a < b ? -1 : 0
};
goog.array.defaultCompareEquality = function (a, b) {
    return a === b
};
goog.array.binaryInsert = function (a, b, c) {
    c = goog.array.binarySearch(a, b, c);
    return 0 > c ? (goog.array.insertAt(a, b, -(c + 1)), !0) : !1
};
goog.array.binaryRemove = function (a, b, c) {
    b = goog.array.binarySearch(a, b, c);
    return 0 <= b ? goog.array.removeAt(a, b) : !1
};
goog.array.bucket = function (a, b) {
    for (var c = {}, d = 0; d < a.length; d++) {
        var e = a[d], f = b(e, d, a);
        goog.isDef(f) && (c[f] || (c[f] = [])).push(e)
    }
    return c
};
goog.array.toObject = function (a, b, c) {
    var d = {};
    goog.array.forEach(a, function (e, f) {
        d[b.call(c, e, f, a)] = e
    });
    return d
};
goog.array.range = function (a, b, c) {
    var d = [], e = 0, f = a;
    c = c || 1;
    void 0 !== b && (e = a, f = b);
    if (0 > c * (f - e))return [];
    if (0 < c)for (a = e; a < f; a += c)d.push(a); else for (a = e; a > f; a += c)d.push(a);
    return d
};
goog.array.repeat = function (a, b) {
    for (var c = [], d = 0; d < b; d++)c[d] = a;
    return c
};
goog.array.flatten = function (a) {
    for (var b = [], c = 0; c < arguments.length; c++) {
        var d = arguments[c];
        goog.isArray(d) ? b.push.apply(b, goog.array.flatten.apply(null, d)) : b.push(d)
    }
    return b
};
goog.array.rotate = function (a, b) {
    goog.asserts.assert(null != a.length);
    a.length && (b %= a.length, 0 < b ? goog.array.ARRAY_PROTOTYPE_.unshift.apply(a, a.splice(-b, b)) : 0 > b && goog.array.ARRAY_PROTOTYPE_.push.apply(a, a.splice(0, -b)));
    return a
};
goog.array.zip = function (a) {
    if (!arguments.length)return [];
    for (var b = [], c = 0; ; c++) {
        for (var d = [], e = 0; e < arguments.length; e++) {
            var f = arguments[e];
            if (c >= f.length)return b;
            d.push(f[c])
        }
        b.push(d)
    }
};
goog.array.shuffle = function (a, b) {
    for (var c = b || Math.random, d = a.length - 1; 0 < d; d--) {
        var e = Math.floor(c() * (d + 1)), f = a[d];
        a[d] = a[e];
        a[e] = f
    }
};
sre.SemanticTree = function (a) {
    this.idCounter_ = 0;
    this.mathml = a;
    this.root = this.parseMathml_(a)
};
sre.SemanticTree.Node = function (a) {
    this.id = a;
    this.mathml = [];
    this.parent = null;
    this.type = sre.SemanticAttr.Type.UNKNOWN;
    this.role = sre.SemanticAttr.Role.UNKNOWN;
    this.font = sre.SemanticAttr.Font.UNKNOWN;
    this.fencePointer = this.embellished = null;
    this.childNodes = [];
    this.textContent = "";
    this.mathmlTree = null;
    this.contentNodes = []
};
sre.SemanticTree.Node.prototype.querySelectorAll = function (a) {
    for (var b = [], c = 0, d; d = this.childNodes[c]; c++)b = b.concat(d.querySelectorAll(a));
    a(this) && b.unshift(this);
    return b
};
sre.SemanticTree.prototype.xml = function (a) {
    var b = (new sre.SystemExternal.xmldom.DOMParser).parseFromString("<stree></stree>", "text/xml");
    a = this.root.xml(b, a);
    b.childNodes[0].appendChild(a);
    return b
};
sre.SemanticTree.Node.prototype.xml = function (a, b) {
    var c = function (c, d) {
        for (var g = d.map(function (c) {
            return c.xml(a, b)
        }), h = a.createElement(c), k = 0, l; l = g[k]; k++)h.appendChild(l);
        return h
    }, d = a.createElement(this.type);
    b || this.xmlAttributes_(d);
    d.textContent = this.textContent;
    0 < this.contentNodes.length && d.appendChild(c("content", this.contentNodes));
    0 < this.childNodes.length && d.appendChild(c("children", this.childNodes));
    return d
};
sre.SemanticTree.prototype.toString = function (a) {
    return (new sre.SystemExternal.xmldom.XMLSerializer).serializeToString(this.xml(a))
};
sre.SemanticTree.prototype.formatXml = function (a) {
    a = this.toString(a);
    return sre.SemanticTree.formatXml(a)
};
sre.SemanticTree.formatXml = function (a) {
    var b = /(>)(<)(\/*)/g;
    a = a.replace(b, "$1\r\n$2$3");
    b = /(>)(.+)(<c)/g;
    a = a.replace(b, "$1\r\n$2\r\n$3");
    var c = "", d = "";
    a.split("\r\n").forEach(function (a) {
        a.match(/.+<\/\w[^>]*>$/) ? c += d + a + "\r\n" : a.match(/^<\/\w/) ? d && (d = d.slice(2), c += d + a + "\r\n") : a.match(/^<\w[^>]*[^\/]>.*$/) ? (c += d + a + "\r\n", d += "  ") : c += d + a + "\r\n"
    });
    return c
};
sre.SemanticTree.Node.prototype.toString = function (a) {
    var b = new sre.SystemExternal.xmldom.XMLSerializer, c = (new sre.SystemExternal.xmldom.DOMParser).parseFromString("//", "text/xml");
    return b.serializeToString(this.xml(c, a))
};
sre.SemanticTree.Node.prototype.xmlAttributes_ = function (a) {
    a.setAttribute("role", this.role);
    this.font != sre.SemanticAttr.Font.UNKNOWN && a.setAttribute("font", this.font);
    this.embellished && a.setAttribute("embellished", this.embellished);
    null !== this.fencePointer && a.setAttribute("fencepointer", this.fencePointer.toString());
    a.setAttribute("id", this.id)
};
sre.SemanticTree.prototype.createNode_ = function () {
    return new sre.SemanticTree.Node(this.idCounter_++)
};
sre.SemanticTree.prototype.replaceNode_ = function (a, b) {
    var c = a.parent;
    c ? c.replaceChild_(a, b) : this.root = b
};
sre.SemanticTree.Node.prototype.updateContent_ = function (a) {
    a = a.trim();
    if (this.textContent != a) {
        var b = sre.SemanticAttr.lookupMeaning(a);
        this.textContent = a;
        this.role = b.role;
        this.type = b.type;
        this.font = b.font
    }
};
sre.SemanticTree.Node.prototype.addMathmlNodes_ = function (a) {
    for (var b = 0, c; c = a[b]; b++)-1 == this.mathml.indexOf(c) && this.mathml.push(c)
};
sre.SemanticTree.Node.prototype.removeMathmlNodes_ = function (a) {
    for (var b = this.mathml, c = 0, d; d = a[c]; c++)d = b.indexOf(d), -1 != d && goog.array.removeAt(b, d);
    this.mathml = b
};
sre.SemanticTree.Node.prototype.appendChild_ = function (a) {
    this.childNodes.push(a);
    this.addMathmlNodes_(a.mathml);
    a.parent = this
};
sre.SemanticTree.Node.prototype.replaceChild_ = function (a, b) {
    var c = this.childNodes.indexOf(a);
    if (-1 != c) {
        b.parent = this;
        a.parent = null;
        this.childNodes[c] = b;
        var c = a.mathml.filter(function (a) {
            return -1 == b.mathml.indexOf(a)
        }), d = b.mathml.filter(function (b) {
            return -1 == a.mathml.indexOf(b)
        });
        this.removeMathmlNodes_(c);
        this.addMathmlNodes_(d)
    }
};
sre.SemanticTree.Node.prototype.appendContentNode_ = function (a) {
    a && (this.contentNodes.push(a), this.addMathmlNodes_(a.mathml), a.parent = this)
};
sre.SemanticTree.Node.prototype.removeContentNode_ = function (a) {
    a && (a = this.contentNodes.indexOf(a), -1 != a && goog.array.removeAt(this.contentNodes, a))
};
sre.SemanticTree.Node.prototype.equals = function (a) {
    if (!a || this.type !== a.type || this.role !== a.role || this.textContent !== a.textContent || this.childNodes.length !== a.childNodes.length || this.contentNodes.length !== a.contentNodes.length)return !1;
    for (var b = 0, c, d; c = this.childNodes[b], d = a.childNodes[b]; b++)if (!c.equals(d))return !1;
    for (b = 0; c = this.contentNodes[b], d = a.contentNodes[b]; b++)if (!c.equals(d))return !1;
    return !0
};
sre.SemanticTree.prototype.parseMathml_ = function (a) {
    var b = sre.DomUtil.toArray(a.childNodes);
    switch (sre.SemanticUtil.tagName(a)) {
        case "SEMANTICS":
            if (0 < b.length) {
                b = this.parseMathml_(b[0]);
                break
            }
        case "MATH":
        case "MROW":
        case "MPADDED":
        case "MSTYLE":
            return b = sre.SemanticUtil.purgeNodes(b), b = 1 == b.length ? this.parseMathml_(b[0]) : this.processRow_(this.parseMathmlChildren_(b)), b.mathml.unshift(a), b;
        case "MFRAC":
            b = this.makeFractionNode_(this.parseMathml_(b[0]), this.parseMathml_(b[1]));
            break;
        case "MSUB":
        case "MSUP":
        case "MSUBSUP":
        case "MOVER":
        case "MUNDER":
        case "MUNDEROVER":
            b =
                this.makeLimitNode_(sre.SemanticUtil.tagName(a), this.parseMathmlChildren_(b));
            break;
        case "MROOT":
            b = this.makeBranchNode_(sre.SemanticAttr.Type.ROOT, [this.parseMathml_(b[1]), this.parseMathml_(b[0])], []);
            break;
        case "MSQRT":
            b = this.parseMathmlChildren_(sre.SemanticUtil.purgeNodes(b));
            b = this.makeBranchNode_(sre.SemanticAttr.Type.SQRT, [this.processRow_(b)], []);
            break;
        case "MTABLE":
            b = this.makeBranchNode_(sre.SemanticAttr.Type.TABLE, this.parseMathmlChildren_(b), []);
            sre.SemanticTree.tableIsMultiline_(b) && this.tableToMultiline_(b);
            break;
        case "MTR":
            b = this.makeBranchNode_(sre.SemanticAttr.Type.ROW, this.parseMathmlChildren_(b), []);
            b.role = sre.SemanticAttr.Role.TABLE;
            break;
        case "MTD":
            b = this.parseMathmlChildren_(sre.SemanticUtil.purgeNodes(b));
            b = this.makeBranchNode_(sre.SemanticAttr.Type.CELL, [this.processRow_(b)], []);
            b.role = sre.SemanticAttr.Role.TABLE;
            break;
        case "MS":
        case "MTEXT":
            b = this.makeLeafNode_(a);
            b.type = sre.SemanticAttr.Type.TEXT;
            "MS" === sre.SemanticUtil.tagName(a) && (b.role = sre.SemanticAttr.Role.STRING);
            sre.SemanticTree.exprFont_(b);
            break;
        case "MI":
            b = this.makeIdentifierNode_(a);
            break;
        case "MN":
            b = this.makeLeafNode_(a);
            if (b.type == sre.SemanticAttr.Type.UNKNOWN || b.type == sre.SemanticAttr.Type.IDENTIFIER)b.type = sre.SemanticAttr.Type.NUMBER;
            sre.SemanticTree.numberRole_(b);
            sre.SemanticTree.exprFont_(b);
            break;
        case "MO":
            b = this.makeLeafNode_(a);
            b.type == sre.SemanticAttr.Type.UNKNOWN && (b.type = sre.SemanticAttr.Type.OPERATOR);
            break;
        case "MFENCED":
            b = this.processMfenced_(a, this.parseMathmlChildren_(sre.SemanticUtil.purgeNodes(b)));
            b = this.processTablesInRow_([b])[0];
            break;
        case "MENCLOSE":
            b = this.parseMathmlChildren_(sre.SemanticUtil.purgeNodes(b));
            b = this.makeBranchNode_(sre.SemanticAttr.Type.ENCLOSE, [this.processRow_(b)], []);
            b.role = a.getAttribute("notation") || sre.SemanticAttr.Role.UNKNOWN;
            break;
        case "MMULTISCRIPTS":
            b = this.processMultiScript_(b);
            break;
        case "NONE":
            b = this.makeEmptyNode_();
            break;
        default:
            b = this.makeUnprocessed_(a)
    }
    b.mathml.unshift(a);
    b.mathmlTree = a;
    return b
};
sre.SemanticTree.prototype.parseMathmlChildren_ = function (a) {
    for (var b = [], c = 0, d; d = a[c]; c++)b.push(this.parseMathml_(d));
    return b
};
sre.SemanticTree.prototype.makeUnprocessed_ = function (a) {
    var b = this.createNode_();
    b.mathml = [a];
    return b
};
sre.SemanticTree.prototype.makeEmptyNode_ = function () {
    var a = this.createNode_();
    a.type = sre.SemanticAttr.Type.EMPTY;
    return a
};
sre.SemanticTree.prototype.makeContentNode_ = function (a) {
    var b = this.createNode_();
    b.updateContent_(a);
    return b
};
sre.SemanticTree.prototype.makeMultipleContentNodes_ = function (a, b) {
    for (var c = [], d = 0; d < a; d++)c.push(this.makeContentNode_(b));
    return c
};
sre.SemanticTree.prototype.makeLeafNode_ = function (a) {
    if (!a.textContent)return this.makeEmptyNode_();
    var b = this.makeContentNode_(a.textContent);
    b.font = a.getAttribute("mathvariant") || b.font;
    return b
};
sre.SemanticTree.prototype.makeBranchNode_ = function (a, b, c, d) {
    var e = this.createNode_();
    d && e.updateContent_(d);
    e.type = a;
    e.childNodes = b;
    e.contentNodes = c;
    b.concat(c).forEach(function (a) {
        a.parent = e;
        e.addMathmlNodes_(a.mathml)
    });
    return e
};
sre.SemanticTree.prototype.makeIdentifierNode_ = function (a) {
    var b = this.makeLeafNode_(a), c = a.getAttribute("mathvariant");
    if ("MathML-Unit" === a.getAttribute("class"))b.type = sre.SemanticAttr.Type.IDENTIFIER, b.role = sre.SemanticAttr.Role.UNIT; else if (!c && 1 == b.textContent.length && (b.role == sre.SemanticAttr.Role.INTEGER || b.role == sre.SemanticAttr.Role.LATINLETTER || b.role == sre.SemanticAttr.Role.GREEKLETTER) && b.font == sre.SemanticAttr.Font.NORMAL)return b.font = sre.SemanticAttr.Font.ITALIC, b;
    b.type == sre.SemanticAttr.Type.UNKNOWN &&
    (b.type = sre.SemanticAttr.Type.IDENTIFIER);
    sre.SemanticTree.exprFont_(b);
    return b
};
sre.SemanticTree.prototype.makeImplicitNode_ = function (a) {
    a = this.getMixedNumbers_(a);
    a = this.combineUnits_(a);
    if (1 == a.length)return a[0];
    var b = this.makeMultipleContentNodes_(a.length - 1, sre.SemanticAttr.invisibleTimes()), c = this.makeInfixNode_(a, b[0]);
    c.role = sre.SemanticAttr.Role.IMPLICIT;
    b.forEach(function (a) {
        a.parent = c
    });
    c.contentNodes = b;
    return c
};
sre.SemanticTree.prototype.makeInfixNode_ = function (a, b) {
    var c = this.makeBranchNode_(sre.SemanticAttr.Type.INFIXOP, a, [b], sre.SemanticTree.getEmbellishedInner_(b).textContent);
    c.role = b.role;
    return c
};
sre.SemanticTree.prototype.makeConcatNode_ = function (a, b, c) {
    if (0 == b.length)return a;
    var d = b.map(function (a) {
        return sre.SemanticTree.getEmbellishedInner_(a).textContent
    }).join(" ");
    a = this.makeBranchNode_(c, [a], b, d);
    1 < b.length && (a.role = sre.SemanticAttr.Role.MULTIOP);
    return a
};
sre.SemanticTree.prototype.makePrefixNode_ = function (a, b) {
    for (var c = sre.SemanticTree.partitionNodes_(b, sre.SemanticTree.attrPred_("role", "SUBTRACTION")), d = this.makeConcatNode_(a, c.comp.pop(), sre.SemanticAttr.Type.PREFIXOP); 0 < c.rel.length;)d = this.makeConcatNode_(d, [c.rel.pop()], sre.SemanticAttr.Type.PREFIXOP), d.role = sre.SemanticAttr.Role.NEGATIVE, d = this.makeConcatNode_(d, c.comp.pop(), sre.SemanticAttr.Type.PREFIXOP);
    return d
};
sre.SemanticTree.prototype.makePostfixNode_ = function (a, b) {
    return b.length ? this.makeConcatNode_(a, b, sre.SemanticAttr.Type.POSTFIXOP) : a
};
sre.SemanticTree.prototype.processRow_ = function (a) {
    a = a.filter(function (a) {
        return !sre.SemanticTree.attrPred_("type", "EMPTY")(a)
    });
    if (0 == a.length)return this.makeEmptyNode_();
    a = this.getFencesInRow_(a);
    a = this.processTablesInRow_(a);
    a = this.getPunctuationInRow_(a);
    a = this.getTextInRow_(a);
    a = this.getFunctionsInRow_(a);
    return this.processRelationsInRow_(a)
};
sre.SemanticTree.prototype.combineUnits_ = function (a) {
    var b = sre.SemanticTree.partitionNodes_(a, function (a) {
        return !sre.SemanticTree.attrPred_("role", "UNIT")(a)
    });
    if (a.length === b.rel.length)return b.rel;
    a = [];
    do {
        var c = b.comp.shift(), d = b.rel.shift();
        1 == c.length && (a = a.concat(c));
        if (1 < c.length) {
            var e = this.makeContentNode_(sre.SemanticAttr.invisibleTimes()), c = this.makeInfixNode_(c, e);
            c.role = sre.SemanticAttr.Role.UNIT;
            a.push(c)
        }
        d && a.push(d)
    } while (d);
    return a
};
sre.SemanticTree.prototype.getMixedNumbers_ = function (a) {
    var b = sre.SemanticTree.partitionNodes_(a, function (a) {
        return sre.SemanticTree.attrPred_("type", "FRACTION")(a) && sre.SemanticTree.attrPred_("role", "VULGAR")(a)
    });
    if (!b.rel.length)return a;
    a = [];
    for (var c = 0, d; d = b.rel[c]; c++) {
        var e = b.comp[c], f = e.length - 1;
        e[f] && sre.SemanticTree.attrPred_("type", "NUMBER")(e[f]) && sre.SemanticTree.attrPred_("role", "INTEGER")(e[f]) ? (d = this.makeBranchNode_(sre.SemanticAttr.Type.NUMBER, [e[f], d], []), d.role = sre.SemanticAttr.Role.MIXED,
            a = a.concat(e.slice(0, f))) : a = a.concat(e);
        a.push(d)
    }
    return a.concat(b.comp[b.comp.length - 1])
};
sre.SemanticTree.prototype.getTextInRow_ = function (a) {
    if (1 >= a.length)return a;
    var b = sre.SemanticTree.partitionNodes_(a, sre.SemanticTree.attrPred_("type", "TEXT"));
    if (0 == b.rel.length)return a;
    a = [];
    var c = b.comp[0];
    0 < c.length && a.push(this.processRow_(c));
    for (var d = 0; c = b.rel[d]; d++)a.push(c), c = b.comp[d + 1], 0 < c.length && a.push(this.processRow_(c));
    return [this.makeDummyNode_(a)]
};
sre.SemanticTree.prototype.processRelationsInRow_ = function (a) {
    var b = sre.SemanticTree.partitionNodes_(a, sre.SemanticTree.isRelation_), c = b.rel[0];
    if (!c)return this.processOperationsInRow_(a);
    if (1 == a.length)return a[0];
    a = b.comp.map(goog.bind(this.processOperationsInRow_, this));
    if (b.rel.some(function (a) {
            return !a.equals(c)
        }))return this.makeBranchNode_(sre.SemanticAttr.Type.MULTIREL, a, b.rel);
    b = this.makeBranchNode_(sre.SemanticAttr.Type.RELSEQ, a, b.rel, sre.SemanticTree.getEmbellishedInner_(c).textContent);
    b.role = c.role;
    return b
};
sre.SemanticTree.prototype.processOperationsInRow_ = function (a) {
    if (0 == a.length)return this.makeEmptyNode_();
    if (1 == a.length)return a[0];
    for (var b = []; 0 < a.length && sre.SemanticTree.isOperator_(a[0]);)b.push(a.shift());
    if (0 == a.length)return this.makePrefixNode_(b.pop(), b);
    if (1 == a.length)return this.makePrefixNode_(a[0], b);
    a = sre.SemanticTree.sliceNodes_(a, sre.SemanticTree.isOperator_);
    b = this.makePrefixNode_(this.makeImplicitNode_(a.head), b);
    return a.div ? this.makeOperationsTree_(a.tail, b, a.div) : b
};
sre.SemanticTree.prototype.makeOperationsTree_ = function (a, b, c, d) {
    d = d || [];
    if (0 == a.length)return d.unshift(c), b.type == sre.SemanticAttr.Type.INFIXOP ? (d = this.makePostfixNode_(b.childNodes.pop(), d), b.appendChild_(d), b) : this.makePostfixNode_(b, d);
    a = sre.SemanticTree.sliceNodes_(a, sre.SemanticTree.isOperator_);
    if (0 == a.head.length)return d.push(a.div), this.makeOperationsTree_(a.tail, b, c, d);
    d = this.makePrefixNode_(this.makeImplicitNode_(a.head), d);
    b = this.appendOperand_(b, c, d);
    return a.div ? this.makeOperationsTree_(a.tail,
        b, a.div, []) : b
};
sre.SemanticTree.prototype.appendOperand_ = function (a, b, c) {
    return a.type != sre.SemanticAttr.Type.INFIXOP ? this.makeInfixNode_([a, c], b) : this.appendExistingOperator_(a, b, c) ? a : b.role == sre.SemanticAttr.Role.MULTIPLICATION ? this.appendMultiplicativeOp_(a, b, c) : this.appendAdditiveOp_(a, b, c)
};
sre.SemanticTree.prototype.appendMultiplicativeOp_ = function (a, b, c) {
    if (a.role == sre.SemanticAttr.Role.IMPLICIT)return this.makeInfixNode_([a, c], b);
    for (var d = a, e = a.childNodes[a.childNodes.length - 1]; e && e.type == sre.SemanticAttr.Type.INFIXOP;)d = e, e = d.childNodes[a.childNodes.length - 1];
    b = this.makeInfixNode_([d.childNodes.pop(), c], b);
    d.appendChild_(b);
    return a
};
sre.SemanticTree.prototype.appendAdditiveOp_ = function (a, b, c) {
    return this.makeInfixNode_([a, c], b)
};
sre.SemanticTree.prototype.appendExistingOperator_ = function (a, b, c) {
    return a && a.type == sre.SemanticAttr.Type.INFIXOP && a.role !== sre.SemanticAttr.Role.IMPLICIT ? a.contentNodes[0].equals(b) ? (a.appendContentNode_(b), a.appendChild_(c), !0) : this.appendExistingOperator_(a.childNodes[a.childNodes.length - 1], b, c) : !1
};
sre.SemanticTree.prototype.getFencesInRow_ = function (a) {
    a = sre.SemanticTree.partitionNodes_(a, sre.SemanticTree.isFence_);
    a = sre.SemanticTree.purgeFences_(a);
    var b = a.comp.shift();
    return this.processFences_(a.rel, a.comp, [], [b])
};
sre.SemanticTree.prototype.processFences_ = function (a, b, c, d) {
    if (0 == a.length && 0 == c.length)return d[0];
    var e = sre.SemanticTree.attrPred_("role", "OPEN");
    if (0 == a.length) {
        for (a = d.shift(); 0 < c.length;) {
            if (e(c[0]))b = c.shift(), sre.SemanticTree.fenceToPunct_(b), a.push(b); else {
                c = sre.SemanticTree.sliceNodes_(c, e);
                var f = c.head.length - 1, g = this.processNeutralFences_(c.head, d.slice(0, f));
                d = d.slice(f);
                a.push.apply(a, g);
                c.div && c.tail.unshift(c.div);
                c = c.tail
            }
            a.push.apply(a, d.shift())
        }
        return a
    }
    f = c[c.length - 1];
    g = a[0].role;
    if (g == sre.SemanticAttr.Role.OPEN || g == sre.SemanticAttr.Role.NEUTRAL && (!f || a[0].textContent != f.textContent))return c.push(a.shift()), d.push(b.shift()), this.processFences_(a, b, c, d);
    if (f && (g == sre.SemanticAttr.Role.CLOSE && f.role == sre.SemanticAttr.Role.OPEN || g == sre.SemanticAttr.Role.NEUTRAL && a[0].textContent == f.textContent))return e = this.makeHorizontalFencedNode_(c.pop(), a.shift(), d.pop()), d.push(d.pop().concat([e], b.shift())), this.processFences_(a, b, c, d);
    if (f && g == sre.SemanticAttr.Role.CLOSE && f.role == sre.SemanticAttr.Role.NEUTRAL &&
        c.some(e))return c = sre.SemanticTree.sliceNodes_(c, e, !0), e = d.pop(), f = d.length - c.tail.length + 1, g = this.processNeutralFences_(c.tail, d.slice(f)), d = d.slice(0, f), e = this.makeHorizontalFencedNode_(c.div, a.shift(), d.pop().concat(g, e)), d.push(d.pop().concat([e], b.shift())), this.processFences_(a, b, c.head, d);
    e = a.shift();
    sre.SemanticTree.fenceToPunct_(e);
    d.push(d.pop().concat([e], b.shift()));
    return this.processFences_(a, b, c, d)
};
sre.SemanticTree.prototype.processNeutralFences_ = function (a, b) {
    if (0 == a.length)return a;
    if (1 == a.length)return sre.SemanticTree.fenceToPunct_(a[0]), a;
    var c = a.shift(), d = sre.SemanticTree.sliceNodes_(a, function (a) {
        return a.textContent == c.textContent
    });
    if (!d.div) {
        sre.SemanticTree.fenceToPunct_(c);
        var e = b.shift();
        e.unshift(c);
        return e.concat(this.processNeutralFences_(a, b))
    }
    var f = this.combineFencedContent_(c, d.div, d.head, b);
    return 0 < d.tail.length ? (e = f.shift(), d = this.processNeutralFences_(d.tail, f), e.concat(d)) :
        f[0]
};
sre.SemanticTree.prototype.combineFencedContent_ = function (a, b, c, d) {
    if (0 == c.length)return a = this.makeHorizontalFencedNode_(a, b, d.shift()), d.unshift(a), d;
    var e = d.shift(), f = c.length - 1, g = d.slice(0, f);
    d = d.slice(f);
    f = d.shift();
    c = this.processNeutralFences_(c, g);
    e.push.apply(e, c);
    e.push.apply(e, f);
    a = this.makeHorizontalFencedNode_(a, b, e);
    0 < d.length ? d[0].unshift(a) : d = [[a]];
    return d
};
sre.SemanticTree.fenceToPunct_ = function (a) {
    var b;
    switch (a.role) {
        case sre.SemanticAttr.Role.NEUTRAL:
            b = sre.SemanticAttr.Role.VBAR;
            break;
        case sre.SemanticAttr.Role.OPEN:
            b = sre.SemanticAttr.Role.OPENFENCE;
            break;
        case sre.SemanticAttr.Role.CLOSE:
            b = sre.SemanticAttr.Role.CLOSEFENCE;
            break;
        default:
            return
    }
    for (; a.embellished;)a.embellished = sre.SemanticAttr.Type.PUNCTUATION, a.role = b, a = a.childNodes[0];
    a.type = sre.SemanticAttr.Type.PUNCTUATION;
    a.role = b
};
sre.SemanticTree.prototype.makeHorizontalFencedNode_ = function (a, b, c) {
    c = this.processRow_(c);
    b = this.makeBranchNode_(sre.SemanticAttr.Type.FENCED, [c], [a, b]);
    b.role = a.role == sre.SemanticAttr.Role.OPEN ? sre.SemanticAttr.Role.LEFTRIGHT : a.role;
    return sre.SemanticTree.rewriteFencedNode_(b)
};
sre.SemanticTree.prototype.getPunctuationInRow_ = function (a) {
    var b = sre.SemanticTree.partitionNodes_(a, function (a) {
        return sre.SemanticTree.isPunctuation_(a) && !sre.SemanticTree.attrPred_("role", "ELLIPSIS")(a)
    });
    if (0 == b.rel.length)return a;
    a = [];
    var c = b.comp.shift();
    0 < c.length && a.push(this.processRow_(c));
    for (var d = 0; 0 < b.comp.length;)a.push(b.rel[d++]), c = b.comp.shift(), 0 < c.length && a.push(this.processRow_(c));
    return [this.makePunctuatedNode_(a, b.rel)]
};
sre.SemanticTree.prototype.makePunctuatedNode_ = function (a, b) {
    var c = this.makeBranchNode_(sre.SemanticAttr.Type.PUNCTUATED, a, b);
    if (b.length == a.length) {
        var d = b[0].role;
        if (d != sre.SemanticAttr.Role.UNKNOWN && b.every(function (a) {
                return a.role == d
            }))return c.role = d, c
    }
    1 == b.length && a[0].type == sre.SemanticAttr.Type.PUNCTUATION ? c.role = sre.SemanticAttr.Role.STARTPUNCT : 1 == b.length && a[a.length - 1].type == sre.SemanticAttr.Type.PUNCTUATION ? c.role = sre.SemanticAttr.Role.ENDPUNCT : b.every(sre.SemanticTree.attrPred_("role",
        "DUMMY")) ? c.role = sre.SemanticAttr.Role.TEXT : c.role = sre.SemanticAttr.Role.SEQUENCE;
    return c
};
sre.SemanticTree.prototype.makeDummyNode_ = function (a) {
    var b = this.makeMultipleContentNodes_(a.length - 1, sre.SemanticAttr.invisibleComma());
    b.forEach(function (a) {
        a.role = sre.SemanticAttr.Role.DUMMY
    });
    return this.makePunctuatedNode_(a, b)
};
sre.SemanticTree.prototype.makeLimitNode_ = function (a, b) {
    var c = b[0], d = sre.SemanticTree.attrPred_("type", "FUNCTION")(c), d = sre.SemanticTree.attrPred_("type", "LARGEOP")(c) || sre.SemanticTree.attrPred_("type", "LIMBOTH")(c) || sre.SemanticTree.attrPred_("type", "LIMLOWER")(c) || sre.SemanticTree.attrPred_("type", "LIMUPPER")(c) || d && sre.SemanticTree.attrPred_("role", "LIMFUNC")(c), e = sre.SemanticAttr.Type.UNKNOWN;
    if (d)switch (a) {
        case "MSUB":
        case "MUNDER":
            e = sre.SemanticAttr.Type.LIMLOWER;
            break;
        case "MSUP":
        case "MOVER":
            e =
                sre.SemanticAttr.Type.LIMUPPER;
            break;
        case "MSUBSUP":
        case "MUNDEROVER":
            e = sre.SemanticAttr.Type.LIMBOTH
    } else switch (a) {
        case "MSUB":
            e = sre.SemanticAttr.Type.SUBSCRIPT;
            break;
        case "MSUP":
            e = sre.SemanticAttr.Type.SUPERSCRIPT;
            break;
        case "MSUBSUP":
            var f = this.makeBranchNode_(sre.SemanticAttr.Type.SUBSCRIPT, [c, b[1]], []);
            f.role = sre.SemanticAttr.Role.SUBSUP;
            b = [f, b[2]];
            e = sre.SemanticAttr.Type.SUPERSCRIPT;
            break;
        case "MOVER":
            e = sre.SemanticAttr.Type.OVERSCORE;
            sre.SemanticTree.isAccent_(b[1]) && (b[1].role = sre.SemanticAttr.Role.OVERACCENT);
            break;
        case "MUNDER":
            e = sre.SemanticAttr.Type.UNDERSCORE;
            sre.SemanticTree.isAccent_(b[1]) && (b[1].role = sre.SemanticAttr.Role.UNDERACCENT);
            break;
        default:
            f = sre.SemanticTree.isAccent_(b[1]), d = sre.SemanticTree.isAccent_(b[2]), f && (b[1].role = sre.SemanticAttr.Role.UNDERACCENT), d && (b[2].role = sre.SemanticAttr.Role.OVERACCENT), d && !f ? (f = this.makeBranchNode_(sre.SemanticAttr.Type.OVERSCORE, [c, b[2]], []), f.role = c.role, b = [f, b[1]], e = sre.SemanticAttr.Type.UNDERSCORE) : (f = this.makeBranchNode_(sre.SemanticAttr.Type.UNDERSCORE,
                [c, b[1]], []), f.role = c.role, b = [f, b[2]], e = sre.SemanticAttr.Type.OVERSCORE)
    }
    d = this.makeBranchNode_(e, b, []);
    e = sre.SemanticTree.isEmbellished_(c);
    f && (f.embellished = e);
    d.embellished = e;
    d.role = c.role;
    return d
};
sre.SemanticTree.isAccent_ = function (a) {
    return sre.SemanticTree.attrPred_("type", "FENCE")(a) || sre.SemanticTree.attrPred_("type", "PUNCTUATION")(a) || sre.SemanticTree.attrPred_("type", "OPERATOR")(a) || sre.SemanticTree.attrPred_("type", "RELATION")(a) || sre.SemanticTree.attrPred_("type", "IDENTIFIER")(a) && sre.SemanticTree.attrPred_("role", "UNKNOWN")(a) && !a.textContent.match(new RegExp(sre.SemanticAttr.getInstance().allLetters.join("|")))
};
sre.SemanticTree.prototype.getFunctionsInRow_ = function (a, b) {
    var c = b || [];
    if (0 == a.length)return c;
    var d = a.shift(), e = sre.SemanticTree.classifyFunction_(d, a);
    if (!e)return c.push(d), this.getFunctionsInRow_(a, c);
    var f = this.getFunctionsInRow_(a, []), d = this.getFunctionArgs_(d, f, e);
    return c.concat(d)
};
sre.SemanticTree.classifyFunction_ = function (a, b) {
    if (a.type == sre.SemanticAttr.Type.APPL || a.type == sre.SemanticAttr.Type.BIGOP || a.type == sre.SemanticAttr.Type.INTEGRAL)return "";
    if (b[0] && b[0].textContent == sre.SemanticAttr.functionApplication()) {
        b.shift();
        var c = sre.SemanticAttr.Role.SIMPLEFUNC;
        if (a.role === sre.SemanticAttr.Role.PREFIXFUNC || a.role === sre.SemanticAttr.Role.LIMFUNC)c = a.role;
        sre.SemanticTree.propagateFunctionRole_(a, c);
        return "prefix"
    }
    switch (a.role) {
        case sre.SemanticAttr.Role.INTEGRAL:
            return "integral";
        case sre.SemanticAttr.Role.SUM:
            return "bigop";
        case sre.SemanticAttr.Role.PREFIXFUNC:
        case sre.SemanticAttr.Role.LIMFUNC:
            return "prefix";
        default:
            if (a.type == sre.SemanticAttr.Type.IDENTIFIER || a.role == sre.SemanticAttr.Role.LATINLETTER || a.role == sre.SemanticAttr.Role.GREEKLETTER || a.role == sre.SemanticAttr.Role.OTHERLETTER)return "simple"
    }
    return ""
};
sre.SemanticTree.propagateFunctionRole_ = function (a, b) {
    a && (sre.SemanticTree.attrPred_("role", "SUBSUP")(a) || (a.role = b), sre.SemanticTree.propagateFunctionRole_(a.childNodes[0], b))
};
sre.SemanticTree.prototype.getFunctionArgs_ = function (a, b, c) {
    switch (c) {
        case "integral":
            b = this.getIntegralArgs_(b);
            var d = this.processRow_(b.integrand);
            a = this.makeIntegralNode_(a, d, b.intvar);
            b.rest.unshift(a);
            return b.rest;
        case "prefix":
            if (b[0] && b[0].type == sre.SemanticAttr.Type.FENCED)return a = this.makeFunctionNode_(a, b.shift()), b.unshift(a), b;
        case "bigop":
            d = sre.SemanticTree.sliceNodes_(b, sre.SemanticTree.prefixFunctionBoundary_);
            if (!d.head.length)return b.unshift(a), b;
            b = this.processRow_(d.head);
            a = "prefix" ==
            c ? this.makeFunctionNode_(a, b) : this.makeBigOpNode_(a, b);
            d.div && d.tail.unshift(d.div);
            d.tail.unshift(a);
            return d.tail;
        case "simple":
            if (0 == b.length)return [a];
            d = b[0];
            if (d.type == sre.SemanticAttr.Type.FENCED && d.role != sre.SemanticAttr.Role.NEUTRAL && this.simpleFunctionHeuristic_(d))return sre.SemanticTree.propagateFunctionRole_(a, sre.SemanticAttr.Role.SIMPLEFUNC), a = this.makeFunctionNode_(a, b.shift()), b.unshift(a), b;
            b.unshift(a);
            return b
    }
};
sre.SemanticTree.prototype.getIntegralArgs_ = function (a, b) {
    var c = b || [];
    if (0 == a.length)return {integrand: c, intvar: null, rest: a};
    var d = a[0];
    if (sre.SemanticTree.generalFunctionBoundary_(d))return {integrand: c, intvar: null, rest: a};
    if (sre.SemanticTree.integralDxBoundarySingle_(d))return {integrand: c, intvar: d, rest: a.slice(1)};
    if (a[1] && sre.SemanticTree.integralDxBoundary_(d, a[1])) {
        var e = this.makeContentNode_(sre.SemanticAttr.invisibleComma()), d = this.makePunctuatedNode_([d, e, a[1]], [e]);
        d.role = sre.SemanticAttr.Role.INTEGRAL;
        return {integrand: c, intvar: d, rest: a.slice(2)}
    }
    c.push(a.shift());
    return this.getIntegralArgs_(a, c)
};
sre.SemanticTree.prototype.makeFunctionNode_ = function (a, b) {
    var c = this.makeContentNode_(sre.SemanticAttr.functionApplication());
    c.type = sre.SemanticAttr.Type.PUNCTUATION;
    c.role = sre.SemanticAttr.Role.APPLICATION;
    var d = sre.SemanticTree.getFunctionOp_(a, function (a) {
        return sre.SemanticTree.attrPred_("type", "FUNCTION")(a) || sre.SemanticTree.attrPred_("type", "IDENTIFIER")(a) && sre.SemanticTree.attrPred_("role", "SIMPLEFUNC")(a)
    }), c = this.makeBranchNode_(sre.SemanticAttr.Type.APPL, [a, b], d ? [c, d] : [c]);
    c.role = a.role;
    return c
};
sre.SemanticTree.prototype.makeBigOpNode_ = function (a, b) {
    var c = sre.SemanticTree.getFunctionOp_(a, sre.SemanticTree.attrPred_("type", "LARGEOP")), c = this.makeBranchNode_(sre.SemanticAttr.Type.BIGOP, [a, b], c ? [c] : []);
    c.role = a.role;
    return c
};
sre.SemanticTree.prototype.makeIntegralNode_ = function (a, b, c) {
    b = b || this.makeEmptyNode_();
    c = c || this.makeEmptyNode_();
    var d = sre.SemanticTree.getFunctionOp_(a, sre.SemanticTree.attrPred_("type", "LARGEOP"));
    b = this.makeBranchNode_(sre.SemanticAttr.Type.INTEGRAL, [a, b, c], d ? [d] : []);
    b.role = a.role;
    return b
};
sre.SemanticTree.getFunctionOp_ = function (a, b) {
    if (b(a))return a;
    for (var c = 0, d; d = a.childNodes[c]; c++)if (d = sre.SemanticTree.getFunctionOp_(d, b))return d;
    return null
};
sre.SemanticTree.prototype.simpleFunctionHeuristic_ = function (a) {
    a = a.childNodes;
    if (0 == a.length)return !0;
    if (1 < a.length)return !1;
    a = a[0];
    return a.type != sre.SemanticAttr.Type.INFIXOP || a.role == sre.SemanticAttr.Role.IMPLICIT && !a.childNodes.some(sre.SemanticTree.attrPred_("type", "INFIXOP")) ? !0 : !1
};
sre.SemanticTree.prefixFunctionBoundary_ = function (a) {
    return sre.SemanticTree.isOperator_(a) || sre.SemanticTree.generalFunctionBoundary_(a)
};
sre.SemanticTree.integralDxBoundary_ = function (a, b) {
    return !!b && sre.SemanticTree.attrPred_("type", "IDENTIFIER")(b) && sre.SemanticAttr.isCharacterD(a.textContent)
};
sre.SemanticTree.integralDxBoundarySingle_ = function (a) {
    if (sre.SemanticTree.attrPred_("type", "IDENTIFIER")(a)) {
        var b = a.textContent[0];
        return b && a.textContent[1] && sre.SemanticAttr.isCharacterD(b)
    }
    return !1
};
sre.SemanticTree.generalFunctionBoundary_ = function (a) {
    return sre.SemanticTree.isRelation_(a) || sre.SemanticTree.isPunctuation_(a)
};
sre.SemanticTree.prototype.processTablesInRow_ = function (a) {
    a = sre.SemanticTree.partitionNodes_(a, sre.SemanticTree.tableIsMatrixOrVector_);
    for (var b = [], c = 0, d; d = a.rel[c]; c++)b = b.concat(a.comp.shift()), b.push(this.tableToMatrixOrVector_(d));
    b = b.concat(a.comp.shift());
    a = sre.SemanticTree.partitionNodes_(b, sre.SemanticTree.isTableOrMultiline_);
    b = [];
    for (c = 0; d = a.rel[c]; c++) {
        var e = a.comp.shift();
        sre.SemanticTree.tableIsCases_(d, e) && this.tableToCases_(d, e.pop());
        b = b.concat(e);
        b.push(d)
    }
    return b.concat(a.comp.shift())
};
sre.SemanticTree.isTableOrMultiline_ = function (a) {
    return !!a && (sre.SemanticTree.attrPred_("type", "TABLE")(a) || sre.SemanticTree.attrPred_("type", "MULTILINE")(a))
};
sre.SemanticTree.tableIsMatrixOrVector_ = function (a) {
    return !!a && sre.SemanticTree.attrPred_("type", "FENCED")(a) && (sre.SemanticTree.attrPred_("role", "LEFTRIGHT")(a) || sre.SemanticTree.attrPred_("role", "NEUTRAL")(a)) && 1 == a.childNodes.length && sre.SemanticTree.isTableOrMultiline_(a.childNodes[0])
};
sre.SemanticTree.prototype.tableToMatrixOrVector_ = function (a) {
    var b = a.childNodes[0];
    sre.SemanticTree.attrPred_("type", "MULTILINE")(b) ? this.tableToVector_(a) : this.tableToMatrix_(a);
    a.contentNodes.forEach(goog.bind(b.appendContentNode_, b));
    a = 0;
    for (var c; c = b.childNodes[a]; a++)sre.SemanticTree.assignRoleToRow_(c, sre.SemanticTree.getComponentRoles_(b));
    return b
};
sre.SemanticTree.prototype.tableToVector_ = function (a) {
    var b = a.childNodes[0];
    b.type = sre.SemanticAttr.Type.VECTOR;
    1 === b.childNodes.length ? this.tableToSquare_(a) : 2 === b.childNodes.length && (b.role = sre.SemanticAttr.Role.BINOMIAL)
};
sre.SemanticTree.prototype.tableToMatrix_ = function (a) {
    var b = a.childNodes[0];
    b.type = sre.SemanticAttr.Type.MATRIX;
    b.childNodes && 0 < b.childNodes.length && b.childNodes[0].childNodes && b.childNodes.length === b.childNodes[0].childNodes.length ? this.tableToSquare_(a) : b.childNodes && 1 === b.childNodes.length && (b.role = sre.SemanticAttr.Role.ROWVECTOR)
};
sre.SemanticTree.prototype.tableToSquare_ = function (a) {
    var b = a.childNodes[0];
    sre.SemanticTree.attrPred_("role", "NEUTRAL")(a) ? b.role = sre.SemanticAttr.Role.DETERMINANT : b.role = sre.SemanticAttr.Role.SQUAREMATRIX
};
sre.SemanticTree.getComponentRoles_ = function (a) {
    var b = a.role;
    return b && b !== sre.SemanticAttr.Role.UNKNOWN ? b : sre.SemanticAttr.Role[a.type.toUpperCase()] || sre.SemanticAttr.Role.UNKNOWN
};
sre.SemanticTree.tableIsCases_ = function (a, b) {
    return 0 < b.length && sre.SemanticTree.attrPred_("role", "OPENFENCE")(b[b.length - 1])
};
sre.SemanticTree.prototype.tableToCases_ = function (a, b) {
    for (var c = 0, d; d = a.childNodes[c]; c++)sre.SemanticTree.assignRoleToRow_(d, sre.SemanticAttr.Role.CASES);
    a.type = sre.SemanticAttr.Type.CASES;
    a.appendContentNode_(b);
    return a
};
sre.SemanticTree.tableIsMultiline_ = function (a) {
    return a.childNodes.every(function (a) {
        return 1 >= a.childNodes.length
    })
};
sre.SemanticTree.prototype.tableToMultiline_ = function (a) {
    a.type = sre.SemanticAttr.Type.MULTILINE;
    for (var b = 0, c; c = a.childNodes[b]; b++)sre.SemanticTree.rowToLine_(c, sre.SemanticAttr.Role.MULTILINE)
};
sre.SemanticTree.rowToLine_ = function (a, b) {
    var c = b || sre.SemanticAttr.Role.UNKNOWN;
    sre.SemanticTree.attrPred_("type", "ROW")(a) && 1 == a.childNodes.length && sre.SemanticTree.attrPred_("type", "CELL")(a.childNodes[0]) && (a.type = sre.SemanticAttr.Type.LINE, a.role = c, a.childNodes = a.childNodes[0].childNodes)
};
sre.SemanticTree.assignRoleToRow_ = function (a, b) {
    if (sre.SemanticTree.attrPred_("type", "LINE")(a))a.role = b; else if (sre.SemanticTree.attrPred_("type", "ROW")(a)) {
        a.role = b;
        var c = sre.SemanticTree.attrPred_("type", "CELL");
        a.childNodes.forEach(function (a) {
            c(a) && (a.role = b)
        })
    }
};
sre.SemanticTree.sliceNodes_ = function (a, b, c) {
    c && a.reverse();
    for (var d = [], e = 0, f; f = a[e]; e++) {
        if (b(f))return c ? {head: a.slice(e + 1).reverse(), div: f, tail: d.reverse()} : {
            head: d,
            div: f,
            tail: a.slice(e + 1)
        };
        d.push(f)
    }
    return c ? {head: [], div: null, tail: d.reverse()} : {head: d, div: null, tail: []}
};
sre.SemanticTree.partitionNodes_ = function (a, b) {
    var c = a, d = [], e = [];
    do {
        var f = sre.SemanticTree.sliceNodes_(c, b);
        e.push(f.head);
        d.push(f.div);
        c = f.tail
    } while (f.div);
    d.pop();
    return {rel: d, comp: e}
};
sre.SemanticTree.attrPred_ = function (a, b) {
    return function (c) {
        c = c[a];
        var d;
        a:switch (a) {
            case "role":
                d = sre.SemanticAttr.Role[b];
                break a;
            case "font":
                d = sre.SemanticAttr.Font[b];
                break a;
            default:
                d = sre.SemanticAttr.Type[b]
        }
        return c == d
    }
};
sre.SemanticTree.prototype.processMfenced_ = function (a, b) {
    var c = sre.SemanticTree.getAttribute_(a, "separators", ","), d = sre.SemanticTree.getAttribute_(a, "open", "("), e = sre.SemanticTree.getAttribute_(a, "close", ")");
    if (c && 0 < b.length) {
        var f = sre.MathUtil.nextSeparatorFunction(c), g = [b.shift()];
        b.forEach(goog.bind(function (a) {
            g.push(this.makeContentNode_(f()));
            g.push(a)
        }, this));
        b = g
    }
    if (d && e)return this.makeHorizontalFencedNode_(this.makeContentNode_(d), this.makeContentNode_(e), b);
    d && b.unshift(this.makeContentNode_(d));
    e && b.push(this.makeContentNode_(e));
    return this.processRow_(b)
};
sre.SemanticTree.getAttribute_ = function (a, b, c) {
    if (!a.hasAttribute(b))return c;
    a = a.getAttribute(b);
    return a.match(/^\s*$/) ? null : a
};
sre.SemanticTree.numberRole_ = function (a) {
    if (a.role === sre.SemanticAttr.Role.UNKNOWN) {
        var b = sre.SemanticUtil.splitUnicode(a.textContent).map(sre.SemanticAttr.lookupMeaning);
        b.every(function (a) {
            return a.type == sre.SemanticAttr.Type.NUMBER && a.role == sre.SemanticAttr.Role.INTEGER || a.type == sre.SemanticAttr.Type.PUNCTUATION && a.role == sre.SemanticAttr.Role.COMMA
        }) ? a.role = sre.SemanticAttr.Role.INTEGER : b.every(function (a) {
            return a.type == sre.SemanticAttr.Type.NUMBER && a.role == sre.SemanticAttr.Role.INTEGER || a.type ==
                sre.SemanticAttr.Type.PUNCTUATION
        }) ? a.role = sre.SemanticAttr.Role.FLOAT : a.role = sre.SemanticAttr.Role.OTHERNUMBER
    }
};
sre.SemanticTree.exprFont_ = function (a) {
    if (a.font === sre.SemanticAttr.Font.UNKNOWN) {
        var b = sre.SemanticUtil.splitUnicode(a.textContent).map(sre.SemanticAttr.lookupMeaning).reduce(function (a, b) {
            return a && b.font && b.font != sre.SemanticAttr.Font.UNKNOWN && b.font != a ? a == sre.SemanticAttr.Font.UNKNOWN ? b.font : null : a
        }, sre.SemanticAttr.Font.UNKNOWN);
        b && (a.font = b)
    }
};
sre.SemanticTree.prototype.makeFractionNode_ = function (a, b) {
    var c = this.makeBranchNode_(sre.SemanticAttr.Type.FRACTION, [a, b], []);
    c.role = c.childNodes.every(function (a) {
        return sre.SemanticTree.attrPred_("role", "INTEGER")(a)
    }) ? sre.SemanticAttr.Role.VULGAR : c.childNodes.every(function (a) {
        return sre.SemanticTree.attrPred_("role", "UNIT")(a)
    }) ? sre.SemanticAttr.Role.UNIT : sre.SemanticAttr.Role.DIVISION;
    return c
};
sre.SemanticTree.prototype.processMultiScript_ = function (a) {
    if (!a.length)return this.makeEmptyNode_();
    var b = this.parseMathml_(a.shift());
    if (!a.length)return b;
    for (var c = [], d = [], e = [], f = [], g = !1, h = 0, k = 0, l; l = a[k]; k++)"MPRESCRIPTS" === sre.SemanticUtil.tagName(l) ? (g = !0, h = 0) : (g ? h & 1 ? c.push(l) : d.push(l) : h & 1 ? e.push(l) : f.push(l), h++);
    if (!sre.SemanticUtil.purgeNodes(c).length && !sre.SemanticUtil.purgeNodes(d).length) {
        a = sre.SemanticUtil.purgeNodes(e);
        c = sre.SemanticUtil.purgeNodes(f);
        if (!a.length && !c.length)return b;
        d = c.length ? a.length ? "MSUBSUP" : "MSUB" : "MSUP";
        b = [b];
        c.length && b.push(this.makeScriptNode_(f, sre.SemanticAttr.Role.RIGHTSUB, !0));
        a.length && b.push(this.makeScriptNode_(e, sre.SemanticAttr.Role.RIGHTSUPER, !0));
        return this.makeLimitNode_(d, b)
    }
    e = this.makeBranchNode_(sre.SemanticAttr.Type.TENSOR, [b, this.makeScriptNode_(d, sre.SemanticAttr.Role.LEFTSUB), this.makeScriptNode_(c, sre.SemanticAttr.Role.LEFTSUPER), this.makeScriptNode_(f, sre.SemanticAttr.Role.RIGHTSUB), this.makeScriptNode_(e, sre.SemanticAttr.Role.RIGHTSUPER)],
        []);
    e.role = b.role;
    e.embellished = sre.SemanticTree.isEmbellished_(b);
    return e
};
sre.SemanticTree.prototype.makeScriptNode_ = function (a, b, c) {
    switch (a.length) {
        case 0:
            a = this.makeEmptyNode_();
            break;
        case 1:
            a = this.parseMathml_(a[0]);
            if (c)return a;
            break;
        default:
            a = this.makeDummyNode_(this.parseMathmlChildren_(a))
    }
    a.role = b;
    return a
};
sre.SemanticTree.isEmbellished_ = function (a) {
    return a.embellished ? a.embellished : sre.SemanticAttr.isEmbellishedType(a.type) ? a.type : null
};
sre.SemanticTree.isOperator_ = function (a) {
    return sre.SemanticTree.attrPred_("type", "OPERATOR")(a) || sre.SemanticTree.attrPred_("embellished", "OPERATOR")(a)
};
sre.SemanticTree.isRelation_ = function (a) {
    return sre.SemanticTree.attrPred_("type", "RELATION")(a) || sre.SemanticTree.attrPred_("embellished", "RELATION")(a)
};
sre.SemanticTree.isPunctuation_ = function (a) {
    return sre.SemanticTree.attrPred_("type", "PUNCTUATION")(a) || sre.SemanticTree.attrPred_("embellished", "PUNCTUATION")(a)
};
sre.SemanticTree.isFence_ = function (a) {
    return sre.SemanticTree.attrPred_("type", "FENCE")(a) || sre.SemanticTree.attrPred_("embellished", "FENCE")(a)
};
sre.SemanticTree.getEmbellishedInner_ = function (a) {
    return a && a.embellished && 0 < a.childNodes.length ? sre.SemanticTree.getEmbellishedInner_(a.childNodes[0]) : a
};
sre.SemanticTree.purgeFences_ = function (a) {
    var b = a.rel;
    a = a.comp;
    for (var c = [], d = []; 0 < b.length;) {
        var e = b.shift(), f = a.shift();
        sre.SemanticTree.isElligibleFence_(e) ? (c.push(e), d.push(f)) : (sre.SemanticTree.fenceToPunct_(e), f.push(e), f = f.concat(a.shift()), a.unshift(f))
    }
    d.push(a.shift());
    return {rel: c, comp: d}
};
sre.SemanticTree.isElligibleFence_ = function (a) {
    if (!a || !sre.SemanticTree.isFence_(a))return !1;
    if (!a.embellished)return !0;
    var b = function (a) {
        return a.embellished ? !(!sre.SemanticTree.attrPred_("type", "TENSOR")(a) || sre.SemanticTree.attrPred_("type", "EMPTY")(a.childNodes[1]) && sre.SemanticTree.attrPred_("type", "EMPTY")(a.childNodes[2]) || sre.SemanticTree.attrPred_("type", "EMPTY")(a.childNodes[3]) && sre.SemanticTree.attrPred_("type", "EMPTY")(a.childNodes[4])) || sre.SemanticTree.attrPred_("role", "CLOSE")(a) &&
        sre.SemanticTree.attrPred_("type", "TENSOR")(a) || sre.SemanticTree.attrPred_("role", "OPEN")(a) && (sre.SemanticTree.attrPred_("type", "SUBSCRIPT")(a) || sre.SemanticTree.attrPred_("type", "SUPERSCRIPT")(a)) ? !1 : b(a.childNodes[0]) : !0
    };
    return b(a)
};
sre.SemanticTree.rewriteFencedNode_ = function (a) {
    var b = a.contentNodes[1], c = sre.SemanticTree.rewriteFence_(a, a.contentNodes[0]);
    a.contentNodes[0] = c.fence;
    c = sre.SemanticTree.rewriteFence_(c.node, b);
    a.contentNodes[1] = c.fence;
    return c.node
};
sre.SemanticTree.rewriteFence_ = function (a, b) {
    if (!b.embellished)return {node: a, fence: b};
    var c = b.childNodes[0], d = sre.SemanticTree.rewriteFence_(a, c);
    if (sre.SemanticTree.attrPred_("type", "SUPERSCRIPT")(b) || sre.SemanticTree.attrPred_("type", "SUBSCRIPT")(b) || sre.SemanticTree.attrPred_("type", "TENSOR")(b))return sre.SemanticTree.attrPred_("role", "SUBSUP")(b) || (b.role = a.role), b.replaceChild_(c, d.node), sre.SemanticTree.propagateFencePointer_(b, c), {
        node: b,
        fence: d.fence
    };
    b.replaceChild_(c, d.fence);
    b.mathmlTree &&
    -1 === b.mathml.indexOf(b.mathmlTree) && b.mathml.push(b.mathmlTree);
    return {node: d.node, fence: b}
};
sre.SemanticTree.propagateFencePointer_ = function (a, b) {
    a.fencePointer = b.fencePointer || b.id;
    a.embellished = null
};
sre.SemanticTree.prototype.displayTree = function () {
    this.root.displayTree(0)
};
sre.SemanticTree.Node.prototype.displayTree = function (a) {
    a++;
    var b = Array(a).join("  ");
    console.log(b + this.toString());
    console.log(b + "MathmlTree:");
    console.log(b + this.mathmlTreeString_());
    console.log(b + "MathML:");
    for (var c = 0, d; d = this.mathml[c]; c++)console.log(b + d.toString());
    console.log(b + "Begin Content");
    this.contentNodes.forEach(function (b) {
        b.displayTree(a)
    });
    console.log(b + "End Content");
    console.log(b + "Begin Children");
    this.childNodes.forEach(function (b) {
        b.displayTree(a)
    });
    console.log(b + "End Children")
};
sre.SemanticTree.Node.prototype.mathmlTreeString_ = function () {
    return this.mathmlTree ? this.mathmlTree.toString() : "EMPTY"
};
sre.EnrichMathml = function () {
};
sre.EnrichMathml.SETTINGS = {collapsed: !0, implicit: !0};
sre.EnrichMathml.ATTRIBUTE_PREFIX_ = "data-semantic-";
sre.EnrichMathml.Attribute = {
    ADDED: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "added",
    CHILDREN: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "children",
    COLLAPSED: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "collapsed",
    CONTENT: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "content",
    FONT: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "font",
    ID: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "id",
    OPERATOR: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "operator",
    PARENT: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "parent",
    ROLE: sre.EnrichMathml.ATTRIBUTE_PREFIX_ + "role",
    TYPE: sre.EnrichMathml.ATTRIBUTE_PREFIX_ +
    "type"
};
sre.EnrichMathml.enrich = function (a, b) {
    var c = a.cloneNode(!0);
    sre.EnrichMathml.walkTree(b.root);
    sre.Debugger.getInstance().generateOutput(function () {
        sre.EnrichMathml.formattedOutput(c, a, b, !0)
    });
    return a
};
sre.EnrichMathml.walkTree = function (a) {
    var b = sre.EnrichCaseFactory.getCase(a);
    if (b)return b = b.getMathml(), sre.EnrichMathml.ascendNewNode(b);
    if (1 === a.mathml.length)return sre.Debugger.getInstance().output("Walktree Case 0"), b = a.mathml[0], sre.EnrichMathml.setAttributes(b, a), sre.EnrichMathml.ascendNewNode(b);
    var b = a.contentNodes.map(sre.EnrichMathml.cloneContentNode), c = a.childNodes.map(sre.EnrichMathml.walkTree), c = sre.EnrichMathml.combineContentChildren_(a, b, c), b = a.mathmlTree;
    if (null === b)sre.Debugger.getInstance().output("Walktree Case 1"),
        b = sre.EnrichMathml.introduceNewLayer(c); else {
        var d = sre.EnrichMathml.attachedElement_(c);
        sre.Debugger.getInstance().output("Walktree Case 2");
        d ? (sre.Debugger.getInstance().output("Walktree Case 2.1"), b = d.parentNode) : (sre.Debugger.getInstance().output("Walktree Case 2.2"), b = sre.EnrichMathml.getInnerNode(b))
    }
    b = sre.EnrichMathml.rewriteMfenced(b);
    sre.EnrichMathml.mergeChildren_(b, c);
    sre.EnrichMathml.setAttributes(b, a);
    return sre.EnrichMathml.ascendNewNode(b)
};
sre.EnrichMathml.introduceNewLayer = function (a) {
    var b = sre.EnrichMathml.mathmlLca_(a), c = b.node;
    b.valid && sre.SemanticUtil.hasEmptyTag(c) || (sre.Debugger.getInstance().output("Walktree Case 1.1"), c = sre.SystemExternal.document.createElement("mrow"), a[0] && (sre.Debugger.getInstance().output("Walktree Case 1.1.1"), b = sre.EnrichMathml.attachedElement_(a), a = sre.EnrichMathml.childrenSubset_(b.parentNode, a), sre.DomUtil.replaceNode(b, c), a.forEach(function (a) {
        c.appendChild(a)
    })));
    return c
};
sre.EnrichMathml.childrenSubset_ = function (a, b) {
    var c = sre.DomUtil.toArray(a.childNodes), d = Infinity, e = -Infinity;
    b.forEach(function (a) {
        a = c.indexOf(a);
        -1 !== a && (d = Math.min(d, a), e = Math.max(e, a))
    });
    return c.slice(d, e + 1)
};
sre.EnrichMathml.mergeChildren_ = function (a, b) {
    var c = a.childNodes;
    if (c.length)for (var d = 0; b.length;)c[d] === b[0] || sre.EnrichMathml.functionApplication_(c[d], b[0]) ? (b.shift(), d++) : c[d] && -1 === b.indexOf(c[d]) ? d++ : (a.insertBefore(b[0], c[d]), b.shift()); else b.forEach(function (b) {
        a.appendChild(b)
    })
};
sre.EnrichMathml.functionApplication_ = function (a, b) {
    var c = sre.SemanticAttr.functionApplication();
    return a && b && a.textContent && b.textContent && a.textContent === c && b.textContent === c && "true" === b.getAttribute(sre.EnrichMathml.Attribute.ADDED) ? (sre.DomUtil.replaceNode(a, b), !0) : !1
};
sre.EnrichMathml.mathmlLca_ = function (a) {
    var b = sre.EnrichMathml.attachedElement_(a);
    if (!b)return {valid: !1, node: null};
    a = sre.EnrichMathml.attachedElement_(a.slice().reverse());
    if (b === a)return {valid: !0, node: b};
    var c = sre.EnrichMathml.pathToRoot_(b), b = sre.EnrichMathml.pathToRoot_(a, function (a) {
        return -1 !== c.indexOf(a)
    });
    a = b[0];
    var d = c.indexOf(a);
    return -1 === d ? {valid: !1, node: null} : {valid: sre.EnrichMathml.validLca_(c[d + 1], b[1]), node: a}
};
sre.EnrichMathml.attachedElement_ = function (a) {
    for (var b = 0, c = null; !c && b < a.length;)a[b].parentNode && (c = a[b]), b++;
    return c
};
sre.EnrichMathml.pathToRoot_ = function (a, b) {
    for (var c = b || function (a) {
            return !1
        }, d = [a]; !c(a) && !sre.SemanticUtil.hasMathTag(a) && a.parentNode;)a = sre.EnrichMathml.parentNode_(a), d.unshift(a);
    return d
};
sre.EnrichMathml.validLca_ = function (a, b) {
    return !(!a || !b || a.previousSibling || b.nextSibling)
};
sre.EnrichMathml.ascendNewNode = function (a) {
    for (; !sre.SemanticUtil.hasMathTag(a) && sre.EnrichMathml.unitChild_(a);)a = sre.EnrichMathml.parentNode_(a);
    return a
};
sre.EnrichMathml.unitChild_ = function (a) {
    var b = sre.EnrichMathml.parentNode_(a);
    return b && sre.SemanticUtil.hasEmptyTag(b) ? sre.DomUtil.toArray(b.childNodes).every(function (b) {
        return b === a || sre.SemanticUtil.hasIgnoreTag(b)
    }) : !1
};
sre.EnrichMathml.parentNode_ = function (a) {
    return a.parentNode
};
sre.EnrichMathml.simpleCollapseStructure = function (a) {
    return "number" === typeof a
};
sre.EnrichMathml.interleaveIds = function (a, b) {
    return sre.EnrichMathml.interleaveLists(sre.EnrichMathml.collapsedLeafs(a), sre.EnrichMathml.collapsedLeafs(b))
};
sre.EnrichMathml.collapsedLeafs = function (a) {
    var b = function (a) {
        return sre.EnrichMathml.simpleCollapseStructure(a) ? [a] : a.slice(1)
    };
    return Array.prototype.slice.call(arguments, 0).reduce(function (a, d) {
        return a.concat(b(d))
    }, [])
};
sre.EnrichMathml.addCollapsedAttribute = function (a, b) {
    var c = function (a) {
        return sre.EnrichMathml.simpleCollapseStructure(a) ? a.toString() : "(" + a.map(c).join(" ") + ")"
    };
    a.setAttribute(sre.EnrichMathml.Attribute.COLLAPSED, c(b))
};
sre.EnrichMathml.cloneContentNode = function (a) {
    if (a.mathml.length)return sre.EnrichMathml.walkTree(a);
    var b = sre.EnrichMathml.SETTINGS.implicit ? sre.EnrichMathml.createInvisibleOperator_(a) : sre.SystemExternal.document.createElement("mrow");
    a.mathml = [b];
    return b
};
sre.EnrichMathml.makeIdList = function (a) {
    return a.map(function (a) {
        return a.id
    }).join(",")
};
sre.EnrichMathml.setAttributes = function (a, b) {
    a.setAttribute(sre.EnrichMathml.Attribute.TYPE, b.type);
    a.setAttribute(sre.EnrichMathml.Attribute.ROLE, b.role);
    a.setAttribute(sre.EnrichMathml.Attribute.ID, b.id);
    b.childNodes.length && a.setAttribute(sre.EnrichMathml.Attribute.CHILDREN, sre.EnrichMathml.makeIdList(b.childNodes));
    b.contentNodes.length && a.setAttribute(sre.EnrichMathml.Attribute.CONTENT, sre.EnrichMathml.makeIdList(b.contentNodes));
    b.parent && a.setAttribute(sre.EnrichMathml.Attribute.PARENT, b.parent.id)
};
sre.EnrichMathml.combineContentChildren_ = function (a, b, c) {
    sre.EnrichMathml.setOperatorAttribute_(a, b);
    switch (a.type) {
        case sre.SemanticAttr.Type.RELSEQ:
        case sre.SemanticAttr.Type.INFIXOP:
        case sre.SemanticAttr.Type.MULTIREL:
            return sre.EnrichMathml.interleaveLists(c, b);
        case sre.SemanticAttr.Type.PREFIXOP:
            return b.concat(c);
        case sre.SemanticAttr.Type.POSTFIXOP:
            return c.concat(b);
        case sre.SemanticAttr.Type.FENCED:
            return c.unshift(b[0]), c.push(b[1]), c;
        case sre.SemanticAttr.Type.PUNCTUATED:
            if (a.role === sre.SemanticAttr.Role.TEXT)return sre.EnrichMathml.interleaveLists(c,
                b);
            for (var d = [], e = 0, f = 0, g, h; g = c[e], h = b[f]; e++)g.getAttribute(sre.EnrichMathml.Attribute.ID) == h.getAttribute(sre.EnrichMathml.Attribute.ID) && (f++, d.push(g));
            sre.EnrichMathml.setOperatorAttribute_(a, d);
            return c;
        case sre.SemanticAttr.Type.APPL:
            return [c[0], b[0], c[1]];
        case sre.SemanticAttr.Type.ROOT:
            return [c[1], c[0]];
        default:
            return c
    }
};
sre.EnrichMathml.rewriteMfenced = function (a) {
    if ("MFENCED" !== sre.SemanticUtil.tagName(a))return a;
    for (var b = sre.SystemExternal.document.createElement("mrow"), c = 0, d; d = a.attributes[c]; c++)-1 === ["open", "close", "separators"].indexOf(d.name) && b.setAttribute(d.name, d.value);
    sre.DomUtil.toArray(a.childNodes).forEach(function (a) {
        b.appendChild(a)
    });
    sre.DomUtil.replaceNode(a, b);
    return b
};
sre.EnrichMathml.createInvisibleOperator_ = function (a) {
    var b = sre.SystemExternal.document.createElement("mo"), c = sre.SystemExternal.document.createTextNode(a.textContent);
    b.appendChild(c);
    sre.EnrichMathml.setAttributes(b, a);
    b.setAttribute(sre.EnrichMathml.Attribute.ADDED, "true");
    return b
};
sre.EnrichMathml.setOperatorAttribute_ = function (a, b) {
    var c = a.type + (a.textContent ? "," + a.textContent : "");
    b.forEach(function (a) {
        sre.EnrichMathml.getInnerNode(a).setAttribute(sre.EnrichMathml.Attribute.OPERATOR, c)
    })
};
sre.EnrichMathml.getInnerNode = function (a) {
    var b = sre.DomUtil.toArray(a.childNodes);
    if (!b)return a;
    for (var b = b.filter(function (a) {
        return a.nodeType === sre.DomUtil.NodeType.ELEMENT_NODE && !sre.SemanticUtil.hasIgnoreTag(a)
    }), c = [], d = 0, e; e = b[d]; d++)if (sre.SemanticUtil.hasEmptyTag(e)) {
        var f = sre.EnrichMathml.getInnerNode(e);
        f && f !== e && c.push(f)
    } else c.push(e);
    return 1 === c.length ? c[0] : a
};
sre.EnrichMathml.interleaveLists = function (a, b) {
    for (var c = []; a.length || b.length;)a.length && c.push(a.shift()), b.length && c.push(b.shift());
    return c
};
sre.EnrichMathml.formattedOutput = function (a, b, c, d) {
    d = d || !1;
    sre.EnrichMathml.formattedOutput_(a, "Original MathML", d);
    sre.EnrichMathml.formattedOutput_(c, "Semantic Tree", d);
    sre.EnrichMathml.formattedOutput_(b, "Semantically enriched MathML", d)
};
sre.EnrichMathml.formattedOutput_ = function (a, b, c) {
    a = sre.SemanticTree.formatXml(a.toString());
    c ? console.log(b + ":\n```html\n" + sre.EnrichMathml.removeAttributePrefix(a) + "\n```\n") : console.log(a)
};
sre.EnrichMathml.removeAttributePrefix = function (a) {
    return a.toString().replace(new RegExp(sre.EnrichMathml.ATTRIBUTE_PREFIX_, "g"), "")
};
sre.EnrichMathml.printNodeList__ = function (a, b) {
    console.log(a);
    sre.DomUtil.toArray(b).forEach(function (a) {
        console.log(a.toString())
    });
    console.log("<<<<<<<<<<<<<<<<<")
};
sre.EnrichMathml.testTranslation__ = function (a) {
    sre.Debugger.getInstance().init();
    sre.EnrichMathml.removeAttributePrefix(sre.Semantic.enrichMathml("<math>" + a + "</math>").toString())
};
sre.CaseDoubleScript = function (a) {
    sre.AbstractEnrichCase.call(this, a);
    this.mml = a.mathmlTree
};
goog.inherits(sre.CaseDoubleScript, sre.AbstractEnrichCase);
sre.CaseDoubleScript.test = function (a) {
    if (!a.mathmlTree)return !1;
    var b = sre.SemanticUtil.tagName(a.mathmlTree);
    return "MSUBSUP" === b && a.type === sre.SemanticAttr.Type.SUPERSCRIPT || "MUNDEROVER" === b && a.type === sre.SemanticAttr.Type.OVERSCORE
};
sre.CaseDoubleScript.prototype.getMathml = function () {
    var a = this.semantic.childNodes[1], b = this.semantic.childNodes[0], c = b.childNodes[0], d = b.childNodes[1], e = sre.EnrichMathml.walkTree(a), f = sre.EnrichMathml.walkTree(c), g = sre.EnrichMathml.walkTree(d);
    sre.EnrichMathml.setAttributes(this.mml, this.semantic);
    this.mml.setAttribute(sre.EnrichMathml.Attribute.CHILDREN, sre.EnrichMathml.makeIdList([c, d, a]));
    [f, g, e].forEach(goog.bind(function (a) {
        sre.EnrichMathml.getInnerNode(a).setAttribute(sre.EnrichMathml.Attribute.PARENT,
            this.mml.getAttribute(sre.EnrichMathml.Attribute.ID))
    }, this));
    this.mml.setAttribute(sre.EnrichMathml.Attribute.TYPE, b.role);
    sre.EnrichMathml.addCollapsedAttribute(this.mml, [this.semantic.id, [b.id, c.id, d.id], a.id]);
    return this.mml
};
sre.CaseMultiindex = function (a) {
    sre.AbstractEnrichCase.call(this, a);
    this.mml = a.mathmlTree
};
goog.inherits(sre.CaseMultiindex, sre.AbstractEnrichCase);
sre.CaseMultiindex.prototype.completeMultiscript = function (a, b) {
    var c = sre.DomUtil.toArray(this.mml.childNodes).slice(1), d = 0, e = goog.bind(function (a) {
        for (var b = 0, e; e = a[b]; b++) {
            var k = c[d];
            if (k && e == sre.EnrichMathml.getInnerNode(k).getAttribute(sre.EnrichMathml.Attribute.ID))sre.EnrichMathml.getInnerNode(k).setAttribute(sre.EnrichMathml.Attribute.PARENT, this.semantic.id), d++; else {
                var l = this.semantic.querySelectorAll(function (a) {
                    return a.id === e
                });
                this.mml.insertBefore(sre.CaseMultiindex.createNone_(l[0]),
                    k)
            }
        }
    }, this);
    e(a);
    c[d] && "MPRESCRIPTS" !== sre.SemanticUtil.tagName(c[d]) ? this.mml.insertBefore(c[d], sre.SystemExternal.document.createElement("mprescripts")) : d++;
    e(b)
};
sre.CaseMultiindex.createNone_ = function (a) {
    var b = sre.SystemExternal.document.createElement("none");
    a && sre.EnrichMathml.setAttributes(b, a);
    b.setAttribute(sre.EnrichMathml.Attribute.ADDED, "true");
    return b
};
sre.CaseMultiindex.multiscriptIndex = function (a) {
    if (a.type === sre.SemanticAttr.Type.PUNCTUATED && a.contentNodes[0].role === sre.SemanticAttr.Role.DUMMY) {
        for (var b = a.role, c = a.parent.id, d = [a.id], e = 0, f; f = a.childNodes[e]; e++) {
            var g = sre.EnrichMathml.walkTree(f), g = sre.EnrichMathml.getInnerNode(g);
            g.setAttribute(sre.EnrichMathml.Attribute.PARENT, c);
            g.setAttribute(sre.EnrichMathml.Attribute.ROLE, b);
            d.push(f.id)
        }
        return d
    }
    sre.EnrichMathml.walkTree(a);
    return a.id
};
sre.CaseTensor = function (a) {
    sre.CaseMultiindex.call(this, a)
};
goog.inherits(sre.CaseTensor, sre.CaseMultiindex);
sre.CaseTensor.test = function (a) {
    return a.mathmlTree && a.type === sre.SemanticAttr.Type.TENSOR
};
sre.CaseTensor.prototype.getMathml = function () {
    sre.EnrichMathml.walkTree(this.semantic.childNodes[0]);
    var a = sre.CaseMultiindex.multiscriptIndex(this.semantic.childNodes[1]), b = sre.CaseMultiindex.multiscriptIndex(this.semantic.childNodes[2]), c = sre.CaseMultiindex.multiscriptIndex(this.semantic.childNodes[3]), d = sre.CaseMultiindex.multiscriptIndex(this.semantic.childNodes[4]);
    sre.EnrichMathml.setAttributes(this.mml, this.semantic);
    var e = [this.semantic.id, a, b, c, d];
    e.every(sre.EnrichMathml.simpleCollapseStructure) ||
    sre.EnrichMathml.addCollapsedAttribute(this.mml, e);
    e = sre.EnrichMathml.collapsedLeafs(a, b, c, d);
    e.unshift(this.semantic.childNodes[0].id);
    this.mml.setAttribute(sre.EnrichMathml.Attribute.CHILDREN, e.join(","));
    this.completeMultiscript(sre.EnrichMathml.interleaveIds(c, d), sre.EnrichMathml.interleaveIds(a, b));
    return this.mml
};
sre.CaseLine = function (a) {
    sre.AbstractEnrichCase.call(this, a);
    this.mml = a.mathmlTree
};
goog.inherits(sre.CaseLine, sre.AbstractEnrichCase);
sre.CaseLine.test = function (a) {
    return a.mathmlTree && a.type === sre.SemanticAttr.Type.LINE
};
sre.CaseLine.prototype.getMathml = function () {
    this.semantic.childNodes.length && sre.EnrichMathml.walkTree(this.semantic.childNodes[0]);
    sre.EnrichMathml.setAttributes(this.mml, this.semantic);
    return this.mml
};
sre.CaseMultiscripts = function (a) {
    sre.CaseMultiindex.call(this, a)
};
goog.inherits(sre.CaseMultiscripts, sre.CaseMultiindex);
sre.CaseMultiscripts.test = function (a) {
    return a.mathmlTree ? "MMULTISCRIPTS" === sre.SemanticUtil.tagName(a.mathmlTree) && (a.type === sre.SemanticAttr.Type.SUPERSCRIPT || a.type === sre.SemanticAttr.Type.SUBSCRIPT) : !1
};
sre.CaseMultiscripts.prototype.getMathml = function () {
    sre.EnrichMathml.setAttributes(this.mml, this.semantic);
    if (this.semantic.childNodes[0] && this.semantic.childNodes[0].role === sre.SemanticAttr.Role.SUBSUP) {
        var a = this.semantic.childNodes[0], b = a.childNodes[0], c = sre.CaseMultiindex.multiscriptIndex(this.semantic.childNodes[1]), d = sre.CaseMultiindex.multiscriptIndex(a.childNodes[1]), e = [this.semantic.id, [a.id, b.id, d], c];
        sre.EnrichMathml.addCollapsedAttribute(this.mml, e);
        this.mml.setAttribute(sre.EnrichMathml.Attribute.TYPE,
            a.role);
        this.completeMultiscript(sre.EnrichMathml.interleaveIds(d, c), [])
    } else b = this.semantic.childNodes[0], c = sre.CaseMultiindex.multiscriptIndex(this.semantic.childNodes[1]), sre.EnrichMathml.simpleCollapseStructure(c) || (e = [this.semantic.id, b.id, c], sre.EnrichMathml.addCollapsedAttribute(this.mml, e));
    a = sre.EnrichMathml.collapsedLeafs(d || [], c);
    c = sre.EnrichMathml.walkTree(b);
    sre.EnrichMathml.getInnerNode(c).setAttribute(sre.EnrichMathml.Attribute.PARENT, this.semantic.id);
    a.unshift(b.id);
    this.mml.setAttribute(sre.EnrichMathml.Attribute.CHILDREN,
        a.join(","));
    return this.mml
};
sre.CaseEmbellished = function (a) {
    sre.AbstractEnrichCase.call(this, a);
    this.ofenceMml = this.ofence = this.fencedMml = this.fenced = null;
    this.ofenceMap = {};
    this.cfenceMml = this.cfence = null;
    this.cfenceMap = {};
    this.parentCleanup = []
};
goog.inherits(sre.CaseEmbellished, sre.AbstractEnrichCase);
sre.CaseEmbellished.test = function (a) {
    return a.mathmlTree && null !== a.fencePointer
};
sre.CaseEmbellished.prototype.getMathml = function () {
    this.getFenced_();
    this.fencedMml = sre.EnrichMathml.walkTree(this.fenced);
    this.getFencesMml_();
    return this.rewrite_()
};
sre.CaseEmbellished.prototype.getFenced_ = function () {
    for (var a = this.semantic; a.type !== sre.SemanticAttr.Type.FENCED;)a = a.childNodes[0];
    this.fenced = a.childNodes[0];
    this.ofence = a.contentNodes[0];
    this.cfence = a.contentNodes[1];
    sre.CaseEmbellished.fencedMap_(this.ofence, this.ofenceMap);
    sre.CaseEmbellished.fencedMap_(this.cfence, this.cfenceMap)
};
sre.CaseEmbellished.fencedMap_ = function (a, b) {
    b[a.id] = a.mathmlTree;
    a.embellished && sre.CaseEmbellished.fencedMap_(a.childNodes[0], b)
};
sre.CaseEmbellished.prototype.getFencesMml_ = function () {
    for (var a = this.semantic, b = Object.keys(this.ofenceMap), c = Object.keys(this.cfenceMap); !(this.ofenceMml && this.cfenceMml || a === this.fenced);)-1 === b.indexOf(a.fencePointer) || this.ofenceMml || (this.ofenceMml = a.mathmlTree), -1 === c.indexOf(a.fencePointer) || this.cfenceMml || (this.cfenceMml = a.mathmlTree), a = a.childNodes[0];
    this.ofenceMml || (this.ofenceMml = this.ofence.mathmlTree);
    this.cfenceMml || (this.cfenceMml = this.cfence.mathmlTree);
    this.ofenceMml && (this.ofenceMml =
        sre.EnrichMathml.ascendNewNode(this.ofenceMml));
    this.cfenceMml && (this.cfenceMml = sre.EnrichMathml.ascendNewNode(this.cfenceMml))
};
sre.CaseEmbellished.prototype.rewrite_ = function () {
    var a = this.semantic, b = null, c = this.introduceNewLayer_();
    for (sre.EnrichMathml.setAttributes(c, this.fenced.parent); a.type !== sre.SemanticAttr.Type.FENCED;) {
        var d = a.mathmlTree, e = this.specialCase_(a, d);
        if (e)a = e; else {
            sre.EnrichMathml.setAttributes(d, a);
            for (var e = [], f = 1, g; g = a.childNodes[f]; f++)e.push(sre.EnrichMathml.walkTree(g));
            a = a.childNodes[0]
        }
        e = sre.SystemExternal.document.createElement("dummy");
        f = c.parentNode;
        g = d.childNodes[0];
        sre.DomUtil.replaceNode(d,
            e);
        sre.DomUtil.replaceNode(c, d);
        sre.DomUtil.replaceNode(d.childNodes[0], c);
        sre.DomUtil.replaceNode(e, g);
        d.parentNode = f;
        c = d.childNodes[0];
        b || (b = d)
    }
    sre.EnrichMathml.walkTree(this.ofence);
    sre.EnrichMathml.walkTree(this.cfence);
    this.cleanupParents_();
    return b || c
};
sre.CaseEmbellished.prototype.specialCase_ = function (a, b) {
    var c = sre.SemanticUtil.tagName(b), d = null;
    if ("MSUBSUP" === c)var d = a.childNodes[0], e = sre.CaseDoubleScript; else"MMULTISCRIPTS" !== c || a.type !== sre.SemanticAttr.Type.SUPERSCRIPT && a.type !== sre.SemanticAttr.Type.SUBSCRIPT || (e = sre.CaseMultiscripts, d = a.childNodes[0] && a.childNodes[0].role === sre.SemanticAttr.Role.SUBSUP ? a.childNodes[0] : a);
    if (!d)return null;
    var c = d.childNodes[0], f = sre.CaseEmbellished.makeEmptyNode_(c.id);
    d.childNodes[0] = f;
    b = (new e(a)).getMathml();
    d.childNodes[0] = c;
    this.parentCleanup.push(b);
    return d.childNodes[0]
};
sre.CaseEmbellished.makeEmptyNode_ = function (a) {
    var b = sre.SystemExternal.document.createElement("mrow");
    a = new sre.SemanticTree.Node(a);
    a.type = sre.SemanticAttr.Type.EMPTY;
    a.mathmlTree = b;
    return a
};
sre.CaseEmbellished.prototype.introduceNewLayer_ = function () {
    var a = sre.EnrichMathml.introduceNewLayer([this.ofenceMml, this.fencedMml, this.cfenceMml]);
    if (!a.parentNode) {
        for (var b = sre.SystemExternal.document.createElement("mrow"); 0 < a.childNodes.length;)b.appendChild(a.childNodes[0]);
        a.appendChild(b);
        a = b
    }
    return a
};
sre.CaseEmbellished.prototype.cleanupParents_ = function () {
    this.parentCleanup.forEach(function (a) {
        var b = a.childNodes[1].getAttribute(sre.EnrichMathml.Attribute.PARENT);
        a.childNodes[0].setAttribute(sre.EnrichMathml.Attribute.PARENT, b)
    })
};
sre.CaseTable = function (a) {
    sre.AbstractEnrichCase.call(this, a);
    this.mml = a.mathmlTree
};
goog.inherits(sre.CaseTable, sre.AbstractEnrichCase);
sre.CaseTable.test = function (a) {
    return a.mathmlTree && (a.type === sre.SemanticAttr.Type.MATRIX || a.type === sre.SemanticAttr.Type.VECTOR || a.type === sre.SemanticAttr.Type.CASES)
};
sre.CaseTable.prototype.getMathml = function () {
    var a = sre.EnrichMathml.cloneContentNode(this.semantic.contentNodes[0]), b = this.semantic.contentNodes[1] ? sre.EnrichMathml.cloneContentNode(this.semantic.contentNodes[1]) : null;
    this.semantic.childNodes.map(sre.EnrichMathml.walkTree);
    "MFENCED" === sre.SemanticUtil.tagName(this.mml) ? (this.mml.insertBefore(a, this.mml.childNodes[0]), b && this.mml.appendChild(b), this.mml = sre.EnrichMathml.rewriteMfenced(this.mml)) : (a = [a, this.mml], b && a.push(b), this.mml = sre.EnrichMathml.introduceNewLayer(a));
    sre.EnrichMathml.setAttributes(this.mml, this.semantic);
    return this.mml
};
sre.EnrichCases = function () {
};
sre.EnrichCaseFactory.cases.push({
    test: sre.CaseEmbellished.test,
    constr: sre.CaseEmbellished
}, {test: sre.CaseDoubleScript.test, constr: sre.CaseDoubleScript}, {
    test: sre.CaseTensor.test,
    constr: sre.CaseTensor
}, {test: sre.CaseMultiscripts.test, constr: sre.CaseMultiscripts}, {
    test: sre.CaseLine.test,
    constr: sre.CaseLine
}, {test: sre.CaseTable.test, constr: sre.CaseTable});
sre.Semantic = {};
sre.Semantic.Font = sre.SemanticAttr.Font;
sre.Semantic.Role = sre.SemanticAttr.Role;
sre.Semantic.Type = sre.SemanticAttr.Type;
sre.Semantic.getTree = function (a) {
    return (new sre.SemanticTree(a)).xml()
};
sre.Semantic.getTreeFromString = function (a) {
    a = sre.DomUtil.parseInput(a);
    return new sre.SemanticTree(a)
};
sre.Semantic.enrichMathml = function (a) {
    a = sre.DomUtil.parseInput(a);
    return sre.Semantic.annotateMathml(a)
};
sre.Semantic.annotateMathml = function (a) {
    a = a.cloneNode(!0);
    var b = new sre.SemanticTree(a);
    return sre.EnrichMathml.enrich(a, b)
};
