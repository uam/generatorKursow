var CourseOutputApp = angular.module('CourseOutputApp', ['Controllers', 'Filters', 'Services', 'Directives', 'ui.router', 'FBAngular', 'ui.checkbox', 'youtube-embed', 'angularUtils.directives.dirPagination', 'dndLists']);


var Controllers = angular.module('Controllers', []);
var Filters = angular.module('Filters', []);
var Services = angular.module('Services', ['ngResource']);
var Directives = angular.module('Directives', []);


CourseOutputApp.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    if (COURSE_VIEW_TYPE == 0) {
        $stateProvider
            .state('courseDescription', {
                url: "/",
                templateUrl: STATIC_URL + "partials/courseDescription.html",
                controller: "courseDescriptionController"
            })
            .state('courseContent', {
                url: "/courseContent",
                templateUrl: STATIC_URL + "partials/courseContent.html",
                controller: "courseContentController",
            }).state('moduleContent', {
            url: "/courseContent/moduleContent/:id",
            templateUrl: STATIC_URL + "partials/moduleContent.html",
            controller: "moduleContentController",
            params: {
                id: null
            }
        }).state('materialView', {
            url: "/courseContent/moduleContent/:id1/materialView/:id2",
            templateUrl: STATIC_URL + "partials/materialView.html",
            controller: "materialViewController",
            params: {
                id1: null,
                id2: null
            }
        }).state('errorPage', {
            url: "/404",
            templateUrl: STATIC_URL + "partials/404.html",
            controller: 'errorPageController'
        });
    } else if (COURSE_VIEW_TYPE == 1) {
        $stateProvider
            .state('moduleContent', {
                url: "/",
                templateUrl: STATIC_URL + "partials/moduleContent.html",
                controller: "moduleContentController",
            }).state('materialView', {
            url: "/materialView/:id2",
            templateUrl: STATIC_URL + "partials/materialView.html",
            controller: "materialViewController",
            params: {
                id2: null
            }
        }).state('errorPage', {
            url: "/404",
            templateUrl: STATIC_URL + "partials/404.html",
            controller: 'errorPageController'
        });
    } else if (COURSE_VIEW_TYPE == 2) {
        $stateProvider
            .state('materialView', {
                url: "/",
                templateUrl: STATIC_URL + "partials/materialView.html",
                controller: "materialViewController",
            }).state('errorPage', {
            url: "/404",
            templateUrl: STATIC_URL + "partials/404.html",
            controller: 'errorPageController'
        });
    }


});

