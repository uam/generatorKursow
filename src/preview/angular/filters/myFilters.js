Filters.filter('trustAsResourceUrl', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);

Filters.filter('removeSlash', function () {

    return function (text) {

        var str = text;
        var res = str.replace("\\", " ");

        return res;
    };
});