Directives.directive("typeTheorem", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/type-theorem.html'
    };
});


Directives.directive("typeDefinition", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/type-definition.html'
    };
});


Directives.directive("typeProof", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/type-proof.html'
    };
});


Directives.directive("typeExample", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/type-example.html'
    };
});


/* type of answers */

Directives.directive("testOneChoice", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/test-one-choice.html'
    };
});

Directives.directive("testMultiChoice", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/test-multi-choice.html'
    };
});

Directives.directive("singleInput", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/single-input.html'
    };
});

Directives.directive("typeVideo", function () {

    return {
        templateUrl: STATIC_URL + 'angular/directives/type_materials/type-video.html'
    };
});
