Directives.directive('dynamicDirective', function ($compile) {
    return {
        restrict: 'A',
        replace: false,
        terminal: true,
        priority: 1000,
        link: function (scope, element, attrs) {

            element.attr(scope.$eval(attrs.dynamicDirective), "");

            element.removeAttr("dynamic-directive"); //remove the attribute to avoid indefinite loop
            element.removeAttr("data-dynamic-directive");

            $compile(element)(scope);
        }
    };
});

Directives.directive("picture", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/picture/template.html',
        controller: 'pictureController',

    };


});

Directives.directive("testonechoice", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/testonechoice/template.html',
        controller: 'testonechoiceController',

    };


});

Directives.directive("video", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/video/template.html',
        controller: 'videoController',

    };


});

Directives.directive("text", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/text/template.html',
        controller: 'textController',

    };


});

Directives.directive("testmultichoice", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/testmultichoice/template.html',
        controller: 'testmultichoiceController',

    };


});

Directives.directive("singleinput", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/singleinput/template.html',
        controller: 'singleinputController',

    };


});

Directives.directive("iframe", function () {

    return {
        restrict: 'A',
        scope: {
            data: '=blah'
        },
        templateUrl: STATIC_URL + 'plugins/iframe/template.html',
        controller: 'iframeController',

    };

});
