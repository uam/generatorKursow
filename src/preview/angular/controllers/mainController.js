Controllers.controller('mainController', ['$scope', 'Fullscreen', 'setTitleService',
    function ($scope, Fullscreen, setTitleService) {

        $scope.screen = "Pełen ekran"
        $scope.title = setTitleService;


        $scope.checkboxModel = true;

        $scope.goFullscreen = function () {

            if (Fullscreen.isEnabled()) {
                Fullscreen.cancel();
                $scope.screen = "Pełen ekran"
            } else {
                Fullscreen.all();
                $scope.screen = "Normalny ekran";
            }

        }


    }]);