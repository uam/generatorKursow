Controllers.controller('courseContentController', ['$scope', '$stateParams', 'courseLoaderService', 'setTitleService',
    function ($scope, $stateParams, courseLoaderService, setTitleService) {


        $(".se-pre-con").show();
        $scope.pageSize = 10;

        courseLoaderService.get(function (ans) {

            $scope.modules = ans.modules;
            setTitleService.setTitle("Zawartość kursu: " + ans.title);
            $(".se-pre-con").fadeOut("slow");

        });


    }]);