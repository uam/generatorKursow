Controllers.controller('errorPageController', ['$scope', 'setTitleService', '$state',
    function ($scope, setTitleService, $state) {

        $(".se-pre-con").show();
        setTitleService.setTitle("Strona nie istnieje");


        $scope.goToHome = function () {

            if (COURSE_VIEW_TYPE == 0) {
                $state.go("courseDescription");
            } else if (COURSE_VIEW_TYPE == 1) {
                $state.go("moduleContent");
            } else if (COURSE_VIEW_TYPE == 2) {
                $state.go("materialView");
            }


        }

        $(".se-pre-con").fadeOut("slow");


    }]);