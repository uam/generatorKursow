Controllers.controller('moduleContentController', ['$scope', '$stateParams', 'courseLoaderService', '$state', 'setTitleService',
    function ($scope, $stateParams, courseLoaderService, $state, setTitleService) {


        $(".se-pre-con").show();
        $scope.pageSize = 10;

        $scope.id = $stateParams.id;
        $scope.COURSE_VIEW_TYPE = COURSE_VIEW_TYPE;

        courseLoaderService.get(function (ans) {

            var goTo404 = true;

            if (COURSE_VIEW_TYPE == 1) {

                $scope.materials = ans.materials;
                goTo404 = false;
                setTitleService.setTitle("Moduł: " + ans.title);
                $scope.id = ans.id;

            } else {

                for (var i = 0; i < ans.modules.length; ++i) {
                    if (ans.modules[i].id == $scope.id) {
                        $scope.materials = ans.modules[i].materials;
                        goTo404 = false;
                        setTitleService.setTitle("Moduł: " + ans.modules[i].title);
                        break;
                    }
                }
            }

            if (goTo404) {
                $state.go("errorPage");
            }


            $(".se-pre-con").fadeOut("slow");

        });


    }]);