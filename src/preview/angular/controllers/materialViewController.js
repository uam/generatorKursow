Controllers.controller('materialViewController', ['$scope', '$stateParams', 'courseLoaderService', '$state', 'setTitleService',
    function ($scope, $stateParams, courseLoaderService, $state, setTitleService) {

        $(".se-pre-con").show();

        $scope.COURSE_VIEW_TYPE = COURSE_VIEW_TYPE;

        $scope.id1 = $stateParams.id1;
        $scope.id2 = $stateParams.id2;

        var helper = null;

        courseLoaderService.get(function (ans) {

            $scope.title = ans.title;
            var goTo404 = true;
            var idHelper = null;

            if (COURSE_VIEW_TYPE == 0 || COURSE_VIEW_TYPE == 1) {
                if (COURSE_VIEW_TYPE == 0) {

                    for (var i = 0; i < ans.modules.length; ++i) {
                        if (ans.modules[i].id == $scope.id1) {
                            helper = ans.modules[i].materials;
                            $scope.titleModule = ans.modules[i].title;
                            break;
                        }
                    }
                } else if (COURSE_VIEW_TYPE == 1) {
                    helper = ans.materials;
                    $scope.titleModule = ans.title;

                }


                for (var i = 0; i < helper.length; ++i) {
                    if (helper[i].id == $scope.id2) {
                        $scope.material = helper[i];
                        idHelper = i;
                        goTo404 = false;
                        setTitleService.setTitle($scope.material.title);
                        break;
                    }
                }

                $scope.otherMaterials = helper;

                (idHelper + 1) == helper.length ? $scope.nextMaterialId = helper[0].id : $scope.nextMaterialId = helper[idHelper + 1].id;
                (idHelper - 1) == -1 ? $scope.previousMaterialId = helper[helper.length - 1].id : $scope.previousMaterialId = helper[idHelper - 1].id;

                if ($scope.material.proof) {
                    $scope.PartProof = $scope.material.proof.parts[$scope.material.proof.parts.length - 1].number;
                }


            } else if (COURSE_VIEW_TYPE == 2) {
                $scope.material = ans;
                idHelper = i;
                goTo404 = false;
                setTitleService.setTitle($scope.material.title);
            }


            if (goTo404) {
                $state.go("errorPage");
            }


            $(".se-pre-con").fadeOut("slow");


        });

        // TEST ONE CHOISE //

        $scope.testOneChoiseAnswer = [];
        $scope.testOneChoiseNotice = [];
        $scope.testOneChoiseAddAnswer = function (parentInd, ind, answer) {

            $scope.testOneChoiseAnswer[parentInd][ind] = answer;

        }

        $scope.testOneChoiseCheck = function (parentInd, ind, answers) {

            if (!$scope.testOneChoiseAnswer[parentInd][ind]) {
                $scope.testOneChoiseNotice[parentInd][ind] = "empty";
                return;
            }

            //find good answer
            var goodAnswer = "";
            for (var i = 0; i < answers.length; i++) {
                if (answers[i].correct) {
                    goodAnswer = answers[i].value;
                }
            }

            if ($scope.testOneChoiseAnswer[parentInd][ind] == goodAnswer) {
                $scope.testOneChoiseNotice[parentInd][ind] = "true";
            } else {
                $scope.testOneChoiseNotice[parentInd][ind] = "false";
            }

        }

        // TEST ONE CHOISE //

        // TEST MULTI CHOISE //

        $scope.testMultiChoiseAnswer = [];
        $scope.testMultiChoiseNotice = [];
        $scope.testMultiChoiseAddAnswer = function (parentInd, ind, answer) {


            if (!$scope.testMultiChoiseAnswer[parentInd][ind]) {
                $scope.testMultiChoiseAnswer[parentInd][ind] = [];
            }

            for (var i = 0; i < $scope.testMultiChoiseAnswer[parentInd][ind].length; i++) {
                if (answer == $scope.testMultiChoiseAnswer[parentInd][ind][i]) {
                    $scope.testMultiChoiseAnswer[parentInd][ind].splice(i, 1);
                    return;
                }
            }
            $scope.testMultiChoiseAnswer[parentInd][ind].push(answer);
        }

        $scope.testMultiChoiseCheck = function (parentInd, ind, answers) {

            if (!$scope.testMultiChoiseAnswer[parentInd][ind]) {

                $scope.testMultiChoiseNotice[parentInd][ind] = "empty";
                return;
            }

            //find good answer
            var numberOfGoodAnsers = 0;
            var goodAnswers = [];

            for (var i = 0; i < answers.length; i++) {
                if (answers[i].correct == true) {
                    numberOfGoodAnsers++;
                    goodAnswers.push(answers[i].value);
                }
            }

            if ($scope.testMultiChoiseAnswer[parentInd][ind].length != numberOfGoodAnsers) {
                $scope.testMultiChoiseNotice[parentInd][ind] = "false";
                return;
            }

            for (var i = 0; i < goodAnswers.length; i++) {
                var it = goodAnswers[i];
                var chuck = false;
                for (var q = 0; q < $scope.testMultiChoiseAnswer[parentInd][ind].length; q++) {
                    if (it == $scope.testMultiChoiseAnswer[parentInd][ind][q]) {
                        chuck = true;
                        break;
                    }
                }
                if (!chuck) {
                    $scope.testMultiChoiseNotice[parentInd][ind] = "false";
                    return;
                }
            }

            $scope.testMultiChoiseNotice[parentInd][ind] = "true";

        }

        // TEST MULTI CHOISE //

        // SINGLE INPUT //

        $scope.singleAnswerInputTab = [];
        $scope.answerMarek = [];
        $scope.checkInputSingle = function (index, answer) {

            if (!$scope.singleAnswerInputTab[index] || $scope.singleAnswerInputTab[index] == 'undefined') {
                $scope.answerMarek[index] = 'pusty';
                return;
            }

            if ($scope.singleAnswerInputTab[index] == answer) {
                $scope.answerMarek[index] = 'dobrze';
            } else {
                $scope.answerMarek[index] = 'zle';
            }
        };

        // SINGLE INPUT //

        $scope.playerVars = {
            controls: 0
        };


        // show proof's parts
        $scope.numberPart = 1;
        $scope.showMoreParts = function () {

            return $scope.numberPart++;
        };

        // show/hide Full Proof
        $scope.fullProof = false;
        $scope.showFullProof = function () {

            $scope.fullProof = !$scope.fullProof;
        }


    }]);