Controllers.controller('courseDescriptionController', ['$scope', 'courseLoaderService', '$state', 'setTitleService',
    function ($scope, courseLoaderService, $state, setTitleService) {


        $(".se-pre-con").show();
        courseLoaderService.get(function (ans) {
            $scope.course = ans;

            setTitleService.setTitle($scope.course.title);
            $(".se-pre-con").fadeOut("slow");
        });


    }]);