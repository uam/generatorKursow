Services.factory('courseLoaderService', ['$resource', function ($resource) {


    return $resource('data/course.json', {
        cache: true
    });

}]);
