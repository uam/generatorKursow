Controllers.controller('iframeController', ['$scope', '$element', '$sce', '$compile',
    function ($scope, $element, $sce, $compile) {

        $scope.title = $scope.data.title;

        $(window).on('resize', function () {
            var adress = getLocation($scope.src);
            if (adress.host == "www.geogebra.org") {
                reloadGeogebra();
            }


        });

        var init = function () {
            var adress = getLocation($scope.data.video_url);
            if (adress.host == "www.youtube.com") {


                $scope.src = $sce.trustAsResourceUrl($scope.data.video_url);
                $($element).find(".iframe").attr("allowfullscreen", "allowfullscreen");
            } else if (adress.host == "www.geogebra.org") {


                var pathname = adress.pathname;
                var tab = pathname.split("/");
                var height = tab[8];
                var width = tab[6];

                width = Math.floor((height * 16) / 9);
                tab[6] = width;
                pathname = tab.join("/");


                $scope.src = $sce.trustAsResourceUrl("https://" + adress.host + pathname);

            }

        }

        var getLocation = function (href) {
            var l = document.createElement("a");
            l.href = href;
            return l;
        };


        var reloadGeogebra = function () {

            var geogebraCurrentWidth = $($($element).find(".iframe-parent")[0]).width();
            if (geogebraCurrentWidth != geogebraWidth)
                $($element).find(".iframe").attr("src", $scope.src)
            geogebraWidth = geogebraCurrentWidth;
        }

        var geogebraWidth = $($($element).find(".iframe-parent")[0]).width();

        init();


    }]);