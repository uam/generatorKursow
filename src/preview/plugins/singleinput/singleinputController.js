Controllers.controller('singleinputController', ['$scope', '$element',
    function ($scope, $element) {

        // SINGLE INPUT //

        $scope.singleAnswerInputTab = [];
        $scope.answerMarek = [];
        $scope.checkInputSingle = function (index, answer) {

            if (!$scope.singleAnswerInputTab[index] || $scope.singleAnswerInputTab[index] == 'undefined') {
                $scope.answerMarek[index] = 'pusty';
                return;
            }

            if ($scope.singleAnswerInputTab[index] == answer) {
                $scope.answerMarek[index] = 'dobrze';
            } else {
                $scope.answerMarek[index] = 'zle';
            }
        };

        // SINGLE INPUT //


    }]);