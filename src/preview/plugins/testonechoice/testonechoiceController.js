Controllers.controller('testonechoiceController', ['$scope', '$element',
    function ($scope, $element) {
        //$scope.elem = $scope.data;

        // TEST ONE CHOISE //

        $scope.testOneChoiseAnswer = [];
        $scope.testOneChoiseNotice = [];
        $scope.testOneChoiseAddAnswer = function (parentInd, ind, answer) {

            $scope.testOneChoiseAnswer[parentInd][ind] = answer;

        }

        $scope.testOneChoiseCheck = function (parentInd, ind, answers) {

            if (!$scope.testOneChoiseAnswer[parentInd][ind]) {
                $scope.testOneChoiseNotice[parentInd][ind] = "empty";
                return;
            }

            //find good answer
            var goodAnswer = "";
            for (var i = 0; i < answers.length; i++) {
                if (answers[i].correct) {
                    goodAnswer = answers[i].value;
                }
            }

            if ($scope.testOneChoiseAnswer[parentInd][ind] == goodAnswer) {
                $scope.testOneChoiseNotice[parentInd][ind] = "true";
            } else {
                $scope.testOneChoiseNotice[parentInd][ind] = "false";
            }

        }


    }]);