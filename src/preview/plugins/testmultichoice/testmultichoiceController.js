Controllers.controller('testmultichoiceController', ['$scope', '$element',
    function ($scope, $element) {


        // TEST MULTI CHOISE //

        $scope.testMultiChoiseAnswer = [];
        $scope.testMultiChoiseNotice = [];
        $scope.testMultiChoiseAddAnswer = function (parentInd, ind, answer) {


            if (!$scope.testMultiChoiseAnswer[parentInd][ind]) {
                $scope.testMultiChoiseAnswer[parentInd][ind] = [];
            }

            for (var i = 0; i < $scope.testMultiChoiseAnswer[parentInd][ind].length; i++) {
                if (answer == $scope.testMultiChoiseAnswer[parentInd][ind][i]) {
                    $scope.testMultiChoiseAnswer[parentInd][ind].splice(i, 1);
                    return;
                }
            }
            $scope.testMultiChoiseAnswer[parentInd][ind].push(answer);
        }

        $scope.testMultiChoiseCheck = function (parentInd, ind, answers) {

            if (!$scope.testMultiChoiseAnswer[parentInd][ind]) {

                $scope.testMultiChoiseNotice[parentInd][ind] = "empty";
                return;
            }

            //find good answer
            var numberOfGoodAnsers = 0;
            var goodAnswers = [];

            for (var i = 0; i < answers.length; i++) {
                if (answers[i].correct == true) {
                    numberOfGoodAnsers++;
                    goodAnswers.push(answers[i].value);
                }
            }

            if ($scope.testMultiChoiseAnswer[parentInd][ind].length != numberOfGoodAnsers) {
                $scope.testMultiChoiseNotice[parentInd][ind] = "false";
                return;
            }

            for (var i = 0; i < goodAnswers.length; i++) {
                var it = goodAnswers[i];
                var chuck = false;
                for (var q = 0; q < $scope.testMultiChoiseAnswer[parentInd][ind].length; q++) {
                    if (it == $scope.testMultiChoiseAnswer[parentInd][ind][q]) {
                        chuck = true;
                        break;
                    }
                }
                if (!chuck) {
                    $scope.testMultiChoiseNotice[parentInd][ind] = "false";
                    return;
                }
            }

            $scope.testMultiChoiseNotice[parentInd][ind] = "true";

        }

        // TEST MULTI CHOISE //


    }]);