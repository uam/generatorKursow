Controllers.controller('videoController', ['$scope', '$element',
    function ($scope, $element) {

        $scope.title = $scope.data.title;
        $scope.videoSrcYT = $scope.data.src_youtube;
        $scope.videoSrcFile = $scope.data.src_file;

        if ($scope.videoSrcFile != "") {
            var path = "media/";
            $scope.videoSrcFile = path + $scope.videoSrcFile;
        }


    }]);