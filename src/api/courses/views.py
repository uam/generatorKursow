from rest_framework import viewsets
from rest_framework import permissions

from .serializers import CourseSerializer, ModuleSerializer
from apps.course.models import Course, Module


class CourseViewSet(viewsets.ModelViewSet):

    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ModuleViewSet(viewsets.ModelViewSet):

    queryset = Module.objects.all()
    serializer_class = ModuleSerializer

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
