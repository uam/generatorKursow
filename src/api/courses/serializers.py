# -*- coding: utf-8 -*-
from rest_framework import serializers

from apps.course.models import Course, Module
from apps.common.models import DEFAULT_FIELDS_TITLE
from api.common.serializers import DefaultSerializer


from api.materials.serializers import MaterialSerializer
from apps.material.models import Material


class ModuleSerializer(DefaultSerializer):

    materials = MaterialSerializer(many=True)

    id = serializers.IntegerField(read_only=False, required=False)

    def create(self, validated_data):

        _materials = validated_data.pop('materials')

        material_ids = [material.get('id') for material in _materials]
        material_list = Material.objects.filter(id__in=material_ids)

        module = Module.objects.create(**validated_data)

        module.materials = material_list
        module.save()

        return module

    def update(self, instance, validated_data):

        _materials = validated_data.pop('materials')

        material_ids = [material.get('id') for material in _materials]
        material_list = Material.objects.filter(id__in=material_ids)

        instance.materials = material_list

        super(ModuleSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        model = Module
        fields = DEFAULT_FIELDS_TITLE + ('materials', )


class ModuleDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Module
        fields = DEFAULT_FIELDS_TITLE + ('materials', )


class CourseSerializer(DefaultSerializer):

    modules = ModuleSerializer(many=True)

    def create(self, validated_data):

        _modules = validated_data.pop('modules')

        module_ids = [module.get('id') for module in _modules]
        module_list = Module.objects.filter(id__in=module_ids)

        course = Course.objects.create(**validated_data)

        course.modules = module_list
        course.save()

        return course

    def update(self, instance, validated_data):

        _modules = validated_data.pop('modules')

        module_ids = [module.get('id') for module in _modules]
        module_list = Module.objects.filter(id__in=module_ids)

        instance.modules = module_list

        super(CourseSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        model = Course
        fields = DEFAULT_FIELDS_TITLE + ('modules', )
