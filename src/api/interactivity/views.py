from rest_framework import viewsets

from api.interactivity.serializers import InteractiveTestMultiChoiceElementSerializer, \
    InteractiveTestOneChoiceElementSerializer, InteractiveElementSerializer, InteractiveSingleInputElementSerializer, \
    InteractiveTextFieldElementSerializer, InteractiveVideoElementSerializer, \
    InteractiveImageElementSerializer
from apps.interactivity.models import InteractiveTestMultiChoiceElement, InteractiveTestOneChoiceElement, \
    InteractiveElement, InteractiveSingleInputElement, \
    InteractiveTextFieldElement, InteractiveVideoElement, InteractiveImageElement


class InteractiveElementViewSet(viewsets.ModelViewSet):
    queryset = InteractiveElement.objects.all()
    serializer_class = InteractiveElementSerializer


class InteractiveTestMultiChoiceElementViewSet(viewsets.ModelViewSet):

    queryset = InteractiveTestMultiChoiceElement.objects.all()
    serializer_class = InteractiveTestMultiChoiceElementSerializer


class InteractiveTestOneChoiceElementViewSet(viewsets.ModelViewSet):

    queryset = InteractiveTestOneChoiceElement.objects.all()
    serializer_class = InteractiveTestOneChoiceElementSerializer


class InteractiveSingleInputElementViewSet(viewsets.ModelViewSet):

    queryset = InteractiveSingleInputElement.objects.all()
    serializer_class = InteractiveSingleInputElementSerializer


class InteractiveTextFieldElementViewSet(viewsets.ModelViewSet):
    queryset = InteractiveTextFieldElement.objects.all()
    serializer_class = InteractiveTextFieldElementSerializer


class InteractiveVideoElementViewSet(viewsets.ModelViewSet):
    queryset = InteractiveVideoElement.objects.all()
    serializer_class = InteractiveVideoElementSerializer


class InteractiveImageElementViewSet(viewsets.ModelViewSet):
    queryset = InteractiveImageElement.objects.all()
    serializer_class = InteractiveImageElementSerializer
