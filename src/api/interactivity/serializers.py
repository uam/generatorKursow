# -*- coding: utf-8 -*-
from collections import OrderedDict

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import SkipField, set_value
from rest_framework.settings import api_settings

from api.common.serializers import DEFAULT_INTERACTIVE_ELEMENTS_FIELDS, \
    DefaultInteractiveElementSerializer
from api.theory.serializers import ExerciseSerializer
from apps.theory.models import Exercise


class InteractiveElementSerializer(DefaultInteractiveElementSerializer):

    id = serializers.IntegerField(read_only=False, required=False)

    def create(self, validated_data):
        return super(InteractiveElementSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        return super(InteractiveElementSerializer, self).update(instance, validated_data)

    @staticmethod
    def get_proper_serializer(interactive_type):

        from apps.interactivity.models import InteractiveElement

        return InteractiveElement.get_child_by_name(interactive_type).get_serializer()

    def to_internal_value(self, data):

        interactive_type = data.get('interactive_type')

        if not interactive_type:
            raise ValidationError('interactive_type polem wymaganym')

        serializer = self.get_proper_serializer(data.get('interactive_type'))(data=data)

        if not isinstance(data, dict):
            message = self.error_messages['invalid'].format(
                datatype=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            })

        ret = OrderedDict()
        errors = OrderedDict()
        fields = serializer._writable_fields

        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            except ValidationError as exc:
                errors[field.field_name] = exc.detail
            except SkipField:
                pass
            else:
                set_value(ret, field.source_attrs, validated_value)

        if errors:
            raise ValidationError(errors)

        return ret

    def to_representation(self, obj):
        instance = obj
        serializer = obj.get_serializer()(obj, context=self.context)

        ret = OrderedDict()
        fields = serializer._readable_fields

        for field in fields:
            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            if attribute is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)

        return ret

    class Meta:
        from apps.interactivity.models import InteractiveElement
        model = InteractiveElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS


class InteractiveTestMultiChoiceElementSerializer(InteractiveElementSerializer):

    exercises = ExerciseSerializer(many=True)

    def create(self, validated_data):
        from apps.interactivity.models import InteractiveTestMultiChoiceElement

        _exercises = validated_data.pop('exercises')

        exercises = self.fields['exercises'].create(_exercises)

        interactive_element = InteractiveTestMultiChoiceElement.objects.create(**validated_data)

        interactive_element.exercises = exercises
        interactive_element.save()

        return interactive_element

    def update(self, instance, validated_data):

        _exercises = validated_data.pop('exercises')
        # Delete any exercises not included in the request
        exercises_ids = [element.get('id') for element in _exercises]
        for element in instance.exercises.all():
            if element.id not in exercises_ids:
                element.delete()

        updated_exercises = []
        # Create or update element instances that are in the request
        for item in _exercises:
            exercise_instance = Exercise.objects.filter(id=item.get('id'))

            if len(exercise_instance) == 1:
                updated_exercises.append(ExerciseSerializer().update(exercise_instance.get(), item))
            else:
                updated_exercises.append(ExerciseSerializer().create(item))

        instance.exercises = updated_exercises

        super(InteractiveTestMultiChoiceElementSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        from apps.interactivity.models import InteractiveTestMultiChoiceElement
        model = InteractiveTestMultiChoiceElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('number', 'exercises')


class InteractiveTestOneChoiceElementSerializer(InteractiveElementSerializer):

    exercises = ExerciseSerializer(many=True)

    def create(self, validated_data):
        from apps.interactivity.models import InteractiveTestOneChoiceElement

        _exercises = validated_data.pop('exercises')

        exercises = self.fields['exercises'].create(_exercises)

        interactive_element = InteractiveTestOneChoiceElement.objects.create(**validated_data)

        interactive_element.exercises = exercises
        interactive_element.save()

        return interactive_element

    def update(self, instance, validated_data):

        _exercises = validated_data.pop('exercises')
        # Delete any exercises not included in the request
        exercises_ids = [element.get('id') for element in _exercises]
        for element in instance.exercises.all():
            if element.id not in exercises_ids:
                element.delete()

        updated_exercises = []
        # Create or update element instances that are in the request
        for item in _exercises:
            exercise_instance = Exercise.objects.filter(id=item.get('id'))

            if len(exercise_instance) == 1:
                updated_exercises.append(ExerciseSerializer().update(exercise_instance.get(), item))
            else:
                updated_exercises.append(ExerciseSerializer().create(item))

        instance.exercises = updated_exercises

        super(InteractiveTestOneChoiceElementSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        from apps.interactivity.models import InteractiveTestOneChoiceElement
        model = InteractiveTestOneChoiceElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('number', 'exercises')


class InteractiveSingleInputElementSerializer(InteractiveElementSerializer):

    class Meta:
        from apps.interactivity.models import InteractiveSingleInputElement
        model = InteractiveSingleInputElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('question', 'answer', 'number')


class InteractiveTextFieldElementSerializer(InteractiveElementSerializer):

    class Meta:
        from apps.interactivity.models import InteractiveTextFieldElement
        model = InteractiveTextFieldElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('text_field', 'number', )


class InteractiveVideoElementSerializer(InteractiveElementSerializer):

    class Meta:
        from apps.interactivity.models import InteractiveVideoElement
        model = InteractiveVideoElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('video_url', 'number')


class InteractiveImageElementSerializer(InteractiveElementSerializer):

    class Meta:
        from apps.interactivity.models import InteractiveImageElement
        model = InteractiveImageElement
        fields = DEFAULT_INTERACTIVE_ELEMENTS_FIELDS + ('image', 'number')
