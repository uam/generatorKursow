from rest_framework import viewsets

from apps.material.models import Material, ExampleMaterial, TheoremMaterial, ProofMaterial, \
    DefinitionMaterial
from api.materials.serializers import MaterialSerializer, ExampleMaterialSerializer, \
    TheoremMaterialSerializer, ProofMaterialSerializer, \
    DefinitionMaterialSerializer


class MaterialViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Material.objects.all()
    serializer_class = MaterialSerializer


class ExampleMaterialViewSet(viewsets.ModelViewSet):

    queryset = ExampleMaterial.objects.all()
    serializer_class = ExampleMaterialSerializer


class TheoremMaterialViewSet(viewsets.ModelViewSet):

    queryset = TheoremMaterial.objects.all()
    serializer_class = TheoremMaterialSerializer


class ProofMaterialViewSet(viewsets.ModelViewSet):

    queryset = ProofMaterial.objects.all()
    serializer_class = ProofMaterialSerializer


class DefinitionMaterialViewSet(viewsets.ModelViewSet):

    queryset = DefinitionMaterial.objects.all()
    serializer_class = DefinitionMaterialSerializer


