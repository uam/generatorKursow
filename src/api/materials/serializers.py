# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from rest_framework import serializers

from api.common.serializers import DefaultSerializer
from api.theory.serializers import TheoremSerializer, ProofSerializer, DefinitionSerializer
from apps.common.models import DEFAULT_FIELDS_TITLE
from apps.interactivity.models import InteractiveElement
from api.interactivity.serializers import InteractiveElementSerializer
from apps.theory.models import Theorem, Proof, Definition


class MaterialSerializer(DefaultSerializer):

    material_type = serializers.SerializerMethodField()
    interactiveElements = InteractiveElementSerializer(many=True)

    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        from apps.material.models import Material
        model = Material

    def get_material_type(self, obj):
        return obj.MaterialType.name

    def to_representation(self, obj):
        return obj.get_serializer()(obj, context=self.context).to_representation(obj)


MATERIAL_FIELDS = (
    'video', 'outputPdf', 'interactiveElements', 'material_type',) + DEFAULT_FIELDS_TITLE


class TheoremMaterialSerializer(DefaultSerializer):

    material_type = serializers.SerializerMethodField()
    theorem = TheoremSerializer()
    interactiveElements = InteractiveElementSerializer(many=True)

    id = serializers.IntegerField(read_only=False, required=False)

    @staticmethod
    def get_material_type(obj):
        return obj.MaterialType.name

    def create(self, validated_data):
        from apps.material.models import TheoremMaterial

        _interactiveElements = validated_data.pop('interactiveElements')
        _theorem = validated_data.pop('theorem')

        theorem, _ = Theorem.objects.update_or_create(id=_theorem.get('id'), defaults=_theorem)

        material = TheoremMaterial(theorem=theorem, **validated_data)

        interactiveElements_list = []

        for element in _interactiveElements:
            interactiveElements_list.append(
                InteractiveElementSerializer.get_proper_serializer(element.pop('interactive_type'))().create(element))

        material.save()
        material.interactiveElements = interactiveElements_list

        material.save()

        return material

    def update(self, instance, validated_data):

        _interactiveElements = validated_data.pop('interactiveElements')

        # Delete any elements not included in the request
        elements_ids = [element.get('id') for element in _interactiveElements]
        for element in instance.interactiveElements.all():
            if element.id not in elements_ids:
                element.delete()

        updated_elements = []
        # Create or update element instances that are in the request
        for item in _interactiveElements:

            interactive_instance = InteractiveElement.objects.filter(id=item.get('id'))

            if len(interactive_instance) == 1:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.pop('interactive_type'))().update(interactive_instance.get(), item))

            else:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.pop('interactive_type'))().create(item))

        instance.interactiveElements = updated_elements

        theorem = validated_data.pop('theorem')
        theorem, _ = Theorem.objects.update_or_create(id=theorem.get('id'), defaults=theorem)

        instance.theorem = theorem

        super(TheoremMaterialSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        from apps.material.models import TheoremMaterial
        model = TheoremMaterial
        fields = MATERIAL_FIELDS + ('theorem', )


class ProofMaterialSerializer(DefaultSerializer):

    material_type = serializers.SerializerMethodField()
    proof = ProofSerializer()
    interactiveElements = InteractiveElementSerializer(many=True)

    id = serializers.IntegerField(read_only=False, required=False)

    @staticmethod
    def get_material_type(obj):
        return obj.MaterialType.name

    def create(self, validated_data):
        from apps.material.models import ProofMaterial

        _interactiveElements = validated_data.pop('interactiveElements')
        _proof = validated_data.pop('proof')

        proof, _ = Proof.objects.update_or_create(id=_proof.get('id'), defaults=_proof)

        material = ProofMaterial(proof=proof, **validated_data)

        interactiveElements_list = []

        for element in _interactiveElements:
            interactiveElements_list.append(
                InteractiveElementSerializer.get_proper_serializer(element.get('interactive_type'))().create(element))

        material.save()
        material.interactiveElements = interactiveElements_list

        material.save()

        return material

    def update(self, instance, validated_data):

        _interactiveElements = validated_data.pop('interactiveElements')

        # Delete any elements not included in the request
        elements_ids = [element.get('id') for element in _interactiveElements]
        for element in instance.interactiveElements.all():
            if element.id not in elements_ids:
                element.delete()

        updated_elements = []
        # Create or update element instances that are in the request
        for item in _interactiveElements:

            interactive_instance = InteractiveElement.objects.filter(id=item.get('id'))

            if len(interactive_instance) == 1:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.get('interactive_type'))().update(interactive_instance.get(), item))

            else:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.get('interactive_type'))().create(item))

        instance.interactiveElements = updated_elements

        proof = validated_data.pop('proof')
        proof, _ = Proof.objects.update_or_create(id=proof.get('id'), defaults=proof)

        instance.proof = proof

        super(ProofMaterialSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        from apps.material.models import ProofMaterial
        model = ProofMaterial
        fields = MATERIAL_FIELDS + ('proof', )


class DefinitionMaterialSerializer(DefaultSerializer):

    material_type = serializers.SerializerMethodField()
    definition = DefinitionSerializer()
    interactiveElements = InteractiveElementSerializer(many=True)

    @staticmethod
    def get_material_type(obj):
        return obj.MaterialType.name

    def create(self, validated_data):
        from apps.material.models import DefinitionMaterial

        _interactiveElements = validated_data.pop('interactiveElements')
        _definition = validated_data.pop('definition')

        definition, _ = Definition.objects.update_or_create(id=_definition.get('id'), defaults=_definition)

        material = DefinitionMaterial(definition=definition, **validated_data)

        interactiveElements_list = []

        for element in _interactiveElements:
            interactiveElements_list.append(
                InteractiveElementSerializer.get_proper_serializer(element.get('interactive_type'))().create(element))

        material.save()
        material.interactiveElements = interactiveElements_list

        material.save()

        return material

    def update(self, instance, validated_data):

        _interactiveElements = validated_data.pop('interactiveElements')

        # Delete any elements not included in the request
        elements_ids = [element.get('id') for element in _interactiveElements]
        for element in instance.interactiveElements.all():
            if element.id not in elements_ids:
                element.delete()

        updated_elements = []
        # Create or update element instances that are in the request
        for item in _interactiveElements:

            interactive_instance = InteractiveElement.objects.filter(id=item.get('id'))

            if len(interactive_instance) == 1:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.get('interactive_type'))().update(interactive_instance.get(), item))

            else:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.get('interactive_type'))().create(item))

        instance.interactiveElements = updated_elements

        definition = validated_data.pop('definition')
        definition, _ = Definition.objects.update_or_create(id=definition.get('id'), defaults=definition)

        instance.definition = definition

        super(DefinitionMaterialSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        from apps.material.models import DefinitionMaterial
        model = DefinitionMaterial
        fields = MATERIAL_FIELDS + ('definition', )


class ExampleMaterialSerializer(DefaultSerializer):

    material_type = serializers.SerializerMethodField()
    interactiveElements = InteractiveElementSerializer(many=True)

    @staticmethod
    def get_material_type(obj):
        return obj.MaterialType.name

    def create(self, validated_data):
        from apps.material.models import ExampleMaterial

        _interactiveElements = validated_data.pop('interactiveElements')

        material = ExampleMaterial(**validated_data)

        interactiveElements_list = []

        for element in _interactiveElements:
            interactiveElements_list.append(
                InteractiveElementSerializer.get_proper_serializer(element.get('interactive_type'))().create(element))

        material.save()
        material.interactiveElements = interactiveElements_list

        material.save()

        return material

    def update(self, instance, validated_data):

        _interactiveElements = validated_data.pop('interactiveElements')

        # Delete any elements not included in the request
        elements_ids = [element.get('id') for element in _interactiveElements]
        for element in instance.interactiveElements.all():
            if element.id not in elements_ids:
                element.delete()

        updated_elements = []
        # Create or update element instances that are in the request
        for item in _interactiveElements:

            interactive_instance = InteractiveElement.objects.filter(id=item.get('id'))

            if len(interactive_instance) == 1:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.get('interactive_type'))().update(interactive_instance.get(), item))

            else:
                updated_elements.append(InteractiveElementSerializer.get_proper_serializer(
                    item.get('interactive_type'))().create(item))

        instance.interactiveElements = updated_elements

        super(ExampleMaterialSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        from apps.material.models import ExampleMaterial
        model = ExampleMaterial
        fields = MATERIAL_FIELDS + ('summary', 'entry_text')
