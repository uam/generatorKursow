from django.conf.urls import patterns, include, url
from rest_framework.routers import DefaultRouter

from .courses.views import CourseViewSet, ModuleViewSet
from .materials.views import MaterialViewSet, ExampleMaterialViewSet, \
    DefinitionMaterialViewSet, ProofMaterialViewSet, \
    TheoremMaterialViewSet
from .theory.views import TheoremViewSet, DefinitionViewSet, \
    ExerciseViewSet, ProofViewSet

router = DefaultRouter()
router.register(r'courses', CourseViewSet)
router.register(r'modules', ModuleViewSet)

router.register(r'materials', MaterialViewSet)

router.register(r'exampleMaterials', ExampleMaterialViewSet)
router.register(r'proofMaterials', ProofMaterialViewSet)
router.register(r'definitionMaterials', DefinitionMaterialViewSet)
router.register(r'theoremMaterials', TheoremMaterialViewSet)

router.register(r'proofs', ProofViewSet)
router.register(r'exercises', ExerciseViewSet)
router.register(r'definitions', DefinitionViewSet)
router.register(r'theorems', TheoremViewSet)

urlpatterns = patterns('',

                       url(r'^', include(router.urls)),
                       )
