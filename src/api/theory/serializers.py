from rest_framework import serializers

from apps.theory.models import Theorem, Proof, ProofPart, Exercise, Definition, Answer
from apps.common.models import DEFAULT_FIELDS_TITLE
from api.common.serializers import DefaultSerializer


class TheoremSerializer(DefaultSerializer):

    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = Theorem
        fields = DEFAULT_FIELDS_TITLE + ('text', )


class ProofPartSerializer(DefaultSerializer):
    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = ProofPart
        fields = ('text', 'number', 'id')


class ProofSerializer(serializers.ModelSerializer):

    parts = ProofPartSerializer(many=True)
    id = serializers.IntegerField(read_only=False, required=False)

    def create(self, validated_data):

        parts = validated_data.pop('parts')
        proof, _ = Proof.objects.update_or_create(validated_data.get('id'), defaults=validated_data)
        for item in parts:
            ProofPart.objects.create(proof=proof, **item)
        return proof

    def update(self, instance, validated_data):

        parts = validated_data.pop('parts')

        parts_ids = [part.get('id') for part in parts]
        for part in instance.parts.all():
            if part.id not in parts_ids:
                part.delete()

        updated_parts = []

        for item in parts:
            (part_object, _) = ProofPart.objects.update_or_create(id=item.get('id', None), proof=instance, defaults=item)
            updated_parts.append(part_object)

        instance.parts = updated_parts

        super(ProofSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        model = Proof
        fields = DEFAULT_FIELDS_TITLE + ('text', 'parts')


class AnswerSerializer(DefaultSerializer):

    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = Answer
        fields = ('value', 'number', 'correct', 'id', 'hint_text')


class ExerciseSerializer(DefaultSerializer):

    answers = AnswerSerializer(many=True, read_only=False)
    id = serializers.IntegerField(read_only=False, required=False)

    def create(self, validated_data):

        _answers = validated_data.pop('answers')

        exercise, _ = Exercise.objects.update_or_create(id=validated_data.get('id'), defaults=validated_data)

        for answer in _answers:
            Answer.objects.create(exercise=exercise, **answer)

        return exercise

    def update(self, instance, validated_data):

        _answers = validated_data.pop('answers')

        # Delete any answers not included in the request
        answers_ids = [answer.get('id') for answer in _answers]
        for answer in instance.answers.all():
            if answer.id not in answers_ids:
                answer.delete()

        updated_answers = []

        # Create or update answer instances that are in the request
        for item in _answers:
            (answer_object, _) = Answer.objects.update_or_create(id=item.get('id', None), exercise=instance, defaults=item)
            updated_answers.append(answer_object)

        instance.answers = updated_answers

        super(ExerciseSerializer, self).update(instance, validated_data)

        return instance

    class Meta:
        model = Exercise
        fields = DEFAULT_FIELDS_TITLE + ('text', 'answers', 'id', 'number')


class DefinitionSerializer(DefaultSerializer):

    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = Definition

        fields = DEFAULT_FIELDS_TITLE + ('text', )
