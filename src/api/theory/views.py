from rest_framework import viewsets

from api.permissions import IsOwnerOrReadOnly
from api.theory.serializers import TheoremSerializer, ProofSerializer, \
    ExerciseSerializer, DefinitionSerializer
from apps.theory.models import Theorem, Definition, Exercise, Proof


class ProofViewSet(viewsets.ModelViewSet):

    queryset = Proof.objects.all()
    serializer_class = ProofSerializer
    permission_classes = (IsOwnerOrReadOnly,)


class ExerciseViewSet(viewsets.ModelViewSet):

    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = (IsOwnerOrReadOnly,)


class DefinitionViewSet(viewsets.ModelViewSet):

    queryset = Definition.objects.all()
    serializer_class = DefinitionSerializer
    permission_classes = (IsOwnerOrReadOnly,)


class TheoremViewSet(viewsets.ModelViewSet):

    queryset = Theorem.objects.all()
    serializer_class = TheoremSerializer
    permission_classes = (IsOwnerOrReadOnly,)



