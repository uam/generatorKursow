from django.contrib.auth.models import User

from rest_framework import serializers

from apps.common.models import DefaultFields, DEFAULT_FIELDS

DEFAULT_INTERACTIVE_ELEMENTS_FIELDS = DEFAULT_FIELDS + ('interactive_type', )


class UserSerializer(serializers.ModelSerializer):

    full_name = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        return obj.get_full_name()

    class Meta:
        model = User
        fields = ('username', 'full_name', )


class DefaultSerializer(serializers.ModelSerializer):

    createdBy = UserSerializer(read_only=True)
    modifiedBy = UserSerializer(read_only=True)

    class Meta:
        model = DefaultFields
        fields = DEFAULT_FIELDS


class DefaultInteractiveElementSerializer(DefaultSerializer):

    id = serializers.IntegerField(read_only=False, required=False)
    interactive_type = serializers.CharField(required=True)

    class Meta:
        model = DefaultFields
        fields = DEFAULT_FIELDS + ('interactive_type', )
