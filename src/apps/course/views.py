from apps.common.views import *

from .forms import CourseForm
from .models import Course


class CourseList(BaseListView):
    model = Course


class CourseCreate(BaseCreateView):
    model = Course
    form_class = CourseForm


class CourseDetail(BaseDetailView):
    model = Course
    title = 'Kurs'


class CourseUpdate(BaseUpdateView):
    model = Course
    form_class = CourseForm


class CourseDelete(BaseDeleteView):
    model = Course

