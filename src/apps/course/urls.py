from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns(
    '',

    url(r'^$', CourseList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', CourseDetail.as_view(), name='detail'),
        url(r'^(?P<pk>[0-9]+)/edit/$', CourseUpdate.as_view(), name='edit'),
        url(r'^(?P<pk>[0-9]+)/delete/$', CourseDelete.as_view(), name='delete'),
    url(r'^new/$', CourseCreate.as_view(), name='new'),
)