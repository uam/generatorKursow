# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
        ('module', '0001_initial'),
        ('course', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='modules',
            field=models.ManyToManyField(to='module.Module', verbose_name=b'Modu\xc5\x82y'),
        ),
        migrations.AddField(
            model_name='course',
            name='visibilityType',
            field=models.ForeignKey(default=1, to='common.VisibilityType'),
        ),
    ]
