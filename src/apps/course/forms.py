from apps.common.forms import BaseCrispyForm
from apps.module.models import Module

from .models import Course


class CourseForm(BaseCrispyForm):

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        self.fields['modules'].queryset = Module.visible_manager.visible(self.request_user)

    class Meta:
        model = Course
        exclude = ('visibilityType', 'createdBy', 'modifiedBy', )
