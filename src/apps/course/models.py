# -*- coding: utf-8 -*-
from apps.common.models import DefaultFields
from apps.module.models import Module
from django.core.urlresolvers import reverse
from django.db import models
from django_extensions.db.models import TitleDescriptionModel


class Course(DefaultFields, TitleDescriptionModel):
    modules = models.ManyToManyField(Module, verbose_name='Moduły')

    class Meta:
        db_table = 'course'

    def __unicode__(self):
        return self.title



