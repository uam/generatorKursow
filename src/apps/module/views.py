# coding=utf-8
from apps.common.views import BaseDetailView, BaseCreateView, BaseDeleteView, BaseListView, BaseUpdateView
from .forms import ModuleForm
from .models import Module


class ModuleList(BaseListView):
    model = Module


class ModuleCreate(BaseCreateView):
    model = Module
    form_class = ModuleForm


class ModuleDetail(BaseDetailView):
    model = Module
    title = 'Moduł'


class ModuleUpdate(BaseUpdateView):
    model = Module
    form_class = ModuleForm


class ModuleDelete(BaseDeleteView):
    model = Module

