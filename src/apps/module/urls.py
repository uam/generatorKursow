from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns(
    '',

    url(r'^$', ModuleList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', ModuleDetail.as_view(), name='detail'),
        url(r'^(?P<pk>[0-9]+)/edit/$', ModuleUpdate.as_view(), name='edit'),
        url(r'^(?P<pk>[0-9]+)/delete/$', ModuleDelete.as_view(), name='delete'),
    url(r'^new/$', ModuleCreate.as_view(), name='new'),
)
