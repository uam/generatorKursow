from apps.common.forms import BaseCrispyForm
from apps.material.models import Material

from .models import Module


class ModuleForm(BaseCrispyForm):

    def __init__(self, *args, **kwargs):
        super(ModuleForm, self).__init__(*args, **kwargs)
        self.fields['materials'].queryset = Material.visible_manager.visible(self.request_user)

    class Meta:
        model = Module
        exclude = ('visibilityType', 'createdBy', 'modifiedBy', )
