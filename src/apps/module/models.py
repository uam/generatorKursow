# -*- coding: utf-8 -*-
from apps.common.models import DefaultFields
from apps.material.models import Material
from django.core.urlresolvers import reverse
from django.db import models
from django_extensions.db.models import TitleDescriptionModel


class Module(DefaultFields, TitleDescriptionModel):
    materials = models.ManyToManyField(Material, verbose_name='Materiały')

    class Meta:
        db_table = 'module'

    def __unicode__(self):
        return self.title
