# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('material', '0001_initial'),
        ('common', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('createdBy', models.ForeignKey(related_name='module_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('materials', models.ManyToManyField(to='material.Material', verbose_name=b'Materia\xc5\x82y')),
                ('modifiedBy', models.ForeignKey(related_name='module_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('visibilityType', models.ForeignKey(default=1, to='common.VisibilityType')),
            ],
            options={
                'db_table': 'module',
            },
        ),
    ]
