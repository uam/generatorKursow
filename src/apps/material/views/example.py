from apps.material.forms import ExampleMaterialForm
from apps.material.models import ExampleMaterial
from apps.material.views.material import BaseMaterialCreate, BaseMaterialUpdate


class ExampleMaterialCreate(BaseMaterialCreate):
    model = ExampleMaterial
    form_class = ExampleMaterialForm


class ExampleMaterialUpdate(BaseMaterialUpdate):
    model = ExampleMaterial
    form_class = ExampleMaterialForm
