from apps.material.forms import TheoremMaterialForm
from apps.material.models import TheoremMaterial
from apps.material.views.material import BaseMaterialCreate, BaseMaterialUpdate


class TheoremMaterialCreate(BaseMaterialCreate):
    model = TheoremMaterial
    form_class = TheoremMaterialForm


class TheoremMaterialUpdate(BaseMaterialUpdate):
    model = TheoremMaterial
    form_class = TheoremMaterialForm
