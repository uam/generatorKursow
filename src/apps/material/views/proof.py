from apps.material.forms import ProofMaterialForm
from apps.material.models import ProofMaterial
from apps.material.views.material import BaseMaterialCreate, BaseMaterialUpdate


class ProofMaterialCreate(BaseMaterialCreate):
    model = ProofMaterial
    form_class = ProofMaterialForm


class ProofMaterialUpdate(BaseMaterialUpdate):
    model = ProofMaterial
    form_class = ProofMaterialForm
