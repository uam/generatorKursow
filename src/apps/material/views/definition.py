from apps.material.forms import DefinitionMaterialForm
from apps.material.models import DefinitionMaterial
from apps.material.views.material import BaseMaterialCreate, BaseMaterialUpdate


class DefinitionMaterialCreate(BaseMaterialCreate):
    model = DefinitionMaterial
    form_class = DefinitionMaterialForm


class DefinitionMaterialUpdate(BaseMaterialUpdate):
    model = DefinitionMaterial
    form_class = DefinitionMaterialForm
