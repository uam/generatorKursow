# -*- coding: utf-8 -*-
from apps.common.views import *
from django.core.urlresolvers import reverse_lazy
from django.http import QueryDict

from apps.interactivity.forms import InteractiveElementForm
from apps.interactivity.models import InteractiveElement
from django.shortcuts import redirect
from apps.material.forms import TheoremMaterialForm
from apps.material.models import Material


class MaterialList(BaseListView):
    model = Material
    title = 'Materiały'
    

class MaterialDetail(BaseDetailView):
    model = Material
    template_name = "material/detail.html"
    title = 'Materiał'


class MaterialDelete(BaseDeleteView):
    model = Material
    success_url = reverse_lazy('material:list')


class MaterialPublish(BasePublishView):
    model = Material


class MaterialPrivate(BasePrivateView):
    model = Material


class MaterialCreateChoice(BaseCreateChoice):
    template_name = "material/material_choice.html"


class BaseMaterialCreate(BaseCreateView):
    template_name = "material/material_form.html"
    form_list = []
    page_title = "Nowy materiał"

    def get_context_data(self, **kwargs):
        context = super(BaseMaterialCreate, self).get_context_data(**kwargs)

        form_list = kwargs.get('form_list', [])

        if form_list:
            form_list.sort(key=lambda x: x.form_number)

        context.update({
            'interactive_elements': InteractiveElementForm.get_interactive_elements(),
            'form_list': form_list,
            'page_title': self.page_title
        })

        return context

    def form_invalid(self, form, form_list=None):
        return self.render_to_response(self.get_context_data(form=form, form_list=form_list))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_list = []

        form = self.form_class(user=self.request.user, data=request.POST)

        number_list = []
        for key in request.POST.keys():
            key_list = key.split('_')
            if key_list[0].isdigit():
                number_list.append(key_list[0])

        number_list = set(number_list)

        for number in number_list:
            form_id = request.POST.get(number)
            if form_id:
                filtered_post = {key: item for (key, item) in request.POST.items() if key.split('_')[0] == number}
                query_dict = QueryDict('', mutable=True)
                query_dict.update(filtered_post)
                filtered_files = {key: item for (key, item) in request.FILES.items() if key.split('_')[0] == number}
                query_dict_files = QueryDict('', mutable=True)
                query_dict_files.update(filtered_files)
                inline_form = InteractiveElement.get_child_by_name(form_id).get_form()(user=self.request.user, data=query_dict, files=query_dict_files)
                inline_form.set_number(number)
                form_list.append(inline_form)

        form_list_valid = [inline_form.is_valid() for inline_form in form_list]
        form_list_formsets = [all([inline_formset.is_valid() for inline_formset in inline_form.formsets]) for
                              inline_form in form_list]

        form_list_valid.extend(form_list_formsets)

        if form.is_valid() and all(form_list_valid):
            theorem_material = form.save()
            elements = []
            for inline_form in form_list:
                interactive_element = inline_form.save()
                for inline_formset in inline_form.formsets:
                    inline_formset.save()
                elements.append(interactive_element)
            theorem_material.interactiveElements = elements
            theorem_material.save()
            theorem_material = form.save()

            return redirect(theorem_material.get_absolute_url())

        else:
            return self.form_invalid(form, form_list)


class BaseMaterialUpdate(BaseUpdateView):
    template_name = "material/material_form.html"
    page_title = "Edycja materiału"

    def get_context_data(self, **kwargs):
        context = super(BaseMaterialUpdate, self).get_context_data(**kwargs)

        if kwargs.get('form_list'):
            form_list = kwargs.get('form_list')
        else:
            form_list = self.object.get_interactive_elements_form_list(self.request.user)

        form_list.sort(key=lambda x: x.form_number)

        context.update({
            'interactive_elements': InteractiveElementForm.get_interactive_elements(),
            'form_list': form_list,
            'page_title': self.page_title
        })

        return context

    def form_invalid(self, form, form_list=None):
        return self.render_to_response(self.get_context_data(form=form, form_list=form_list))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        form = self.form_class(user=self.request.user, data=request.POST, instance=self.object)

        interactive_elements_instances = self.object.interactiveElements.all()

        form_list = []

        number_list = [key for key in request.POST.keys() if key.isdigit()]

        for interactive_element in interactive_elements_instances:

            if interactive_element.InteractiveType.ID != request.POST.get(str(interactive_element.number)):
                interactive_element.delete()
            else:
                interactive_form = interactive_element.get_form()(user=self.request.user, data=request.POST,
                                                                  files=request.FILES, instance=interactive_element)
                interactive_form.set_number(interactive_element.number)
                form_list.append(interactive_form)

        existing_objects = [str(inline_form.form_number) for inline_form in form_list]

        new_elements = [number for number in number_list if number not in existing_objects]

        for number in new_elements:
            form_id = request.POST.get(number)
            if form_id:
                filtered_post = {key: item for (key, item) in request.POST.items() if key.split('_')[0] == number}
                query_dict = QueryDict('', mutable=True)
                query_dict.update(filtered_post)
                filtered_files = {key: item for (key, item) in request.FILES.items() if key.split('_')[0] == number}
                query_dict_files = QueryDict('', mutable=True)
                query_dict_files.update(filtered_files)
                inline_form = InteractiveElement.get_child_by_name(form_id).get_form()(user=self.request.user,
                                                                                       data=query_dict,
                                                                                       files=query_dict_files)
                inline_form.set_number(number)
                form_list.append(inline_form)

        form_list_valid = [inline_form.is_valid() for inline_form in form_list]
        form_list_formsets = [all([inline_formset.is_valid() for inline_formset in inline_form.formsets]) for
                              inline_form in form_list]

        form_list_valid.extend(form_list_formsets)

        if form.is_valid() and all(form_list_valid):
            theorem_material = form.save()
            elements = []
            for inline_form in form_list:
                interactive_element = inline_form.save()
                for inline_formset in inline_form.formsets:
                    inline_formset.save()
                elements.append(interactive_element)
            theorem_material.interactiveElements = elements
            theorem_material.save()

            return redirect(theorem_material.get_absolute_url())

        else:
            return self.form_invalid(form, form_list)


