# -*- coding: utf-8 -*-
from apps.common.models import DefaultFields, VisibilityTypeManager
from apps.interactivity.models import InteractiveElement
from apps.theory.models import Theorem, Proof, Definition
from django.core.urlresolvers import reverse, NoReverseMatch
from django.db import models
from django_extensions.db.models import TitleDescriptionModel
from polymorphic import PolymorphicModel


class Material(PolymorphicModel, DefaultFields, TitleDescriptionModel):
    video = models.URLField(null=True, blank=True, verbose_name='Adres url filmu')
    outputPdf = models.TextField(max_length=10000, null=True, blank=True, verbose_name='Tekst w wersji jednolitej')
    interactiveElements = models.ManyToManyField(InteractiveElement, null=True, blank=True, verbose_name='Elementy interaktywne')

    # DO NOT OVERRIDE objects manager to work API
    visible_manager = VisibilityTypeManager()

    def get_absolute_edit_url(self):
        name = self.get_real_instance().MaterialType.name
        try:
            reversed = reverse('material:%s:edit' % name, args=[self.pk])
        except NoReverseMatch:
            reversed = reverse('web:home')
        return reversed

    def _get_class_name_to_url(self):
        return 'material'

    def get_visible_name(self):
        return self.get_real_instance().MaterialType.text

    def get_serializer(self):
        raise AssertionError("This is abstract element")

    def get_interactive_elements_form_list(self, user):
        interactive_elements_instances = self.interactiveElements.all()

        form_list = []

        for interactive_element in interactive_elements_instances:
            interactive_form = interactive_element.get_form()(user=user, instance=interactive_element)
            interactive_form.set_number(interactive_element.number)
            form_list.append(interactive_form)

        return form_list

    class MaterialType:
        name = "material"
        text = u"Materiał"

    def __unicode__(self):
        return self.title


class TheoremMaterial(Material):
    theorem = models.ForeignKey(Theorem, verbose_name='Twierdzenie')

    # @staticmethod
    def get_serializer(self):
        from api.materials.serializers import TheoremMaterialSerializer
        return TheoremMaterialSerializer

    class MaterialType:
        name = "theorem"
        text = "Twierdzenie"


class ProofMaterial(Material):
    proof = models.ForeignKey(Proof, verbose_name='Dowód')

    def get_serializer(self):
        from api.materials.serializers import ProofMaterialSerializer
        return ProofMaterialSerializer

    class MaterialType:
        name = "proof"
        text = "Dowód"


class DefinitionMaterial(Material):
    definition = models.ForeignKey(Definition, verbose_name='Definicja')

    def get_serializer(self):
        from api.materials.serializers import DefinitionMaterialSerializer
        return DefinitionMaterialSerializer

    class MaterialType:
        name = "definition"
        text = "Definicja"


class ExampleMaterial(Material):
    entry_text = models.TextField(verbose_name='Tekst początkowy')
    summary = models.TextField(verbose_name='Podsumowanie')

    def get_serializer(self):
        from api.materials.serializers import ExampleMaterialSerializer
        return ExampleMaterialSerializer

    class MaterialType:
        name = "example"
        text = "Przykład"


