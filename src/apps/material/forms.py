from apps.common.forms import BaseCrispyForm, DEFAULT_EXCLUDE_FIELDS
from apps.material.models import TheoremMaterial, ProofMaterial, DefinitionMaterial, ExampleMaterial
from apps.theory.models import Proof, Definition, Theorem

DEFAULT_MATERIAL_EXCLUDE_FIELDS = DEFAULT_EXCLUDE_FIELDS + ('interactiveElements',)


class TheoremMaterialForm(BaseCrispyForm):

    def __init__(self, *args, **kwargs):
        super(TheoremMaterialForm, self).__init__(*args, **kwargs)
        self.fields['theorem'].queryset = Theorem.visible_manager.visible(self.request_user)

    class Meta:
        model = TheoremMaterial
        exclude = DEFAULT_MATERIAL_EXCLUDE_FIELDS


class ProofMaterialForm(BaseCrispyForm):

    def __init__(self, *args, **kwargs):
        super(ProofMaterialForm, self).__init__(*args, **kwargs)
        self.fields['proof'].queryset = Proof.visible_manager.visible(self.request_user)

    class Meta:
        model = ProofMaterial
        exclude = DEFAULT_MATERIAL_EXCLUDE_FIELDS


class DefinitionMaterialForm(BaseCrispyForm):

    def __init__(self, *args, **kwargs):
        super(DefinitionMaterialForm, self).__init__(*args, **kwargs)
        self.fields['definition'].queryset = Definition.visible_manager.visible(self.request_user)

    class Meta:
        model = DefinitionMaterial
        exclude = DEFAULT_MATERIAL_EXCLUDE_FIELDS


class ExampleMaterialForm(BaseCrispyForm):

    class Meta:
        model = ExampleMaterial
        exclude = DEFAULT_MATERIAL_EXCLUDE_FIELDS
