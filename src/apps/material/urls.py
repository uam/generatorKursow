from django.conf.urls import patterns, include, url
from .views import definition as definition_views
from .views import example as example_views
from .views import proof as proof_views
from .views import theorem as theorem_views
from .views import material as material_views


definition_patterns = patterns(
    '',

    url(r'^(?P<pk>[0-9]+)/edit/$', definition_views.DefinitionMaterialUpdate.as_view(), name='edit'),
    url(r'^new/$', definition_views.DefinitionMaterialCreate.as_view(), name='new'),
)

example_patterns = patterns(
    '',

    url(r'^(?P<pk>[0-9]+)/edit/$', example_views.ExampleMaterialUpdate.as_view(), name='edit'),
    url(r'^new/$', example_views.ExampleMaterialCreate.as_view(), name='new'),
)

proof_patterns = patterns(
    '',

    url(r'^(?P<pk>[0-9]+)/edit/$', proof_views.ProofMaterialUpdate.as_view(), name='edit'),
    url(r'^new/$', proof_views.ProofMaterialCreate.as_view(), name='new'),
)

theorem_patterns = patterns(
    '',

    url(r'^(?P<pk>[0-9]+)/edit/$', theorem_views.TheoremMaterialUpdate.as_view(), name='edit'),
    url(r'^new/$', theorem_views.TheoremMaterialCreate.as_view(), name='new'),
)


urlpatterns = patterns(
    '',

    url(r'^$', material_views.MaterialList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', material_views.MaterialDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/delete/', material_views.MaterialDelete.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/publish/', material_views.MaterialPublish.as_view(), name='publish'),
    url(r'^(?P<pk>[0-9]+)/private/', material_views.MaterialPrivate.as_view(), name='private'),
    url(r'^new/$', material_views.MaterialCreateChoice.as_view(), name='new'),

    url(r'^definition/', include(definition_patterns, namespace='definition')),
    url(r'^example/', include(example_patterns, namespace='example')),
    url(r'^proof/', include(proof_patterns, namespace='proof')),
    url(r'^theorem/', include(theorem_patterns, namespace='theorem')),

)
