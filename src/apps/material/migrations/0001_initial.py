# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('common', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('theory', '0001_initial'),
        ('interactivity', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('video', models.URLField(null=True, verbose_name=b'Adres url filmu', blank=True)),
                ('outputPdf', models.TextField(max_length=10000, null=True, verbose_name=b'Tekst w wersji jednolitej', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DefinitionMaterial',
            fields=[
                ('material_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='material.Material')),
                ('definition', models.ForeignKey(verbose_name=b'Definicja', to='theory.Definition')),
            ],
            options={
                'abstract': False,
            },
            bases=('material.material',),
        ),
        migrations.CreateModel(
            name='ExampleMaterial',
            fields=[
                ('material_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='material.Material')),
                ('entry_text', models.TextField(verbose_name=b'Tekst pocz\xc4\x85tkowy')),
                ('summary', models.TextField(verbose_name=b'Podsumowanie')),
            ],
            options={
                'abstract': False,
            },
            bases=('material.material',),
        ),
        migrations.CreateModel(
            name='ProofMaterial',
            fields=[
                ('material_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='material.Material')),
                ('proof', models.ForeignKey(verbose_name=b'Dow\xc3\xb3d', to='theory.Proof')),
            ],
            options={
                'abstract': False,
            },
            bases=('material.material',),
        ),
        migrations.CreateModel(
            name='TheoremMaterial',
            fields=[
                ('material_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='material.Material')),
                ('theorem', models.ForeignKey(verbose_name=b'Twierdzenie', to='theory.Theorem')),
            ],
            options={
                'abstract': False,
            },
            bases=('material.material',),
        ),
        migrations.AddField(
            model_name='material',
            name='createdBy',
            field=models.ForeignKey(related_name='material_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='interactiveElements',
            field=models.ManyToManyField(to='interactivity.InteractiveElement', null=True, verbose_name=b'Elementy interaktywne', blank=True),
        ),
        migrations.AddField(
            model_name='material',
            name='modifiedBy',
            field=models.ForeignKey(related_name='material_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_material.material_set+', editable=False, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='material',
            name='visibilityType',
            field=models.ForeignKey(default=1, to='common.VisibilityType'),
        ),
    ]
