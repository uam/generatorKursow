from apps.common.forms import BaseCrispyForm, BaseInteractiveRelatedElementForm
from apps.interactivity.forms import DEFAULT_EXCLUDE_FIELDS
from django import forms
from .models import Theorem, Exercise, Answer, Definition, Proof


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        exclude = DEFAULT_EXCLUDE_FIELDS


class DefinitionForm(BaseCrispyForm):
    class Meta:
        model = Definition
        exclude = DEFAULT_EXCLUDE_FIELDS


class ExerciseForm(BaseInteractiveRelatedElementForm):
    class Meta:
        model = Exercise
        fields = ('number', 'title', 'description', 'text')


class ProofForm(BaseCrispyForm):
    class Meta:
        model = Proof
        exclude = DEFAULT_EXCLUDE_FIELDS


class TheoremForm(BaseCrispyForm):
    class Meta:
        model = Theorem
        exclude = DEFAULT_EXCLUDE_FIELDS
