# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('number', models.IntegerField(default=1, verbose_name='Numer odpowiedzi')),
                ('value', models.TextField(verbose_name='Tre\u015b\u0107 odpowiedzi')),
                ('correct', models.BooleanField(default=False, verbose_name='Prawid\u0142owa odpowied\u017a?')),
                ('hint_text', models.CharField(max_length=512, null=True, verbose_name='Tekst podpowiedzi')),
                ('createdBy', models.ForeignKey(related_name='answer_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('number',),
            },
        ),
        migrations.CreateModel(
            name='Definition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('text', models.TextField(verbose_name='Tre\u015b\u0107')),
                ('createdBy', models.ForeignKey(related_name='definition_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('modifiedBy', models.ForeignKey(related_name='definition_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('visibilityType', models.ForeignKey(default=1, to='common.VisibilityType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('text', models.TextField(verbose_name='Tre\u015b\u0107')),
                ('number', models.IntegerField(default=1)),
                ('createdBy', models.ForeignKey(related_name='exercise_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('modifiedBy', models.ForeignKey(related_name='exercise_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('visibilityType', models.ForeignKey(default=1, to='common.VisibilityType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Proof',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('text', models.TextField(verbose_name='Tre\u015b\u0107')),
                ('createdBy', models.ForeignKey(related_name='proof_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('modifiedBy', models.ForeignKey(related_name='proof_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('visibilityType', models.ForeignKey(default=1, to='common.VisibilityType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProofPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(default=1)),
                ('text', models.TextField()),
                ('proof', models.ForeignKey(related_name='parts', to='theory.Proof')),
            ],
        ),
        migrations.CreateModel(
            name='Theorem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('text', models.TextField(verbose_name='Tre\u015b\u0107')),
                ('createdBy', models.ForeignKey(related_name='theorem_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('modifiedBy', models.ForeignKey(related_name='theorem_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('visibilityType', models.ForeignKey(default=1, to='common.VisibilityType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='answer',
            name='exercise',
            field=models.ForeignKey(related_name='answers', to='theory.Exercise'),
        ),
        migrations.AddField(
            model_name='answer',
            name='modifiedBy',
            field=models.ForeignKey(related_name='answer_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
