# -*- coding: utf-8 -*-

from apps.common.models import DefaultFields
from author.decorators import with_author

from django.core.urlresolvers import reverse, NoReverseMatch
from django.db import models
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel


class AbstractTheoryModel(DefaultFields, TitleDescriptionModel):
    text = models.TextField(verbose_name=u'Treść')

    def get_absolute_url(self, operation='detail'):
        name = self._get_class_name_to_url()
        try:
            reversed = reverse('theory:%s:%s' % (name, operation), args=[self.pk])
        except NoReverseMatch:
            reversed = reverse('web:home')
        return reversed

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title


@with_author
class Answer(TimeStampedModel):
    exercise = models.ForeignKey('Exercise', related_name='answers')
    number = models.IntegerField(default=1, verbose_name=u'Numer odpowiedzi')
    value = models.TextField(verbose_name=u'Treść odpowiedzi')
    correct = models.BooleanField(default=False, verbose_name=u'Prawidłowa odpowiedź?')
    hint_text = models.CharField(max_length=512, null=True, verbose_name=u'Tekst podpowiedzi')

    def __str__(self):
        return self.value

    class Meta:
        ordering = ('number',)


class Definition(AbstractTheoryModel):
    pass


class Exercise(AbstractTheoryModel):
    number = models.IntegerField(default=1)


class Proof(AbstractTheoryModel):
    pass


class ProofPart(models.Model):
    proof = models.ForeignKey(Proof, related_name="parts")
    number = models.IntegerField(default=1)
    text = models.TextField()


class Theorem(AbstractTheoryModel):
    pass
