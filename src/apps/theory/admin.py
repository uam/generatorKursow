from django.contrib import admin

from .models import Theorem, Definition, Proof, ProofPart, Exercise, Answer

admin.site.register(Theorem)
admin.site.register(Definition)
admin.site.register(Proof)
admin.site.register(ProofPart)
admin.site.register(Exercise)
admin.site.register(Answer)
