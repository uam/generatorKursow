from django.conf.urls import patterns, include, url
from .views import definition as definition_views
from .views import exercise as exercise_views
from .views import proof as proof_views
from .views import theorem as theorem_views
from .views import theory as theory_views

theorem_patterns = patterns(
    '',

    url(r'^$', theorem_views.TheoremList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', theorem_views.TheoremDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', theorem_views.TheoremUpdate.as_view(), name='edit'),
    url(r'^(?P<pk>[0-9]+)/delete/$', theorem_views.TheoremDelete.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/publish/', theorem_views.TheoremPublish.as_view(), name='publish'),
    url(r'^(?P<pk>[0-9]+)/private/', theorem_views.TheoremPrivate.as_view(), name='private'),
    url(r'^new/$', theorem_views.TheoremCreate.as_view(), name='new'),
)

proof_patterns = patterns(
    '',

    url(r'^$', proof_views.ProofList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', proof_views.ProofDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', proof_views.ProofUpdate.as_view(), name='edit'),
    url(r'^(?P<pk>[0-9]+)/delete/$', proof_views.ProofDelete.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/publish/', proof_views.ProofPublish.as_view(), name='publish'),
    url(r'^(?P<pk>[0-9]+)/private/', proof_views.ProofPrivate.as_view(), name='private'),
    url(r'^new/$', proof_views.ProofCreate.as_view(), name='new'),
)

definition_patterns = patterns(
    '',

    url(r'^$', definition_views.DefinitionList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', definition_views.DefinitionDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', definition_views.DefinitionUpdate.as_view(), name='edit'),
    url(r'^(?P<pk>[0-9]+)/delete/$', definition_views.DefinitionDelete.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/publish/', definition_views.DefinitionPublish.as_view(), name='publish'),
    url(r'^(?P<pk>[0-9]+)/private/', definition_views.DefinitionPrivate.as_view(), name='private'),
    url(r'^new/$', definition_views.DefinitionCreate.as_view(), name='new'),
)

exercise_patterns = patterns(
    '',

    url(r'^$', exercise_views.ExerciseList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', exercise_views.ExerciseDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', exercise_views.ExerciseUpdate.as_view(), name='edit'),
    url(r'^(?P<pk>[0-9]+)/delete/$', exercise_views.ExerciseDelete.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/publish/', exercise_views.ExercisePublish.as_view(), name='publish'),
    url(r'^(?P<pk>[0-9]+)/private/', exercise_views.ExercisePrivate.as_view(), name='private'),
    url(r'^new/$', exercise_views.ExerciseCreate.as_view(), name='new'),
)

urlpatterns = patterns(
    '',
    url(r'^new/$', theory_views.TheoryCreateChoice.as_view(), name='new'),

    url(r'^theorem/', include(theorem_patterns, namespace='theorem')),
    url(r'^proof/', include(proof_patterns, namespace='proof')),
    url(r'^definition/', include(definition_patterns, namespace='definition')),
    url(r'^exercise/', include(exercise_patterns, namespace='exercise')),

)
