from apps.common.views import BaseDetailView, BaseDeleteView, BaseListView, BaseCreateView, BaseUpdateView, \
    BasePublishView, BasePrivateView
from apps.theory.forms import TheoremForm
from apps.theory.models import Theorem


class TheoremModelView(object):
    model = Theorem


class TheoremDetail(TheoremModelView, BaseDetailView):
    template_name = 'theory/theory_detail.html'


class TheoremList(TheoremModelView, BaseListView):
    pass


class TheoremDelete(TheoremModelView, BaseDeleteView):
    pass


class TheoremCreate(TheoremModelView, BaseCreateView):
    form_class = TheoremForm


class TheoremUpdate(TheoremModelView, BaseUpdateView):
    form_class = TheoremForm


class TheoremPublish(TheoremModelView, BasePublishView):
    pass


class TheoremPrivate(TheoremModelView, BasePrivateView):
    pass