from apps.common.views import BaseDetailView, BaseDeleteView, BaseListView, BaseCreateView, BaseUpdateView, \
    BasePublishView, BasePrivateView
from apps.theory.forms import ExerciseForm
from apps.theory.models import Exercise
from django.http import HttpResponseRedirect


class ExerciseModelView(object):
    model = Exercise


class ExerciseDetail(ExerciseModelView, BaseDetailView):
    template_name = 'theory/theory_detail.html'


class ExerciseList(ExerciseModelView, BaseListView):
    pass


class ExerciseDelete(ExerciseModelView, BaseDeleteView):
    pass


class ExerciseCreate(ExerciseModelView, BaseCreateView):
    form_class = ExerciseForm
    template_name = "theory/exercise_form.html"


class ExerciseUpdate(ExerciseModelView, BaseUpdateView):
    form_class = ExerciseForm
    template_name = "theory/exercise_form.html"

    def form_valid(self, form, **kwargs):

        self.object = form.save()

        for formset in form.formsets:
            formset.save()

        return HttpResponseRedirect(self.object.get_absolute_url())


class ExercisePublish(ExerciseModelView, BasePublishView):
    pass


class ExercisePrivate(ExerciseModelView, BasePrivateView):
    pass