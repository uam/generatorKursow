from apps.common.views import BaseDetailView, BaseDeleteView, BaseListView, BaseCreateView, BaseUpdateView, \
    BasePublishView, BasePrivateView
from apps.theory.forms import ProofForm
from apps.theory.models import Proof


class ProofModelView(object):
    model = Proof


class ProofDetail(ProofModelView, BaseDetailView):
    template_name = 'theory/theory_detail.html'


class ProofList(ProofModelView, BaseListView):
    pass


class ProofDelete(ProofModelView, BaseDeleteView):
    pass


class ProofCreate(ProofModelView, BaseCreateView):
    form_class = ProofForm


class ProofUpdate(ProofModelView, BaseUpdateView):
    form_class = ProofForm


class ProofPublish(ProofModelView, BasePublishView):
    pass


class ProofPrivate(ProofModelView, BasePrivateView):
    pass