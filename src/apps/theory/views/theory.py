from django.views.generic import TemplateView


class TheoryCreateChoice(TemplateView):
    template_name = "base/type_choice.html"
