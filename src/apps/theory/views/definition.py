from apps.common.views import BaseDetailView, BaseDeleteView, BaseListView, BaseCreateView, BaseUpdateView, \
    BasePublishView, BasePrivateView
from apps.theory.forms import DefinitionForm
from apps.theory.models import Definition


class DefinitionModelView(object):
    model = Definition
    title = 'Definicja'


class DefinitionDetail(DefinitionModelView, BaseDetailView):
    template_name = 'theory/theory_detail.html'


class DefinitionList(DefinitionModelView, BaseListView):
    pass


class DefinitionDelete(DefinitionModelView, BaseDeleteView):
    pass


class DefinitionCreate(DefinitionModelView, BaseCreateView):
    form_class = DefinitionForm


class DefinitionUpdate(DefinitionModelView, BaseUpdateView):
    form_class = DefinitionForm


class DefinitionPublish(DefinitionModelView, BasePublishView):
    pass


class DefinitionPrivate(DefinitionModelView, BasePrivateView):
    pass