from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from django import forms
from django.core.urlresolvers import reverse
from django.forms import ModelChoiceField, inlineformset_factory

DEFAULT_EXCLUDE_FIELDS = ('visibilityType', 'createdBy', 'modifiedBy',)


class FormRequestUserMixin(object):
    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(user=self.request.user, **self.get_form_kwargs())


class BaseCrispyForm(forms.ModelForm):
    request_user = None

    def __init__(self, user=None, *args, **kwargs):
        self.request_user = user
        super(BaseCrispyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.form_tag = False


class BaseCrispyFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(BaseCrispyFormHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-horizontal'
        self.label_class = 'col-lg-2'
        self.field_class = 'col-lg-8'
        self.form_tag = False


class BaseCrispyRelatedFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(BaseCrispyRelatedFormHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-horizontal'
        self.label_class = 'col-lg-5'
        self.field_class = 'col-lg-7'
        self.form_tag = False


class BaseInteractiveElementForm(BaseCrispyForm):
    formsets = []
    query_dict = None

    def __init__(self, *args, **kwargs):
        super(BaseInteractiveElementForm, self).__init__(*args, **kwargs)

        self.form_id = self.Meta.model.InteractiveType.ID
        self.form_title = self.Meta.model.InteractiveType.text
        self.formsets = []

        if args:
            self.query_dict = args
        elif kwargs.get('data'):
            self.query_dict = (kwargs.get('data'),)

        number = kwargs.get('number')
        if number:
            self.prefix = str(number) + "_" + self.Meta.model.InteractiveType.ID
            self.form_number = number
        else:
            self.prefix = self.Meta.model.InteractiveType.ID

    def set_number(self, number=1):
        self.fields['number'].initial = number
        self.prefix = str(number) + "_" + self.Meta.model.InteractiveType.ID
        self.form_number = number

        self.init_formsets()

    def init_formsets(self):
        model = self.Meta.model

        for related_object in model._meta.get_all_related_objects():
            if self.query_dict:
                self.formsets.append(
                        inlineformset_factory(model, related_object.related_model,
                                              exclude=DEFAULT_EXCLUDE_FIELDS, extra=0)(*self.query_dict,
                                                                                       instance=self.instance,
                                                                                       prefix=self.prefix))
            else:
                self.formsets.append(
                        inlineformset_factory(model, related_object.related_model,
                                              exclude=DEFAULT_EXCLUDE_FIELDS, extra=0)(instance=self.instance,
                                                                                       prefix=self.prefix))

        return self.formsets

    #
    # def _add_buttons_to_layout(self):
    #
    #     my_list = [key for key, item in self.fields.items() if isinstance(item, ModelChoiceField)]
    #
    #     my_dict = {
    #         key: AppendedText(key, '<a href=%s class="fa fa-plus btn-success modal_runner">Dodaj</a>' % reverse(
    #             'interactivity:child-element-new', args=[self.form_id, key]))
    #         for key in my_list
    #         }
    #
    #     return_list = [my_dict.get(x) if x in my_dict.keys() else x for x in self.helper.layout.fields]
    #
    #     self.helper.layout.fields = return_list


class BaseInteractiveRelatedElementForm(BaseCrispyForm):
    formsets = []
    query_dict = None

    def __init__(self, *args, **kwargs):
        super(BaseInteractiveRelatedElementForm, self).__init__(*args, **kwargs)

        self.formsets = []

        if args:
            self.query_dict = args
        elif kwargs.get('data'):
            self.query_dict = (kwargs.get('data'),)

        self.init_formsets()

    def init_formsets(self):
        model = self.Meta.model

        for related_object in model._meta.get_all_related_objects():
            from apps.interactivity.forms import DEFAULT_EXCLUDE_FIELDS
            if self.query_dict:
                self.formsets.append(
                        inlineformset_factory(model, related_object.related_model,
                                              exclude=DEFAULT_EXCLUDE_FIELDS, extra=0)(*self.query_dict,
                                                                                       instance=self.instance,
                                                                                       prefix=self.prefix))
            else:
                self.formsets.append(
                        inlineformset_factory(model, related_object.related_model,
                                              exclude=DEFAULT_EXCLUDE_FIELDS, extra=0)(instance=self.instance,
                                                                                       prefix=self.prefix))

        return self.formsets


    def is_valid(self):
        ret = super(BaseInteractiveRelatedElementForm, self).is_valid()
        if self.formsets:
            return ret and all([formset.is_valid() for formset in self.formsets])

        return ret

