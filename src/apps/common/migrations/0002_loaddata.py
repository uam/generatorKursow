# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management import call_command
from django.db import migrations, models


def load_data(apps, schema_editor):
    call_command('loaddata', 'visibilityType')


def reverse_initial_data(apps, schema_editor):
    VisibilityType = apps.get_model("common", "VisibilityType")
    VisibilityType.objects.all().delete()


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_data, reverse_code=reverse_initial_data),
    ]