from django.contrib import admin

from .models import VisibilityType

admin.site.register(VisibilityType)
