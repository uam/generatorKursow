from author.decorators import with_author
from django.core.urlresolvers import reverse, NoReverseMatch
from django.db import models
from django_extensions.db.models import TimeStampedModel


class VisibilityType(models.Model):
    text = models.CharField(max_length=100)

    def __str__(self):
        return self.text


class VisibilityTypeManager(models.Manager):
    def get_queryset(self):
        return super(VisibilityTypeManager, self).get_queryset()

    def my(self, user):
        return self.get_queryset().filter(createdBy=user)

    def visible(self, user):
        return self.get_queryset().filter(visibilityType=2) | self.get_queryset().filter(createdBy=user)

    def public(self, user):
        return self.get_queryset().filter(visibilityType=2).exclude(createdBy=user)


@with_author
class DefaultFields(TimeStampedModel):
    visibilityType = models.ForeignKey(VisibilityType, default=1)

    objects = models.Manager()
    visible_manager = VisibilityTypeManager()

    def get_absolute_url(self, operation='detail'):
        name = self._get_class_name_to_url()
        try:
            reversed = reverse('%s:%s' % (name, operation), args=[self.pk])
        except NoReverseMatch:
            reversed = reverse('web:home')
        return reversed

    def get_absolute_edit_url(self):
        return self.get_absolute_url('edit')

    def get_absolute_delete_url(self):
        return self.get_absolute_url('delete')

    def get_absolute_publish_url(self):
        return self.get_absolute_url('publish')

    def get_absolute_private_url(self):
        return self.get_absolute_url('private')

    def get_absolute_preview_url(self):
        name = self._get_class_name_to_url()
        try:
            reversed = reverse('web:preview', args=[name, self.pk])
        except NoReverseMatch:
            reversed = reverse('web:home')
        return reversed

    class Meta:
        abstract = True

    def __unicode__(self):
        return "Obiekt %s" % (self.visibilityType.text)

    def _get_class_name_to_url(self):
        return self.__class__.__name__.lower()


TIMESTAMP_FIELDS = ('created', 'modified', )
AUTHOR_FIELDS = ('createdBy', )

DEFAULT_FIELDS = AUTHOR_FIELDS + ('visibilityType', 'id')

DEFAULT_FIELDS_WITH_TIMESTAMP = TIMESTAMP_FIELDS + \
    AUTHOR_FIELDS + ('visibilityType', 'id', )


DEFAULT_FIELDS_TITLE = AUTHOR_FIELDS + ('visibilityType', 'id', 'title', 'description')
