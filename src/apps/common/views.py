# coding=utf-8
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView, DeleteView
from django.views.generic.list import ListView
from django_modalview.generic.base import ModalTemplateView
from django_modalview.generic.component import ModalButton
from django_modalview.generic.edit import ModalDeleteView, ModalUpdateView
from django_modalview.generic.response import ModalJsonResponseRedirect, ModalJsonResponse

from apps.common.forms import FormRequestUserMixin


class VisibleManagerMixin(object):
    title = ''

    def get_context_data(self, *args, **kwargs):
        context = super(VisibleManagerMixin, self).get_context_data(*args, **kwargs)

        context.update({
            'public_objects': self._get_public_objects(),
            'my_objects': self._get_my_objects(),
            'title': self.title
        })

        return context

    def get_queryset(self):
        return self.model.visible_manager.visible(self.request.user)

    def _get_public_objects(self):
        return self.model.visible_manager.public(self.request.user)

    def _get_my_objects(self):
        return self.model.visible_manager.my(self.request.user)


class BaseCreateChoice(TemplateView):
    template_name = "base/type_choice.html"


class BaseDetailView(VisibleManagerMixin, DetailView):
    template_name = "base/object_detail.html"


class BaseListView(VisibleManagerMixin, ListView):
    pass


class BaseDeleteView(VisibleManagerMixin, ModalDeleteView, DeleteView):
    def __init__(self, *args, **kwargs):
        super(BaseDeleteView, self).__init__(*args, **kwargs)
        self.title = "Usuwanie obiektu"
        self.description = "Jesteś pewny?"
        self.close_button.value = 'Zamknij'
        self.submit_button.value = 'Usuń'

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(BaseDeleteView, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        reversed = reverse('material:list')
        self.response = ModalJsonResponseRedirect(reversed)
        super(BaseDeleteView, self).delete(request, *args, **kwargs)


class BaseCreateView(VisibleManagerMixin, FormRequestUserMixin, CreateView):
    template_name = "base/object_form.html"


class BaseUpdateView(VisibleManagerMixin, FormRequestUserMixin, UpdateView):
    template_name = "base/object_form.html"


class BasePublishView(VisibleManagerMixin, ModalUpdateView, UpdateView):
    fields = []

    def __init__(self, *args, **kwargs):
        super(BasePublishView, self).__init__(*args, **kwargs)
        self.title = "Publikowanie materiału"
        self.description = "Jesteś pewny?"
        self.close_button.value = 'Nie'
        self.submit_button.value = 'Tak!'
        self.submit_button.type = 'success'

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(BasePublishView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form, commit=True, **kwargs):
        self.object.visibilityType_id = 2
        self.object.save(update_fields=['visibilityType'])

        self.response = ModalJsonResponse('{"status": "OK"}')


class BasePrivateView(BasePublishView):
    def __init__(self, *args, **kwargs):
        super(BasePrivateView, self).__init__(*args, **kwargs)
        self.title = "Uprywatnienie materiału"

    def form_valid(self, form, commit=True, **kwargs):
        self.object.visibilityType_id = 1
        self.object.save(update_fields=['visibilityType'])

        self.response = ModalJsonResponse('{"status": "OK"}')
