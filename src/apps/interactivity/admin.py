from django.contrib import admin

from .models import InteractiveTestMultiChoiceElement, \
    InteractiveSingleInputElement, InteractiveElement, InteractiveTestOneChoiceElement, \
    InteractiveTextFieldElement, InteractiveVideoElement, InteractiveImageElement

admin.site.register(InteractiveElement)
admin.site.register(InteractiveTestOneChoiceElement)
admin.site.register(InteractiveTestMultiChoiceElement)
admin.site.register(InteractiveSingleInputElement)
admin.site.register(InteractiveTextFieldElement)
admin.site.register(InteractiveVideoElement)
admin.site.register(InteractiveImageElement)
