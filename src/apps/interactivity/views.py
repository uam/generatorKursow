from apps.common.forms import BaseCrispyFormHelper
from apps.interactivity.models import InteractiveElement
from django.views.generic import FormView
from django_modalview.generic.component import ModalResponse
from django_modalview.generic.edit import ModalFormView
from django_modalview.generic.response import ModalJsonResponse


class InteractiveElementView(FormView):
    template_name = "interactivity/form_formset.html"

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        form = form_class(user=self.request.user, **self.get_form_kwargs())
        form.set_number(self.number)

        return form

    def dispatch(self, request, *args, **kwargs):
        child = InteractiveElement.get_child_by_name(kwargs.get('type'))
        self.number = kwargs.get('number')

        self.model = child
        self.form_class = child.get_form()

        return super(InteractiveElementView, self).dispatch(request, *args, **kwargs)


class AddNewInteractiveChildElement(FormView, ModalFormView):
    template_name = "base/modal_form.html"

    def get_context_data(self, **kwargs):
        context = super(AddNewInteractiveChildElement, self).get_context_data(**kwargs)

        context.update({
            'crispy_helper': BaseCrispyFormHelper()
        })

        return context

    def __init__(self, *args, **kwargs):
        super(AddNewInteractiveChildElement, self).__init__(*args, **kwargs)
        self.title = "Test"
        self.form_content_template_name = "base/modal_form.html"

    def form_valid(self, form, **kwargs):

        self.object = form.save()

        for formset in form.formsets:
            formset.save()

        self.response = ModalResponse('Form valid', 'success')

        return ModalJsonResponse({'status': 'ok'})

    def form_invalid(self, form, **kwargs):
        self.response = ModalResponse('Form invalid', 'error')
        return super(AddNewInteractiveChildElement, self).form_invalid(form, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid() and all([inline_form.is_valid() for inline_form in form.formsets]):
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def dispatch(self, request, *args, **kwargs):

        child = InteractiveElement.get_child_by_name(kwargs.get('type'))

        form_dict = child.get_related_objects_form_dict()
        form = form_dict.get(kwargs.get('model'))

        self.model = form.Meta.model
        self.form_class = form

        return super(AddNewInteractiveChildElement, self).dispatch(request, *args, **kwargs)
