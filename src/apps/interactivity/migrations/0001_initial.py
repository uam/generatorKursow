# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('common', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('theory', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='InteractiveElement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('number', models.IntegerField(default=1, verbose_name='Numer elementu')),
            ],
            options={
                'ordering': ['number'],
            },
        ),
        migrations.CreateModel(
            name='InteractiveCanvasElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('script', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.CreateModel(
            name='InteractiveImageElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('image', models.ImageField(upload_to=b'images/', verbose_name='Obraz')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.CreateModel(
            name='InteractiveSingleInputElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('question', models.TextField(max_length=512, verbose_name='Pytanie')),
                ('answer', models.TextField(max_length=512, verbose_name='Odpowied\u017a')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.CreateModel(
            name='InteractiveTestMultiChoiceElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('exercises', models.ManyToManyField(to='theory.Exercise', verbose_name='Zadania')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.CreateModel(
            name='InteractiveTestOneChoiceElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('exercises', models.ManyToManyField(to='theory.Exercise', verbose_name='Zadania')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.CreateModel(
            name='InteractiveTextFieldElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('text_field', models.TextField(verbose_name='Tekst')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.CreateModel(
            name='InteractiveVideoElement',
            fields=[
                ('interactiveelement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='interactivity.InteractiveElement')),
                ('video_url', models.URLField(max_length=512, verbose_name='Adres url')),
            ],
            options={
                'abstract': False,
            },
            bases=('interactivity.interactiveelement',),
        ),
        migrations.AddField(
            model_name='interactiveelement',
            name='createdBy',
            field=models.ForeignKey(related_name='interactiveelement_related_author', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='interactiveelement',
            name='modifiedBy',
            field=models.ForeignKey(related_name='interactiveelement_related_modifier', verbose_name='last updated by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='interactiveelement',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_interactivity.interactiveelement_set+', editable=False, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='interactiveelement',
            name='visibilityType',
            field=models.ForeignKey(default=1, to='common.VisibilityType'),
        ),
    ]
