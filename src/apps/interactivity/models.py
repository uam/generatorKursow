# -*- coding: utf-8 -*-
from abc import abstractmethod

from django.db import models
from polymorphic import PolymorphicModel

from apps.common.models import DefaultFields
from apps.theory.models import Exercise


class InteractiveElement(PolymorphicModel, DefaultFields):
    number = models.IntegerField(default=1, verbose_name=u'Numer elementu')

    @property
    def interactive_type(self):
        return self.InteractiveType.ID

    @interactive_type.setter
    def interactive_type(self, value):
        pass

    @staticmethod
    def get_children():
        related_objects = InteractiveElement._meta.get_all_related_objects()
        return [x.related_model for x in related_objects if x.model == InteractiveElement]

    @abstractmethod
    def get_serializer(self):
        raise Exception("You should never reach this. This is an abstract element")

    @staticmethod
    @abstractmethod
    def get_form():
        raise Exception("You should never reach this. This is an abstract element")

    @staticmethod
    @abstractmethod
    def get_related_objects_form_dict():
        raise Exception("You should never reach this. This is an abstract element")

    @staticmethod
    def get_child_by_name(name):
        children = InteractiveElement.get_children()
        for child in children:
            if child.InteractiveType.ID == name:
                return child
        raise Exception('Child element not found. Check if your class has class InteractiveType:')

    def __unicode__(self):
        return self.InteractiveType.text

    class InteractiveType:
        ID = 'abstract-element'
        text = 'Abstrakcyjny element'

    class Meta:
        ordering = ['number']


class InteractiveTestMultiChoiceElement(InteractiveElement):

    exercises = models.ManyToManyField(Exercise, verbose_name=u"Zadania")

    @staticmethod
    def get_related_objects_form_dict():
        from apps.theory.forms import ExerciseForm
        return {'exercises': ExerciseForm}

    @staticmethod
    def get_form():
        from apps.interactivity.forms import InteractiveTestMultiChoiceElementForm
        return InteractiveTestMultiChoiceElementForm

    @staticmethod
    def get_serializer():
        from api.interactivity.serializers import InteractiveTestMultiChoiceElementSerializer
        return InteractiveTestMultiChoiceElementSerializer

    class InteractiveType:
        ID = 'testmultichoice'
        text = 'Test wielokrotnego wyboru'


class InteractiveTestOneChoiceElement(InteractiveElement):
    exercises = models.ManyToManyField(Exercise, verbose_name=u"Zadania")

    @staticmethod
    def get_related_objects_form_dict():
        from apps.theory.forms import ExerciseForm
        return {'exercises': ExerciseForm}

    @staticmethod
    def get_form():
        from apps.interactivity.forms import InteractiveTestOneChoiceElementForm
        return InteractiveTestOneChoiceElementForm

    @staticmethod
    def get_serializer():
        from api.interactivity.serializers import InteractiveTestOneChoiceElementSerializer
        return InteractiveTestOneChoiceElementSerializer

    class InteractiveType:
        ID = 'testonechoice'
        text = 'Test jednokrotnego wyboru'


class InteractiveSingleInputElement(InteractiveElement):
    question = models.TextField(max_length=512, verbose_name=u"Pytanie")
    answer = models.TextField(max_length=512, verbose_name=u"Odpowiedź")

    @staticmethod
    def get_form():
        from apps.interactivity.forms import InteractiveSingleInputElementForm
        return InteractiveSingleInputElementForm

    @staticmethod
    def get_serializer():
        from api.interactivity.serializers import InteractiveSingleInputElementSerializer
        return InteractiveSingleInputElementSerializer

    class InteractiveType:
        ID = 'singleinput'
        text = 'Odpowiedź podawana przez użytkownika'


class InteractiveTextFieldElement(InteractiveElement):
    text_field = models.TextField(verbose_name=u"Tekst")

    @staticmethod
    def get_form():
        from apps.interactivity.forms import InteractiveTextFieldElementForm
        return InteractiveTextFieldElementForm

    @staticmethod
    def get_serializer():
        from api.interactivity.serializers import InteractiveTextFieldElementSerializer
        return InteractiveTextFieldElementSerializer

    class InteractiveType:
        ID = 'text'
        text = 'Tekst'


class InteractiveVideoElement(InteractiveElement):
    video_url = models.URLField(max_length=512, verbose_name=u"Adres url")

    @staticmethod
    def get_form():
        from apps.interactivity.forms import InteractiveVideoElementForm
        return InteractiveVideoElementForm

    @staticmethod
    def get_serializer():
        from api.interactivity.serializers import InteractiveVideoElementSerializer
        return InteractiveVideoElementSerializer

    class InteractiveType:
        ID = 'iframe'
        text = 'Adres filmu video'


class InteractiveImageElement(InteractiveElement):
    image = models.ImageField(upload_to='images/', verbose_name=u"Obraz")

    @staticmethod
    def get_form():
        from apps.interactivity.forms import InteractiveImageElementForm
        return InteractiveImageElementForm

    @staticmethod
    def get_serializer():
        from api.interactivity.serializers import InteractiveImageElementSerializer
        return InteractiveImageElementSerializer

    class InteractiveType:
        ID = 'picture'
        text = 'Dodaj obrazek'
