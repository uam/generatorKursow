# -*- coding: utf-8 -*-
from django import forms
from django.core.urlresolvers import reverse_lazy

from apps.common.forms import BaseInteractiveElementForm, DEFAULT_EXCLUDE_FIELDS
from apps.theory.models import Exercise
from .models import InteractiveTestMultiChoiceElement, InteractiveSingleInputElement, InteractiveElement, \
    InteractiveTestOneChoiceElement, InteractiveTextFieldElement, \
    InteractiveVideoElement, InteractiveImageElement


class InteractiveTestMultiChoiceElementForm(BaseInteractiveElementForm):

    def __init__(self, *args, **kwargs):
        super(InteractiveTestMultiChoiceElementForm, self).__init__(*args, **kwargs)
        self.fields['exercises'].queryset = Exercise.visible_manager.visible(self.request_user)

    class Meta:
        model = InteractiveTestMultiChoiceElement
        exclude = DEFAULT_EXCLUDE_FIELDS


class InteractiveTestOneChoiceElementForm(BaseInteractiveElementForm):

    def __init__(self, *args, **kwargs):
        super(InteractiveTestOneChoiceElementForm, self).__init__(*args, **kwargs)
        self.fields['exercises'].queryset = Exercise.visible_manager.visible(self.request_user)

    class Meta:
        model = InteractiveTestOneChoiceElement
        exclude = DEFAULT_EXCLUDE_FIELDS


class InteractiveSingleInputElementForm(BaseInteractiveElementForm):

    class Meta:
        model = InteractiveSingleInputElement
        exclude = DEFAULT_EXCLUDE_FIELDS


class InteractiveTextFieldElementForm(BaseInteractiveElementForm):
    class Meta:
        model = InteractiveTextFieldElement
        exclude = DEFAULT_EXCLUDE_FIELDS


class InteractiveVideoElementForm(BaseInteractiveElementForm):
    class Meta:
        model = InteractiveVideoElement
        exclude = DEFAULT_EXCLUDE_FIELDS


class InteractiveImageElementForm(BaseInteractiveElementForm):
    class Meta:
        model = InteractiveImageElement
        exclude = DEFAULT_EXCLUDE_FIELDS


class InteractiveElementForm(forms.Form):

    @staticmethod
    def get_interactive_elements():
        interactive_elements = []

        for obj in InteractiveElement.get_children():
            element = {
                'type': obj.InteractiveType.ID,
                'text': obj.InteractiveType.text,
                'href': reverse_lazy('interactivity:interactive-new', kwargs={'type': obj.InteractiveType.ID, 'number': 1})
            }
            interactive_elements.append(element)

        return interactive_elements



