from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns(
    '',

    url(r'^(?P<type>[\w-]+)/new/(?P<number>[0-9]+)$', InteractiveElementView.as_view(), name='interactive-new'),

        url(r'^add/(?P<type>[\w-]+)/(?P<model>[\w-]+)/', AddNewInteractiveChildElement.as_view(),
            name='child-element-new'),
)