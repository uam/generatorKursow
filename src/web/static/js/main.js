$(function(){
    $('.my_modal_runner').click(function(event) {

        var text = $(this).parent().find('select').find(':selected').data('href');
        console.log(text);
        $(this).attr('href', text);
        console.log($(this).attr('href'));

    });

    $('#interactive_type_buttons').find('button').click(function(evt) {
        evt.preventDefault();
        var url = $(this).data('href'),
            number_list = [],
            accordions = $('#forms').find('.interactive-element-form');

        $(accordions).each(function() {
            number_list.push(parseInt($(this).find('input.form_number').attr('name')));
        });

        var number = 1;
        for (i = 1; i <= accordions.length + 1; i++) {
            if (_.includes(number_list, i) == false)  {
                number = i;
                break;
            }
        }

        url = url.replace(/[0-9]/i, number)

        $('#forms').append($('<div>').load(url, function () {
            $('.remove-interactive-element-button').click(function (evt) {
                evt.preventDefault();

                $(this).parents('.interactive-element-form').remove();
            });
            $('.modal_runner').DjangoModalRunner({
                on_done: function () {
                    //$('#generic-modal').modal('hide');
                    //window.location.reload();
                }
            });

            $(".formset").formset({
                animateForms: true
            });
            $('input[data-formset-add]').click(function() {
                $('input[name$="DELETE"]').parents('.form-group').hide();
            });

            $('input[name$="DELETE"]').parents('.form-group').hide();

            $('select:not([multiple])').select2();

            $('select[multiple]').multiSelect({
                selectableHeader: "<legend>Wszystkie elementy</legend>",
                selectableFooter: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Szukaj...'>",
                selectionHeader: "<legend>Wybrane</legend>",
                afterInit: function(ms){
                    var that = this,
                        $selectableSearch = that.$selectableUl.next(),
                        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e){
                            if (e.which === 40){
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                },
                afterSelect: function(){
                    this.qs1.cache();
                },
                afterDeselect: function(){
                    this.qs1.cache();
                }
            });
        }));
    });


    $('.remove-interactive-element-button').click(function (evt) {
        evt.preventDefault();

        $(this).parents('.interactive-element-form').remove();
    });

    $('.modal_runner').DjangoModalRunner({
        on_done: function () {
            $('#generic-modal').modal('hide');
            window.location.reload();
        }
    });

    $(".formset").formset({
        animateForms: true
    });

    $(".dataTable").dataTable();

    $('input[data-formset-add]').click(function() {
        $('input[name$="DELETE"]').parents('.form-group').hide();
    });

    $('input[name$="DELETE"]').parents('.form-group').hide();

    $('select:not([multiple])').select2();

    $('select[multiple]').multiSelect({
        selectableHeader: "<legend>Wszystkie elementy</legend>",
        selectableFooter: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Szukaj...'>",
        selectionHeader: "<legend>Wybrane</legend>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.next(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

        },
        afterSelect: function(){
            this.qs1.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
        }
    });



});