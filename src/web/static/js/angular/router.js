CourseGeneratorApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {

        $urlRouterProvider.otherwise("/");
        // $locationProvider.html5Mode(true);

        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: '/static/partials/home.html',
                controller: 'HomeController'
            })

        .state('courses', {
                url: "/courses",
                templateUrl: '/static/partials/courses/index.html',
                controller: 'CourseController'
            })
            .state('courses.new', {
                url: "/new",
                templateUrl: '/static/partials/courses/new.html',
                controller: 'CourseNewController'
            })
            .state('courses.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/courses/detail.html',
                controller: 'CourseDetailController'
            }).state('courses.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/courses/edit.html',
                controller: 'CourseEditController'
            })

        .state('modules', {
                url: "/modules",
                templateUrl: '/static/partials/modules/index.html',
                controller: 'ModuleController'
            })
            .state('modules.new', {
                url: "/new",
                templateUrl: '/static/partials/modules/new.html',
                controller: 'ModuleNewController'
            })
            .state('modules.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/modules/detail.html',
                controller: 'ModuleDetailController'
            })
            .state('modules.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/modules/edit.html',
                controller: 'ModuleEditController'
            })

        .state('materials', {
                url: "/materials",
                templateUrl: '/static/partials/materials/index.html',
                controller: 'MaterialController'
            })
            .state('materials.new', {
                url: "/new",
                templateUrl: '/static/partials/materials/new.html',
                controller: 'MaterialNewController'
            })
            .state('materials.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/materials/detail.html',
                controller: 'MaterialDetailController'
            })

        .state('materials.theorem', {
                url: "/theorems",
                templateUrl: '/static/partials/materials/theorems/list.html',
                controller: 'materials.TheoremController'
            })
            .state('materials.theorem.new', {
                url: "/new",
                templateUrl: '/static/partials/materials/theorems/new.html',
                controller: 'materials.TheoremNewController'
            })
            .state('materials.theorem.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/materials/theorems/detail.html',
                controller: 'materials.TheoremDetailController'
            })
            .state('materials.theorem.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/materials/theorems/edit.html',
                controller: 'materials.TheoremEditController'
            })

        .state('materials.proof', {
                url: "/proofs",
                templateUrl: '/static/partials/materials/proofs/list.html',
                controller: 'materials.ProofController'
            })
            .state('materials.proof.new', {
                url: "/new",
                templateUrl: '/static/partials/materials/proofs/new.html',
                controller: 'materials.ProofNewController'
            })
            .state('materials.proof.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/materials/proofs/detail.html',
                controller: 'materials.ProofDetailController'
            })
            .state('materials.proof.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/materials/proofs/edit.html',
                controller: 'materials.ProofEditController'
            })

        .state('materials.exercise', {
                url: "/exercises",
                templateUrl: '/static/partials/materials/exercises/list.html',
                controller: 'materials.ExerciseController'
            })
            .state('materials.exercise.new', {
                url: "/new",
                templateUrl: '/static/partials/materials/exercises/new.html',
                controller: 'materials.ExerciseNewController'
            })
            .state('materials.exercise.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/materials/exercises/detail.html',
                controller: 'materials.ExerciseDetailController'
            })
            .state('materials.exercise.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/materials/exercises/edit.html',
                controller: 'materials.ExerciseEditController'
            })

        .state('materials.definition', {
                url: "/definitions",
                templateUrl: '/static/partials/materials/definitions/list.html',
                controller: 'materials.DefinitionController'
            })
            .state('materials.definition.new', {
                url: "/new",
                templateUrl: '/static/partials/materials/definitions/new.html',
                controller: 'materials.DefinitionNewController'
            })
            .state('materials.definition.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/materials/definitions/detail.html',
                controller: 'materials.DefinitionDetailController'
            })
            .state('materials.definition.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/materials/definitions/edit.html',
                controller: 'materials.DefinitionEditController'
            })

        .state('materials.example', {
                url: "/examples",
                templateUrl: '/static/partials/materials/examples/list.html',
                controller: 'materials.ExampleController'
            })
            .state('materials.example.new', {
                url: "/new",
                templateUrl: '/static/partials/materials/examples/new.html',
                controller: 'materials.ExampleNewController'
            })
            .state('materials.example.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/materials/examples/detail.html',
                controller: 'materials.ExampleDetailController'
            })
            .state('materials.example.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/materials/examples/edit.html',
                controller: 'materials.ExampleEditController'
            })

        .state('definitions', {
                url: "/definitions",
                templateUrl: '/static/partials/theory/definitions/list.html',
                controller: 'theory.DefinitionController'
            })
            .state('definitions.new', {
                url: "/new",
                templateUrl: '/static/partials/theory/definitions/new.html',
                controller: 'theory.DefinitionNewController'
            })
            .state('definitions.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/theory/definitions/detail.html',
                controller: 'theory.DefinitionDetailController'
            })
            .state('definitions.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/theory/definitions/edit.html',
                controller: 'theory.DefinitionEditController'
            })

        .state('proofs', {
                url: "/proofs",
                templateUrl: '/static/partials/theory/proofs/list.html',
                controller: 'theory.ProofController'
            })
            .state('proofs.new', {
                url: "/new",
                templateUrl: '/static/partials/theory/proofs/new.html',
                controller: 'theory.ProofNewController'
            })
            .state('proofs.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/theory/proofs/detail.html',
                controller: 'theory.ProofDetailController'
            })
            .state('proofs.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/theory/proofs/edit.html',
                controller: 'theory.ProofEditController'
            })

        .state('theorems', {
                url: "/theorems",
                templateUrl: '/static/partials/theory/theorems/list.html',
                controller: 'theory.TheoremController'
            })
            .state('theorems.new', {
                url: "/new",
                templateUrl: '/static/partials/theory/theorems/new.html',
                controller: 'theory.TheoremNewController'
            })
            .state('theorems.detail', {
                url: "/{id:int}",
                templateUrl: '/static/partials/theory/theorems/detail.html',
                controller: 'theory.TheoremDetailController'
            })
            .state('theorems.edit', {
                url: "/{id:int}/edit",
                templateUrl: '/static/partials/theory/theorems/edit.html',
                controller: 'theory.TheoremEditController'
            });

    }
]);
