Services.factory('Material', ['$resource',
    function($resource) {
        return $resource('/api/materials/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
