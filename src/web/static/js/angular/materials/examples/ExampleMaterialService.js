Services.factory('ExampleMaterial', ['$resource',
    function($resource) {
        return $resource('/api/exampleMaterials/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
