Controllers.controller('MaterialController', ['$scope', 'Material', function($scope, Material) {
    $scope.materials = Material.query();

}]);

Controllers.controller('MaterialDetailController', ['$scope', '$stateParams', 'Material', function($scope, $stateParams, Material) {
    Material.get({
        id: $stateParams.id
    }).$promise.then(function(material) {
        $scope.material = material;
    }, function(err) {
        console.log(err);
    });

}]);

Controllers.controller('MaterialNewController', ['$scope', '$state', '$stateParams', 'Material', function($scope, $state, $stateParams, Material) {
    $scope.material = new Material();

    $scope.submit = function() {

        $scope.material.materials = [1]

        $scope.material.$save(function() {
            $state.go("materials");
        });

    };

}]);
