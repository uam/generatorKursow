Services.factory('ExerciseMaterial', ['$resource',
    function($resource) {
        return $resource('/api/exerciseMaterials/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
