Controllers.controller('materials.ExerciseController', ['$scope', 'ExerciseMaterial', function($scope, ExerciseMaterial) {
    $scope.materials = ExerciseMaterial.query();

}]);

Controllers.controller('materials.ExerciseDetailController', ['$scope', '$stateParams', '$modal', '$state', 'ExerciseMaterial',
    function($scope, $stateParams, $modal, $state, ExerciseMaterial) {
        ExerciseMaterial.get({
            id: $stateParams.id
        }).$promise.then(function(material) {
            $scope.material = material;
        }, function(err) {
            alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
        });

        $scope.delete = function() {

            var modalInstance = $modal.open({
                animation: true,
                size: 'sm',
                templateUrl: 'static/partials/modals/confirmation.html',
                controller: 'ModalDeleteController',
                resolve: {
                    item: function() {
                        return $scope.material;
                    }
                }
            });

            modalInstance.result.then(function(obj_id) {
                $state.go('materials', {}, {
                    reload: true
                });
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });

        };

    }
]);

Controllers.controller('materials.ExerciseNewController', ['$scope', '$state', '$modal', '$stateParams', 'ExerciseMaterial', 'Exercise', 'InteractivityType',
    function($scope, $state, $modal, $stateParams, ExerciseMaterial, Exercise, InteractivityType) {

        $scope.material = new ExerciseMaterial();

        $scope.material.interactiveElements = [];

        Exercise.query().$promise
            .then(function(exercises) {
                $scope.exercises = exercises;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        InteractivityType.query().$promise
            .then(function(interactiveTypes) {
                $scope.interactiveTypes = interactiveTypes;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        $scope.submit = function() {

            if (!$scope.material.exercise.id) {
                $scope.material.exercise = $scope.material.exercise.originalObject;
            }

            $scope.material.$save()
                .then(function(obj) {
                    $state.go("materials.exercise.detail", {
                        id: obj.id
                    }, {
                        reload: true
                    });
                })
                .catch(function(err) {
                    alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
                });

        };

        $scope.newElement = function(list) {

            var interactiveElement = {};
            interactiveElement.number = list.length + 1;
            interactiveElement.exercises = [];

            list.push(interactiveElement);
        };

        $scope.removeElement = function(element, list) {

            list = _.remove(list, function(el) {
                return el.id == element.id;
            });
        };

        $scope.addExercise = function(exerciseList) {
            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseNewController'
            });

            modalInstance.result.then(function(createdObject) {
                exerciseList.push(createdObject);
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editExercise = function(exercise, exerciseList) {

            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseEditController',
                resolve: {
                    item: function() {
                        return exercise;
                    }
                }
            });

            modalInstance.result.then(function(obj) {

                // exerciseList = _.remove(exerciseList, function(el) {
                //     return el.id == obj.id;
                // });
                // exerciseList.push(obj);
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

       $scope.deleteExercise = function(exercise, exerciseList) {

            exerciseList = _.remove(exerciseList, function(el) {
                return el.id == exercise.id;
            });
        };

    }
]);

Controllers.controller('materials.ExerciseEditController', ['$scope', '$state', '$modal', '$stateParams', 'ExerciseMaterial', 'Exercise', 'InteractivityType',
    function($scope, $state, $modal, $stateParams, ExerciseMaterial, Exercise, InteractivityType) {

        ExerciseMaterial.get({
            id: $stateParams.id
        }).$promise.then(function(material) {
            $scope.material = material;
        }, function(err) {
            console.log(err);
        });

        Exercise.query().$promise
            .then(function(exercises) {
                $scope.exercises = exercises;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        InteractivityType.query().$promise
            .then(function(interactiveTypes) {
                $scope.interactiveTypes = interactiveTypes;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        $scope.submit = function() {

            if (!$scope.material.exercise.id) {
                $scope.material.exercise = $scope.material.exercise.originalObject;
            }

            $scope.material.$update({
                    id: $stateParams.id
                })
                .then(function(obj) {
                    $state.go("materials.exercise.detail", {
                        id: obj.id
                    }, {
                        reload: true
                    });
                })
                .catch(function(err) {
                    alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
                });

        };

        $scope.cancel = function() {
            $state.go("materials.exercise.detail", {
                id: $stateParams.id
            });
        };

        $scope.newElement = function(list) {

            var interactiveElement = {};
            interactiveElement.number = list.length + 1;
            interactiveElement.exercises = [];

            list.push(interactiveElement);
        };

        $scope.removeElement = function(element, list) {

            list = _.remove(list, function(el) {
                return el.id == element.id;
            });
        };

        $scope.addExercise = function(exerciseList) {
            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseNewController'
            });

            modalInstance.result.then(function(createdObject) {
                exerciseList.push(createdObject);
                createdObject.number = exerciseList.length;
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editExercise = function(exercise, exerciseList) {

            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseEditController',
                resolve: {
                    item: function() {
                        return exercise;
                    }
                }
            });

            modalInstance.result.then(function(obj) {

                // exerciseList = _.remove(exerciseList, function(el) {
                //     return el.id == obj.id;
                // });
                // exerciseList.push(obj);
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.deleteExercise = function(exercise, exerciseList) {

            exerciseList = _.remove(exerciseList, function(el) {
                return el.id == exercise.id;
            });
        };

    }
]);
