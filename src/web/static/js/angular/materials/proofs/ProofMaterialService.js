Services.factory('ProofMaterial', ['$resource',
    function($resource) {
        return $resource('/api/proofMaterials/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
