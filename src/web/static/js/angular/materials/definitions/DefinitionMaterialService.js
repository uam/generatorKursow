Services.factory('DefinitionMaterial', ['$resource',
    function($resource) {
        return $resource('/api/definitionMaterials/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
