Controllers.controller('materials.TheoremController', ['$scope', 'TheoremMaterial', function($scope, TheoremMaterial) {
    $scope.materials = TheoremMaterial.query();

}]);

Controllers.controller('materials.TheoremDetailController', ['$scope', '$stateParams', '$modal', '$state', 'TheoremMaterial',
    function($scope, $stateParams, $modal, $state, TheoremMaterial) {
        TheoremMaterial.get({
            id: $stateParams.id
        }).$promise.then(function(material) {
            $scope.material = material;
        }, function(err) {
            alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
        });

        $scope.delete = function() {

            var modalInstance = $modal.open({
                animation: true,
                size: 'sm',
                templateUrl: 'static/partials/modals/confirmation.html',
                controller: 'ModalDeleteController',
                resolve: {
                    item: function() {
                        return $scope.material;
                    }
                }
            });

            modalInstance.result.then(function(obj_id) {
                $state.go('materials', {}, {
                    reload: true
                });
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });

        };

    }
]);

Controllers.controller('materials.TheoremNewController', ['$scope', '$state', '$modal', '$stateParams', 'TheoremMaterial', 'Theorem', 'Exercise', 'InteractivityType',
    function($scope, $state, $modal, $stateParams, TheoremMaterial, Theorem, Exercise, InteractivityType) {

        $scope.material = new TheoremMaterial();

        $scope.material.interactiveElements = [];

        Theorem.query().$promise
            .then(function(theorems) {
                $scope.theorems = theorems;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        InteractivityType.query().$promise
            .then(function(interactiveTypes) {
                $scope.interactiveTypes = interactiveTypes;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        $scope.submit = function() {

            if (!$scope.material.theorem.id) {
                $scope.material.theorem = $scope.material.theorem.originalObject;
            }

            $scope.material.$save()
                .then(function(obj) {
                    $state.go("materials.theorem.detail", {
                        id: obj.id
                    }, {
                        reload: true
                    });
                })
                .catch(function(err) {
                    alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
                });

        };

        $scope.newElement = function(list) {

            var interactiveElement = {};
            interactiveElement.number = list.length + 1;
            interactiveElement.exercises = [];

            list.push(interactiveElement);
        };

        $scope.removeElement = function(element, list) {

            list = _.remove(list, function(el) {
                return el.id == element.id;
            });
        };

        $scope.addExercise = function(exerciseList) {
            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseNewController'
            });

            modalInstance.result.then(function(createdObject) {
                exerciseList.push(createdObject);
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editExercise = function(exercise, exerciseList) {

            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseEditController',
                resolve: {
                    item: function() {
                        return exercise;
                    }
                }
            });

            modalInstance.result.then(function(obj) {

                // exerciseList = _.remove(exerciseList, function(el) {
                //     return el.id == obj.id;
                // });
                // exerciseList.push(obj);
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.deleteExercise = function(exercise, exerciseList) {

            exerciseList = _.remove(exerciseList, function(el) {
                return el.id == exercise.id;
            });
        };

    }
]);

Controllers.controller('materials.TheoremEditController', ['$scope', '$state', '$modal', '$stateParams', 'TheoremMaterial', 'Theorem', 'Exercise', 'InteractivityType',
    function($scope, $state, $modal, $stateParams, TheoremMaterial, Theorem, Exercise, InteractivityType) {

        TheoremMaterial.get({
            id: $stateParams.id
        }).$promise.then(function(material) {
            $scope.material = material;
        }, function(err) {
            console.log(err);
        });

        Theorem.query().$promise
            .then(function(theorems) {
                $scope.theorems = theorems;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        InteractivityType.query().$promise
            .then(function(interactiveTypes) {
                $scope.interactiveTypes = interactiveTypes;
            })
            .catch(function(err) {
                alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
            });

        $scope.submit = function() {

            if (!$scope.material.theorem.id) {
                $scope.material.theorem = $scope.material.theorem.originalObject;
            }

            $scope.material.$update({
                    id: $stateParams.id
                })
                .then(function(obj) {
                    $state.go("materials.theorem.detail", {
                        id: obj.id
                    }, {
                        reload: true
                    });
                })
                .catch(function(err) {
                    alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
                });

        };

        $scope.cancel = function() {
            $state.go("materials.theorem.detail", {
                id: $stateParams.id
            });
        };

        $scope.newElement = function(list) {

            var interactiveElement = {};
            interactiveElement.number = list.length + 1;
            interactiveElement.exercises = [];

            list.push(interactiveElement);
        };

        $scope.removeElement = function(element, list) {

            list = _.remove(list, function(el) {
                return el.id == element.id;
            });
        };

        $scope.addExercise = function(exerciseList) {
            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseNewController'
            });

            modalInstance.result.then(function(createdObject) {
                exerciseList.push(createdObject);
                createdObject.number = exerciseList.length;
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editExercise = function(exercise, exerciseList) {

            var modalInstance = $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'static/partials/modals/exercise.html',
                controller: 'ModalExerciseEditController',
                resolve: {
                    item: function() {
                        return exercise;
                    }
                }
            });

            modalInstance.result.then(function(obj) {

                // exerciseList = _.remove(exerciseList, function(el) {
                //     return el.id == obj.id;
                // });
                // exerciseList.push(obj);
            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.deleteExercise = function(exercise, exerciseList) {

            exerciseList = _.remove(exerciseList, function(el) {
                return el.id == exercise.id;
            });
        };

    }
]);
