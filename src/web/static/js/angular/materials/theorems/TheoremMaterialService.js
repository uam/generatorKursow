Services.factory('TheoremMaterial', ['$resource',
    function($resource) {
        return $resource('/api/theoremMaterials/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
