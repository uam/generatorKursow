
Controllers.controller('ModalExerciseEditController', function($scope, $modalInstance, item, SolutionType) {

    $scope.exercise = item;

    $scope.addAnswer = function(list) {

        var answer = {};
        answer.number = list.length + 1;
        answer.value = '';
        answer.correct = false;

        list.push(answer);
    };

    $scope.deleteAnswer = function(element, list) {

        list = _.remove(list, function(el) {
            return el.id == element.id;
        });
    }


    $scope.ok = function() {
        $modalInstance.close($scope.exercise);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});


Controllers.controller('ModalExerciseNewController', function($scope, $modalInstance, SolutionType, Exercise) {


    $scope.exercise = new Exercise();
    $scope.exercise.answers = [];
    $scope.exercise.title = 'Zadanie'

    $scope.addAnswer = function(list) {

        var answer = {};
        answer.number = list.length + 1;
        answer.value = '';
        answer.correct = false;

        list.push(answer);
    };

    $scope.addAnswer($scope.exercise.answers)

    $scope.ok = function() {
        $modalInstance.close($scope.exercise);

    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});

Controllers.controller('ModalDeleteController', function($scope, $modalInstance, item) {

    var itemId = item.id

    $scope.ok = function() {

        item.$delete({
            id: itemId
        }).then(function() {
            $modalInstance.close(itemId);
        }).catch(function(err) {
            alert("Wystąpił błąd. Sprawdź konsolę"); console.error(err);
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});
