Controllers.controller('theory.ProofController', ['$scope', 'Proof', function($scope, Proof) {
    $scope.proofs = Proof.query();

}]);

Controllers.controller('theory.ProofDetailController', ['$scope', '$state', '$stateParams', '$modal', 'Proof', function($scope, $state, $stateParams, $modal, Proof) {
    Proof.get({
        id: $stateParams.id
    }).$promise.then(function(proof) {
        $scope.proof = proof;
    }, function(err) {
        console.log(err);
    });

    $scope.delete = function() {

        var modalInstance = $modal.open({
            animation: true,
            size: 'sm',
            templateUrl: 'static/partials/modals/confirmation.html',
            controller: 'ModalDeleteController',
            resolve: {
                item: function() {
                    return $scope.proof;
                }
            }
        });

        modalInstance.result.then(function(obj_id) {
            $state.go('proofs', {}, {
                reload: true
            });
        }, function() {
            console.info('Modal dismissed at: ' + new Date());
        });

    }

}]);


Controllers.controller('theory.ProofNewController', ['$scope', '$state', '$stateParams', 'Proof', function($scope, $state, $stateParams, Proof) {
    $scope.proof = new Proof();

    var partsCounter = 0;

    $scope.proof.parts = [];

    $scope.submit = function() {

        $scope.proof.$save()
            .then(function(obj) {
                $state.go('proofs.detail', {
                    id: obj.id
                });
            })
            .catch(function(err) {
                alert("Wystąpił błąd" + JSON.parse(err));
            });

    };

    $scope.newPart = function() {

        var part = {};

        part.text = '';
        part.number = $scope.proof.parts.length + 1;

        $scope.proof.parts.push(part);
    };


}]);



Controllers.controller('theory.ProofEditController', ['$scope', '$state', '$stateParams', 'Proof', function($scope, $state, $stateParams, Proof) {
    Proof.get({
            id: $stateParams.id
        }).$promise
        .then(function(proof) {
            $scope.proof = proof;
        })
        .catch(function(err) {
            console.error(err);
        });

    $scope.submit = function() {

        $scope.proof.$update({
                id: $stateParams.id
            })
            .then(function(obj) {
                $state.go('proofs.detail', {
                    id: obj.id
                });
            })
            .catch(function(err) {
                alert("Wystąpił błąd" + JSON.parse(err));
            });

    };

    $scope.newPart = function() {

        var part = {};

        part.text = '';
        part.number = $scope.proof.parts.length + 1;

        $scope.proof.parts.push(part);
    };


}]);
