Services.factory('Proof', ['$resource',
    function($resource) {
        return $resource('/api/proofs/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
