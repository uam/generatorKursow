Controllers.controller('theory.TheoremController', ['$scope', 'Theorem', function($scope, Theorem) {
    $scope.theorems = Theorem.query();

}]);

Controllers.controller('theory.TheoremDetailController', ['$scope', '$state', '$stateParams', '$modal', 'Theorem', function($scope, $state, $stateParams, $modal, Theorem) {
    Theorem.get({
        id: $stateParams.id
    }).$promise.then(function(theorem) {
        $scope.theorem = theorem;
    }, function(err) {
        console.log(err);
    });

    $scope.delete = function() {

        var modalInstance = $modal.open({
            animation: true,
            size: 'sm',
            templateUrl: 'static/partials/modals/confirmation.html',
            controller: 'ModalDeleteController',
            resolve: {
                item: function() {
                    return $scope.theorem;
                }
            }
        });

        modalInstance.result.then(function(obj_id) {
            $state.go('theorems', {}, {
                reload: true
            });
        }, function() {
            console.info('Modal dismissed at: ' + new Date());
        });

    };

}]);


Controllers.controller('theory.TheoremNewController', ['$scope', '$state', '$stateParams', 'Theorem', function($scope, $state, $stateParams, Theorem) {
    $scope.theorem = new Theorem();

    $scope.submit = function() {

        $scope.theorem.$save()
            .then(function(obj) {
                $state.go('theorems.detail', {
                    id: obj.id
                });
            })
            .catch(function(err) {
                alert("Wystąpił błąd" + JSON.parse(err));
            });

    };


}]);

Controllers.controller('theory.TheoremEditController', ['$scope', '$state', '$stateParams', 'Theorem', function($scope, $state, $stateParams, Theorem) {
    Theorem.get({
            id: $stateParams.id
        }).$promise
        .then(function(theorem) {
            $scope.theorem = theorem
        })
        .catch(function(err) {
            console.error(err)
        });

    $scope.submit = function() {

        $scope.theorem.$update({
                id: $stateParams.id
            })
            .then(function(obj) {
                $state.go('theorems.detail', {
                    id: obj.id
                })
            })
            .catch(function(err) {
                alert("Wystąpił błąd" + JSON.parse(err))
            });

    };

}]);
