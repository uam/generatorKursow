Services.factory('Theorem', ['$resource',
    function($resource) {
        return $resource('/api/theorems/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
