Services.factory('Exercise', ['$resource',
    function($resource) {
        return $resource('/api/exercises/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);

Services.factory('SolutionType', ['$resource',
    function($resource) {
        return $resource('/api/solutionTypes/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
