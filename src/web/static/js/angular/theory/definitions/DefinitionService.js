Services.factory('Definition', ['$resource',
    function($resource) {
        return $resource('/api/definitions/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
