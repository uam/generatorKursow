Controllers.controller('theory.DefinitionController', ['$scope', 'Definition', function($scope, Definition) {
    $scope.definitions = Definition.query();

}]);

Controllers.controller('theory.DefinitionDetailController', ['$scope', '$state', '$stateParams', '$modal', 'Definition', function($scope, $state, $stateParams, $modal, Definition) {
    Definition.get({
        id: $stateParams.id
    }).$promise.then(function(definition) {
        $scope.definition = definition;
    }, function(err) {
        console.log(err);
    });

    $scope.delete = function() {

        var modalInstance = $modal.open({
            animation: true,
            size: 'sm',
            templateUrl: 'static/partials/modals/confirmation.html',
            controller: 'ModalDeleteController',
            resolve: {
                item: function() {
                    return $scope.definition;
                }
            }
        });

        modalInstance.result.then(function(obj_id) {
            $state.go('definitions', {}, {
                reload: true
            });
        }, function() {
            console.info('Modal dismissed at: ' + new Date());
        });

    }

}]);


Controllers.controller('theory.DefinitionNewController', ['$scope', '$state', '$stateParams', 'Definition', function($scope, $state, $stateParams, Definition) {
    $scope.definition = new Definition();

    $scope.submit = function() {

        $scope.definition.$save()
            .then(function(obj) {
                $state.go('definitions.detail', {
                    id: obj.id
                });
            })
            .catch(function(err) {
                alert("Wystąpił błąd" + JSON.parse(err));
            });

    };


}]);

Controllers.controller('theory.DefinitionEditController', ['$scope', '$state', '$stateParams', 'Definition', function($scope, $state, $stateParams, Definition) {
    Definition.get({
            id: $stateParams.id
        }).$promise
        .then(function(definition) {
            $scope.definition = definition
        })
        .catch(function(err) {
            console.error(err);
        });

    $scope.submit = function() {

        $scope.definition.$update({
                id: $stateParams.id
            })
            .then(function(obj) {
                $state.go('definitions.detail', {
                    id: obj.id
                });
            })
            .catch(function(err) {
                alert("Wystąpił błąd" + JSON.parse(err));
            });

    };


}]);
