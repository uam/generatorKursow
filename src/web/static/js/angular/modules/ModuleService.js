Services.factory('Module', ['$resource',
    function($resource) {
        return $resource('/api/modules/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
