Controllers.controller('ModuleController', ['$scope', 'Module', function($scope, Module) {
    $scope.modules = Module.query();

}]);

Controllers.controller('ModuleDetailController', ['$scope', '$stateParams', '$state', '$modal', 'Module', function($scope, $stateParams, $state, $modal, Module) {
    Module.get({
        id: $stateParams.id
    }).$promise.then(function(module) {
        $scope.module = module;
    }, function(err) {
        console.log(err);
    });

    $scope.delete = function() {
        var modalInstance = $modal.open({
            animation: true,
            size: 'sm',
            templateUrl: 'static/partials/modals/confirmation.html',
            controller: 'ModalDeleteController',
            resolve: {
                item: function() {
                    return $scope.module;
                }
            }
        });

        modalInstance.result.then(function(obj_id) {
            $state.go('modules', {}, {
                reload: true
            });
        }, function() {
            console.info('Modal dismissed at: ' + new Date());
        });

    };

}]);

Controllers.controller('ModuleNewController', ['$scope', '$state', '$stateParams', 'Module', 'Material', function($scope, $state, $stateParams, Module, Material) {
    $scope.module = new Module();
    $scope.module.materials = [];

    $scope.materials = Material.query();

    $scope.submit = function() {

        $scope.module.$save()
            .then(function(obj) {
                $state.go("modules.detail", {
                    id: obj.id
                });
            });

    };

    $scope.selectMaterial = function($item, $model, $label) {

        $scope.module.materials.push($item);
        $scope.selected = undefined;

        _.remove($scope.materials, function(el) {
            return el.id == $item.id;
        });

    };

    $scope.removeMaterial = function(item, list) {

        _.remove(list, function(el) {
            return el.id == item.id;
        });

        $scope.materials.push(item);

    };

}]);


Controllers.controller('ModuleEditController', ['$scope', '$state', '$stateParams', 'Module', 'Material', function($scope, $state, $stateParams, Module, Material) {

    Module.get({
            id: $stateParams.id
        }).$promise
        .then(function(module) {
            $scope.module = module;

            Material.query().$promise
                .then(function(materials) {

                    var id_list = _.pluck($scope.module.materials, 'id');

                    $scope.materials = _.filter(materials, function(el) {
                        return !_.includes(id_list, el.id);
                    });

                })
                .catch(function(err) {
                    console.error(err);
                });
        })
        .catch(function(er) {
            console.error(err);

        });


    $scope.submit = function() {

        $scope.module.$update({
            id: $stateParams.id
        }).
        then(function(obj) {
            $state.go("modules.detail", {
                id: obj.id
            }, {
                reload: true
            });
        });

    };

    $scope.selectMaterial = function($item, $model, $label) {

        $scope.module.materials.push($item);
        $scope.selected = undefined;

        _.remove($scope.materials, function(el) {
            return el.id == $item.id;
        });

    };

    $scope.removeMaterial = function(item, list) {

        _.remove(list, function(el) {
            return el.id == item.id;
        });

        $scope.materials.push(item);

    };


}]);
