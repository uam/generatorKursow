Services.factory('Course', ['$resource',
    function($resource) {
        return $resource('/api/courses/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
