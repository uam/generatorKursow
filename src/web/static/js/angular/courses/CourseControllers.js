Controllers.controller('CourseController', ['$scope', 'Course', function($scope, Course) {
    $scope.courses = Course.query();

}]);

Controllers.controller('CourseDetailController', ['$scope', '$stateParams', '$modal', '$state', 'Course', function($scope, $stateParams, $modal, $state, Course) {
    Course.get({
        id: $stateParams.id
    }).$promise.then(function(course) {
        $scope.course = course;
    }, function(err) {
        console.log(err);
    });

    $scope.delete = function() {
        var modalInstance = $modal.open({
            animation: true,
            size: 'sm',
            templateUrl: 'static/partials/modals/confirmation.html',
            controller: 'ModalDeleteController',
            resolve: {
                item: function() {
                    return $scope.course;
                }
            }
        });

        modalInstance.result.then(function(obj_id) {
            $state.go('courses', {}, {
                reload: true
            });
        }, function() {
            console.info('Modal dismissed at: ' + new Date());
        });

    };

}]);

Controllers.controller('CourseNewController', ['$scope', '$state', '$stateParams', 'Course', 'Module', function($scope, $state, $stateParams, Course, Module) {
    $scope.course = new Course();
    $scope.course.modules = [];

    $scope.modules = Module.query();

    $scope.submit = function() {

        $scope.course.$save()
            .then(function(obj) {
                $state.go("courses.detail", {
                    id: obj.id
                });
            })
            .catch(function(err) {
                console.error(err);
            });

    };

    $scope.selectModule = function($item, $model, $label) {

        $scope.course.modules.push($item);
        $scope.selected = undefined;

        _.remove($scope.modules, function(el) {
            return el.id == $item.id;
        });

    };

    $scope.removeModule = function(item, list) {

        _.remove(list, function(el) {
            return el.id == item.id;
        });

        $scope.modules.push(item);

    };

}]);

Controllers.controller('CourseEditController', ['$scope', '$state', '$stateParams', 'Course', 'Module', function($scope, $state, $stateParams, Course, Module) {
    Course.get({
            id: $stateParams.id
        }).$promise
        .then(function(course) {
            $scope.course = course;

            Module.query().$promise
                .then(function(modules) {

                    var id_list = _.pluck($scope.course.modules, 'id');

                    $scope.modules = _.filter(modules, function(el) {
                        return !_.includes(id_list, el.id);
                    });

                })
                .catch(function(err) {
                    console.error(err);
                });
        })
        .catch(function(err) {
            console.error(err);
        });

    $scope.submit = function() {

        $scope.course.$update({
                id: $stateParams.id
            })
            .then(function(obj) {
                $state.go("courses.detail", {
                    id: obj.id
                }, {
                    reload: true
                });
            });

    };

    $scope.selectModule = function($item, $model, $label) {

        $scope.course.modules.push($item);
        $scope.selected = undefined;

        _.remove($scope.modules, function(el) {
            return el.id == $item.id;
        });

    };

    $scope.removeModule = function(item, list) {

        _.remove(list, function(el) {
            return el.id == item.id;
        });

        $scope.modules.push(item);

    };

}]);
