Services.factory('InteractivityType', ['$resource',
    function($resource) {
        return $resource('/api/interactiveTypes/:id/ ', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    }
]);
