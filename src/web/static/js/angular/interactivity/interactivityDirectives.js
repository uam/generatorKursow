Directives.directive("interactive", function () {
   
    return {
        replace: true,
        scope: {
            template: '@'
        },
        template: '<script ng-include src="template"></script>'
    };
});
