var CourseGeneratorApp = angular.module('CourseGeneratorApp', [
    'ui.router',
    'ui.bootstrap',
    'datatables',
    'angucomplete',
    'youtube-embed',
    'angular-loading-bar',
    'ngAnimate',
    'Controllers',
    'Services',
    'Filters',
    'Directives'
]);

var Controllers = angular.module('Controllers', []);
var Filters = angular.module('Filters', []);
var Services = angular.module('Services', ['ngResource']);
var Directives = angular.module('Directives', []);

Services.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);

CourseGeneratorApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
});

CourseGeneratorApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div class="icon-loader"></div></div>';
}]);
