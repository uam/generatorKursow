# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse

menuitems = [{
    'url': reverse('web:home'),
    'icon': 'fa-road',
    'title': 'Strona główna'
}, {
    'url': reverse('course:list'),
    'icon': 'fa-desktop',
    'title': 'Kursy'
}, {
    'url': reverse('module:list'),
    'icon': 'fa-newspaper-o',
    'title': 'Moduły',
}, {
    'url': reverse('material:list'),
    'icon': 'fa-clipboard',
    'title': 'Materiały szkoleniowe',
}, {
    'icon': 'fa-book',
    'title': 'Baza teorii',
    'children': [{
        'url': reverse('theory:definition:list'),
        'icon': 'fa-file-o',
        'title': 'Definicje',
    }, {
        'url': reverse('theory:proof:list'),
        'icon': 'fa-file-o',
        'title': 'Dowody',
    }, {
        'url': reverse('theory:theorem:list'),
        'icon': 'fa-file-o',
        'title': 'Twierdzenia',
    }, {
        'url': reverse('theory:exercise:list'),
        'icon': 'fa-file-o',
        'title': 'Zadania',
    }
    ]

}]
