# -*- coding: utf-8 -*-
from apps.common.forms import BaseCrispyFormHelper, BaseCrispyRelatedFormHelper
from generatorKursow import settings
from web.menu import menuitems

def site_proccessor(request):

    return {
        'site_name': settings.SITE_NAME,
        'meta_keywords': settings.META_KEYWORDS,
        'meta_description': settings.META_DESCRIPTION,
        'request': request,
        'menu': render_menu(request),
        'crispy_helper': BaseCrispyFormHelper(),
        'crispy_related_form_helper': BaseCrispyRelatedFormHelper()
    }


def render_menu(request):

    for item in menuitems:
        if request.get_full_path() == item.get('url'):
            item['active'] = True

    return menuitems
