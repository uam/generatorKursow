from django.conf.urls import patterns, url

from web.views import *


urlpatterns = patterns(
        '',

        url(r'^$',
            home,
            name='home'),

        url(r'^download/course/(?P<id>[\d]+)/$',
            choose_course_template,
            name='download-course'),
        url(r'^download/module/(?P<id>[\d]+)/$',
            choose_module_template,
            name='download-module'),
        url(r'^download/material/(?P<id>[\d]+)/$',
            choose_material_template,
            name='download-material'),

        url(r'^download/(?P<item_type>course|module|material)/(?P<id>[\d]+)/without/template/$',
            download,
            name='download-without-template'),
        url(
            r'^download/(?P<item_type>course|module|material)/(?P<id>[\d]+)/using/template/(?P<template_name>[A-Za-z]+)/$',
            download,
            name='download-using-template'),

        url(r'^preview/(?P<item_type>course|module|material)/(?P<id>[\d]+)/$',
            preview,
            name='preview')

)
