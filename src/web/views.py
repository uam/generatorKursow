# -*- coding: utf-8 -*-
import urllib2
import zipfile
from StringIO import StringIO

import os
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.template.loader import render_to_string
from django.views.decorators.clickjacking import xframe_options_exempt
from plugins.helper import get_full_plugins_info

OUTPUT_DIR_NAME = 'preview'

@login_required
def home(request):
    return render(request, 'web/index.html', {})

def _zipdir(path, ziph):
    full_path = os.path.join(settings.BASE_DIR, path)

    ignore_files_archive_list = ['config.js', 'course.json', 'index.html']

    for root, dirs, files in os.walk(full_path):
        for _file in files:
            if _file not in ignore_files_archive_list:
                relative_root = root[(len(settings.BASE_DIR) + 1):]
                source_path = os.path.join(root, _file)
                target_path = os.path.join(relative_root, _file)
                ziph.write(source_path, target_path)


@login_required
def choose_course_template(request, id):
    return choose_template(request, 'course', id)


@login_required
def choose_module_template(request, id):
    return choose_template(request, 'module', id)


@login_required
def choose_material_template(request, id):
    return choose_template(request, 'material', id)


@login_required
def choose_template(request, item_type, id):
    return render(request, 'userTemplates/choose.html', {
        'item_type': item_type,
        'item_id': id,
        'templates': os.listdir(
            settings.BASE_DIR + '/userTemplates/uploaded')})


@login_required
def download(request, item_type, id, template_name=None):
    if (item_type == 'course'):
        return download_course(request, id, template_name)
    if (item_type == 'module'):
        return download_module(request, id, template_name)
    if (item_type == 'material'):
        return download_material(request, id, template_name)


@login_required
def download_course(request, id, template_name=None):

    url = 'http://%s/api/courses/%s/' % (request.META['HTTP_HOST'], str(id))
    opener = urllib2.build_opener()
    opener.addheaders.append(('Cookie', 'sessionid=%s' % (request.COOKIES.get('sessionid'))))
    json_data = opener.open(url).read()

    in_memory = _write_all_data(json_data, 0, template_name)

    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=kurs.zip'
    in_memory.seek(0)
    response.write(in_memory.read())
    return response


@login_required
def download_module(request, id, template_name=None):

    url = 'http://%s/api/modules/%s/' % (request.META['HTTP_HOST'], str(id))
    opener = urllib2.build_opener()
    opener.addheaders.append(('Cookie', 'sessionid=%s' % (request.COOKIES.get('sessionid'))))
    json_data = opener.open(url).read()

    in_memory = _write_all_data(json_data, 1, template_name)

    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=kurs.zip'
    # in_memory.seek(0)
    response.write(in_memory.read())
    return response


@login_required
def download_material(request, id, template_name=None):

    url = 'http://%s/api/materials/%s/' % (request.META['HTTP_HOST'], str(id))

    opener = urllib2.build_opener()
    opener.addheaders.append(('Cookie', 'sessionid=%s' % (request.COOKIES.get('sessionid'))))
    json_data = opener.open(url).read()

    in_memory = _write_all_data(json_data, 2, template_name)

    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=kurs.zip'
    in_memory.seek(0)
    response.write(in_memory.read())
    return response


def _write_all_data(json_data, view_type, template_name=None):
    in_memory = StringIO()

    archive = zipfile.ZipFile(in_memory, 'w')

    _zipdir(OUTPUT_DIR_NAME, archive)

    theme_files_css = []

    if template_name:
        theme_files_path = os.path.join(
            settings.BASE_DIR, 'userTemplates/uploaded', template_name)
        theme_files = os.listdir(theme_files_path)
        for theme_file in os.listdir(theme_files_path):
            ext = theme_file.split('.')[-1]
            if ext == 'css':
                theme_files_css.append(theme_file)
            full_path = os.path.join(theme_files_path, theme_file)
            archive.write(
                full_path,
                '%s/css/themes/%s' % (OUTPUT_DIR_NAME, theme_file))

    archive.writestr("%s/data/course.json" % OUTPUT_DIR_NAME, json_data)
    archive.writestr("%s/data/config.js" % OUTPUT_DIR_NAME, "var COURSE_VIEW_TYPE = %s;"
                                                            "var STATIC_URL = ''" % view_type)

    plugins = get_full_plugins_info()

    static_files_js = []
    static_files_css = []

    for plugin_name, item in plugins.items():
        static_files_path = os.path.join(item['path'], 'static')
        for root, dirs, files in os.walk(static_files_path):
            for _file in files:
                static_path = '/'.join(root.split('/')[-2:])
                static_file_path = os.path.join(static_path, _file)

                ext = _file.split('.')[-1]
                if ext == 'css':
                    static_files_css.append('%s' % static_file_path)
                elif ext == 'js':
                    static_files_js.append('%s' % static_file_path)

                full_path = os.path.join(static_files_path, static_file_path)
                archive.write(full_path, '%s/%s' % (OUTPUT_DIR_NAME, static_file_path))

    index_html = render_to_string("web/preview.html", {
        'download': True,
        'STATIC_URL': "",
        'static_files_js': static_files_js,
        'static_files_css': static_files_css,
        'theme_files_css': theme_files_css})

    with open('%s/index.html' % settings.BASE_DIR, 'wb') as tmp_index:
        tmp_index.write(index_html)

    archive.write("%s/index.html" % settings.BASE_DIR, "%s/index.html" % OUTPUT_DIR_NAME)

    # fix for Linux zip files read in Windows
    for _file in archive.filelist:
        _file.create_system = 0

    archive.close()

    return in_memory


@xframe_options_exempt
def preview(request, item_type, id):
    plugins = get_full_plugins_info()

    static_files_js = []
    static_files_css = []

    for plugin_name, item in plugins.items():
        static_files_path = os.path.join(item['path'], 'static')
        for root, dirs, files in os.walk(static_files_path):
            for _file in files:
                static_path = '/'.join(root.split('/')[-2:])
                static_file_path = os.path.join(static_path, _file)

                ext = _file.split('.')[-1]
                if ext == 'css':
                    static_files_css.append('%s' % static_file_path)
                elif ext == 'js':
                    static_files_js.append('%s' % static_file_path)

    if item_type == 'course':
        api_url = '/api/courses/%s/' % id
        reversed_url = reverse('course:detail', args=[id])
        course_type = 0
    elif item_type == 'module':
        api_url = '/api/modules/%s/' % id
        reversed_url = reverse('module:detail', args=[id])
        course_type = 1
    else:
        api_url = '/api/materials/%s/' % id
        reversed_url = reverse('material:detail', args=[id])
        course_type = 2

    return render_to_response('web/preview.html', {'download': False,
                                                   'STATIC_URL': "/static/",
                                                   'api_url': api_url,
                                                   'back_href': reversed_url,
                                                   'static_files_js': static_files_js,
                                                   'static_files_css': static_files_css,
                                                   'course_type': course_type})
