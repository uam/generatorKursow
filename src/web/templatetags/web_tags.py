# -*- coding: utf-8 -*-

from apps.course.models import Course
from apps.material.models import Material
from apps.module.models import Module
from django import template

register = template.Library()


@register.inclusion_tag('tags/nav_menu.html', takes_context=True)
def nav_menu(context, calling_page=None):
    return {
        'menuitems': context['menu'],
        'username': context['request'].user.username,
    }


@register.inclusion_tag('tags/interactive_element_display.html', takes_context=True)
def interactive_elements_display(context, interactive_element):
    form = interactive_element.get_form()(instance=interactive_element)

    return {
        'form': form
    }


@register.filter
def is_course(object):
    if isinstance(object, Course):
        return True
    return False


@register.filter
def is_module(object):
    if isinstance(object, Module):
        return True
    return False


@register.filter
def is_material(object):
    if isinstance(object, Material):
        return True
    return False


@register.filter
def is_private(object):
    if object.visibilityType_id == 1:
        return True
    return False


@register.filter
def is_my(object, user):
    if object.createdBy == user:
        return True
    return False

