$(window).load(function () {

    //constant
    var widthOfLeaf = 791;
    var heightOfLeaf = 1024;


    $(window).resize(function () {
        rwd();
    });

    function rwd() {
        var h = $($(".cover .content")[0]).height();
        var ratio = h / heightOfLeaf;
        $($(".cover .content")[0]).width(widthOfLeaf * ratio);
    }
    
    rwd();

});